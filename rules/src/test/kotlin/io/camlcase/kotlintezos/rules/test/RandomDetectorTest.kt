/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.rules.test

import com.android.tools.lint.checks.infrastructure.LintDetectorTest
import com.android.tools.lint.detector.api.Detector
import com.android.tools.lint.detector.api.Issue
import io.camlcase.kotlintezos.rules.RandomDetector
import org.junit.Test

@Suppress("UnstableApiUsage")
class RandomDetectorTest : LintDetectorTest() {

    @Test
    fun test_SecureRandom_OK() {
        lint().files(
            kotlin("""
                    package test.pkg
                    import java.security.SecureRandom
                    fun sample() {
                        val array = ByteArray(10)
                        val random = SecureRandom().nextBytes(array)
                    }
                    """
            ).indented())
            .allowMissingSdk()
            .run()
            .expectClean()
    }

    @Test
    fun test_kotlinRandom_Import() {
        lint().files(
            kotlin("""
                    package test.pkg
                    import kotlin.random.Random
                    fun sample() {
                        val array = ByteArray(10)
                        val random = Random().nextBytes(array)
                    }
                    """
            ).indented())
            .allowMissingSdk()
            .run()
            .expect("""
                src/test/pkg/test.kt:2: Error: Simple random usage is forbidden. [RandomDetector]
                import kotlin.random.Random
                       ~~~~~~~~~~~~~~~~~~~~
                src/test/pkg/test.kt:5: Error: Simple random usage is forbidden. [RandomDetector]
                    val random = Random().nextBytes(array)
                                 ~~~~~~~~
                2 errors, 0 warnings
                    """
            )
    }

    @Test
    fun test_kotlinRandom_Declaration() {
        lint().files(
            kotlin("""
                    package test.pkg
                    fun sample() {
                        val array = ByteArray(10)
                        val random = kotlin.random.Random().nextBytes(array)
                    }
                    """
            ).indented())
            .allowMissingSdk()
            .run()
            .expect("""
                src/test/pkg/test.kt:4: Error: Simple random usage is forbidden. [RandomDetector]
                val random = kotlin.random.Random().nextBytes(array)
                             ~~~~~~~~~~~~~~~~~~~~~~
            1 errors, 0 warnings
                    """
            )
    }

    @Test
    fun test_kotlinRandom_Return() {
        lint().files(
            kotlin("""
                    package test.pkg
                    fun sample() : ByteArray {
                        val array = ByteArray(10)
                        return kotlin.random.Random().nextBytes(array)
                    }
                    """
            ).indented())
            .allowMissingSdk()
            .run()
            .expect("""
                src/test/pkg/test.kt:4: Error: Simple random usage is forbidden. [RandomDetector]
                    return kotlin.random.Random().nextBytes(array)
                           ~~~~~~~~~~~~~~~~~~~~~~
                1 errors, 0 warnings
                    """
            )
    }

    @Test
    fun test_javaRandom_Import() {
        lint().files(
            kotlin("""
                    package test.pkg
                    import java.util.Random
                    fun sample() {
                        val array = ByteArray(10)
                        val random = Random().nextBytes(array)
                    }
                    """
            ).indented())
            .allowMissingSdk()
            .run()
            .expect("""
                src/test/pkg/test.kt:2: Error: Simple random usage is forbidden. [RandomDetector]
                import java.util.Random
                       ~~~~~~~~~~~~~~~~
                src/test/pkg/test.kt:5: Error: Simple random usage is forbidden. [RandomDetector]
                    val random = Random().nextBytes(array)
                                 ~~~~~~~~
                2 errors, 0 warnings
                    """
            )
    }

    override fun getDetector(): Detector {
        return RandomDetector()
    }

    override fun getIssues(): List<Issue> {
        return listOf(RandomDetector.ISSUE)
    }
}
