package io.camlcase.kotlintezos.rules

import com.android.tools.lint.client.api.UElementHandler
import com.android.tools.lint.detector.api.Category
import com.android.tools.lint.detector.api.Detector
import com.android.tools.lint.detector.api.Implementation
import com.android.tools.lint.detector.api.Issue
import com.android.tools.lint.detector.api.JavaContext
import com.android.tools.lint.detector.api.Scope
import com.android.tools.lint.detector.api.Severity
import org.jetbrains.uast.UCallExpression
import org.jetbrains.uast.UElement
import org.jetbrains.uast.UImportStatement
import org.jetbrains.uast.getQualifiedName

@Suppress("UnstableApiUsage")
class RandomDetector : Detector(), Detector.UastScanner {
    private val kotlinRandomName = kotlin.random.Random::class.java.name
    private val javaRandomName = java.util.Random::class.java.name

    override fun getApplicableUastTypes(): List<Class<out UElement>>? {
        return listOf(UImportStatement::class.java, UCallExpression::class.java)
    }

    override fun createUastHandler(context: JavaContext): UElementHandler? {
        return object : UElementHandler() {
            override fun visitImportStatement(node: UImportStatement) {
                node.importReference?.let { importReference: UElement ->
                    if (importReference.asSourceString().contains(javaRandomName)
                        || importReference.asSourceString().contains(kotlinRandomName)
                    ) {
                        context.report(
                            issue = ISSUE,
                            scope = node,
                            location = context.getLocation(importReference),
                            message = REPORT_MESSAGE
                        )
                    }
                }
            }

            override fun visitCallExpression(node: UCallExpression) {
                if (node.classReference.getQualifiedName()?.contains(kotlinRandomName) == true
                    || node.classReference.getQualifiedName()?.contains(javaRandomName) == true
                ) {
                    context.report(
                        issue = ISSUE,
                        scope = node,
                        location = context.getLocation(node),
                        message = REPORT_MESSAGE
                    )
                }
            }
        }
    }

    companion object {
        private const val REPORT_MESSAGE = "Simple random usage is forbidden."

        private val IMPLEMENTATION = Implementation(
            RandomDetector::class.java,
            Scope.JAVA_FILE_SCOPE
        )

        @JvmField
        val ISSUE: Issue = Issue
            .create(
                id = "RandomDetector",
                briefDescription = "Use java.security.SecureRandom for cryptographically strong randomness.",
                explanation = """
                  Standard JDK implementations of java.util.Random use a Linear Congruential Generator to provide random numbers. The algorithm is that it’s not cryptographically strong, and its outputs could be predicted by attackers. 
                  Always use java.security.SecureRandom in places where we need good security.
                """,
                category = Category.SECURITY,
                priority = 9,
                severity = Severity.ERROR,
                androidSpecific = null,
                implementation = IMPLEMENTATION
            )
    }
}
