# KotlinTezos

[![Release Version](https://img.shields.io/maven-central/v/io.camlcase/kotlintezos.svg?label=release)](http://search.maven.org/#search%7Cga%7C1%7Ckotlintezos)


KotlinTezos is an Android/Kotlin SDK for operating with the [Tezos Blockchain](https://tezos.gitlab.io/tezos/index.html). You can find a practical implementation of this library in [the open-source codebase of the Magma Android app](https://gitlab.com/camlcase-dev/magmawallet-android).

The project can be considered a fork of [TezosKit](https://github.com/keefertaylor/TezosKit) as design, purpose and
 documentation are identical.
 
As of May 2021, this project has been closed down due to lack of funding. **No new features will be added. Any team or individual that wants new development, feel free to fork the library**.

## Getting started

### Installation

The minimum API level for this library is [24](https://developer.android.com/about/versions/nougat/android-7.0.html).

```
implementation 'io.camlcase:kotlintezos:LATEST_VERSION'
```

**Use only stable version 1.8.0 or above**. A [security vulnerability was found](https://camlcase.io/blog/critical-security-update-magma-android-oct-26-2020/) for KotlinTezos version 1.8.0-alpha02 and under.

### Build

To run KotlinTezos, you'll need a Github account and a token to call [the Trust Wallet core dependency through Github packages](https://github.com/trustwallet/wallet-core/packages/700258). Add both in your `local.properties`:
```
github-user=<github_username>
github-token=<github_token>
```

To get a token, go to your _Settings -> Developer Settings -> Personal access tokens_ and generate a new one with the following permissions:
```
read:packages
write:packages
repo
```

Check the GP tokens official instructions [here](https://help.github.com/en/packages/publishing-and-managing-packages/about-github-packages#about-tokens)


## Features

- Wallet generation.
- Get balance, branch and counter
- Send XTZ to an address
- Remote forge with external parsing to verify integrity
- Delegate/undelegate to a baker
- Originate an account
- Activate an account
- Estimate the fees for a given list of operations
- Call to a smart contract
- Call to a [FA1.2 smart contract](https://gitlab.com/camlcase-dev/kotlin-tezos/-/blob/develop/KotlinTezos/src/main/kotlin/io/camlcase/kotlintezos/token/TokenContractClient.kt)
- Call to [Dexter](https://gitlab.com/camlcase-dev/dexter) entrypoints for swapping and adding liquidity.
- Calculate Dexter entrypoint values beforehand using native and/or [centralised code](https://gitlab.com/camlcase-dev/dexter-calculations).
- [Conseil](https://github.com/Cryptonomic/Conseil/wiki/REST-API) client, [TzKt](https://api.tzkt.io/) client and [BCD](https://better-call.dev/docs) client to retrieve information.

### TezosNodeClient

The main class for operating with the node is the `TezosNodeClient` class. For security reasons, it should be provided with two separate node urls (one as base and the other to verify forges):

##### Kotlin

```
val client = TezosNodeClient("NODE_URL", "VERIFIER_NODE_URL")
client.getBalance("ADDRESS", object : TezosCallback<Tez> {
    override fun onSuccess(item: Tez?) { }

    override fun onFailure(error: TezosError) { }
})
```
##### Java
```
TezosCallback<String> callback = new TezosCallback<Tez>() {
    @Override
    public void onSuccess(@Nullable Tez item) { }

    @Override
    public void onFailure(@NotNull TezosError error) { }
};
TezosNodeClient client = new TezosNodeClient("NODE_URL", "VERIFIER_NODE_URL");
client.getBalance("ADDRESS", callback);
```

You can also interact with:
- The Conseil service through the `ConseilClient`.
- Any FA1.2 token through the `TokenContractClient`.
- The Dexter smart contract of a FA1.2 token through the `DexterTokenClient`.

### Wallet

Accounts in Tezos are represented with `Wallet` objects. The library uses [Wallet-core](https://developer.trustwallet.com/wallet-core) for seed generation for the two deterministic wallet paths:
- Sequential or linear deterministic (`SDWallet`).
- Hierarchical deterministic (`HDWallet`) using BIP44 derivative path.

##### Kotlin

```
val listOfWords = listOf("A", "B", "C")
val wallet = HDWallet(listOfWords)
```

##### Java
```
List<String> mnemonic = new ArrayList<>(Arrays.asList("A", "B", "C"));
Wallet wallet = SDWallet.Companion.invoke(mnemonic, "PASSPHRASE");

```

### Smart Contracts

`TezosNodeClient` provides a `call` function to connect with smart contracts. Michelson primitives are represented as `MichelsonParameter` which can be injected into the call.

##### Kotlin

```
val wallet = SDWallet(...)
val amount = Tez(1.0)
val parameter = PairMichelsonParameter(StringMichelsonParameter("KotlinTezos"), IntegerMichelsonParameter(42))
val client = TezosNodeClient(...)

client.call(
    wallet.address,
    "CONTRACT_ADDRESS",
    amount,
    parameter,
    wallet,
    OperationFeesPolicy.Estimate(),
    object : TezosCallback<String> {
        (...)
    })
```

##### Java
```
Wallet wallet = SDWallet.Companion.invoke(...);
Tez tez = Tez(1.0);
MichelsonParameter parameter = new PairMichelsonParameter(
    new StringMichelsonParameter("KotlinTezos", null),
    new IntegerMichelsonParameter(42, null),
    null
);
TezosNodeClient client = new TezosNodeClient(...);
TezosCallback<String> callback = new TezosCallback<String>() {
    (...)
};

client.call(
    wallet.getAddress(),
    "CONTRACT_ADDRESS", 
    Tez.Companion.getZero(), 
    parameter, 
    wallet, 
    new OperationFeesPolicy.Estimate(), 
    callback
    );

```


### Callbacks and Completables

Services and clients return results via an asynchronous `TezosCallback` or Java's [`CompletableFuture`](https://developer.android.com/reference/kotlin/java/util/concurrent/CompletableFuture?hl=en). Internally, the threading is managed with [`ExecutorService`](https://developer.android.com/reference/java/util/concurrent/ExecutorService) that can be passed as an attribute.

## Dependencies

Name | Version | License
--- | --- | ---
**Wallet-core** | `2.6.16` | [MIT License](https://github.com/trustwallet/wallet-core/blob/master/LICENSE)
**OkHttp** | `4.9.1` | [Apache License 2.0](https://github.com/square/okhttp/blob/master/LICENSE.txt)
**Kotlin Futures JDK8** | `1.2.0` | [MIT License](https://github.com/vjames19/kotlin-futures/blob/master/LICENSE)
**LazySodium-Android** | `5.0.2` | [Mozilla Public License 2.0](https://github.com/terl/lazysodium-android/blob/master/LICENSE.md)
**Java Native Access** | `5.7.0` | [Apache License 2.0](https://github.com/java-native-access/jna/blob/master/LICENSE)
**KotlinX Serialization** | `1.2.1` | [Apache License 2.0](https://github.com/Kotlin/kotlinx.serialization/blob/master/LICENSE.txt)
**Rhino** | `1.7.13` | [Mozilla Public License 2.0](https://github.com/mozilla/rhino/blob/master/LICENSE.txt)
**Fat AAR Android** | `1.3.6` | [Apache License 2.0](https://github.com/kezong/fat-aar-android/blob/master/LICENSE)


## Attribution

This project uses source code from these libraries:

Name |  License
--- | --- 
BitcoinJ |  [Apache License 2.0](https://github.com/bitcoinj/bitcoinj/blob/master/COPYING)

## Analysis

The project has custom lint rules to avoid known vulnerabilities. To ensure the codebase's health and security we use the following tooling:

Name |  License  |  Usage
--- | --- | --
Detekt |  [Apache License 2.0](https://github.com/detekt/detekt/blob/main/LICENSE)  |  Static code analysis for Kotlin
DependencyCheck |  [Apache License 2.0](https://github.com/jeremylong/DependencyCheck/blob/main/LICENSE.txt)  |  OWASP dependency-check is a software composition analysis utility that detects publicly disclosed vulnerabilities in application dependencies.

## Contribute

Contributions and bug fixes are appreciated. If you find any bugs or vulnerabilities, please feel free to open an issue or contact us at: [support@camlcase.io](mailto:support@camlcase.io).

## Related camlCase projects:

- [Magma Wallet (Android)](https://gitlab.com/camlcase-dev/magmawallet-android): Mobile Tezos wallet, written in Kotlin.
- [Magma Wallet (iOS)](https://gitlab.com/camlcase-dev/magmawallet-ios): Mobile Tezos wallet, written in Swift.
- [camlKit](https://gitlab.com/camlcase-dev/camlkit): Native Swift library for interacting with the Tezos Blockchain and other camlCase applications, such as Dexter.
- [Tezos RPC Postman](https://gitlab.com/camlcase-dev/tezos-rpc-postman): A tutorial showing how to use the Tezos RPC, by providing developers with a postman collection.

