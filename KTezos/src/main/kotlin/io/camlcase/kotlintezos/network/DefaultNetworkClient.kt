/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.network

import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.parseGeneralErrors
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.Header
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.RPCErrorParser
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Dispatcher
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Standard implementation of [NetworkClient] using a [DefaultClientWrapper].
 *
 * Threading will be delegated to the wrapper's client Executor.
 *
 * @param baseUrl Domain space the client will append paths to
 * @param clientHeaders General headers for the client to use. E.g.: ConseilClient
 */
class DefaultNetworkClient(
    private val baseUrl: String,
    private val clientHeaders: Iterable<Header> = emptyList(),
    private val wrapper: ClientWrapper<OkHttpClient>
) : NetworkClient {
    constructor(baseUrl: String, debug: Boolean = false) : this(
        baseUrl,
        emptyList(),
        DefaultClientWrapper(debug = debug)
    )

    override fun <T> send(rpc: RPC<T>, callback: TezosCallback<T>) {
        send(
            rpc,
            { callback.onSuccess(it) },
            { callback.onFailure(it.wrap(TezosErrorType.RPC_ERROR)) })
    }

    override fun <T> send(rpc: RPC<T>): CompletableFuture<T?> {
        val future = CompletableFuture<T?>()
        send(
            rpc,
            { future.complete(it) },
            { future.completeExceptionally(it.wrap(TezosErrorType.RPC_ERROR)) })
        return future
    }

    private fun <T> send(rpc: RPC<T>, onSuccess: (T?) -> Unit, onError: (Throwable) -> Unit) {
        val request = prepareRequest(rpc)
        try {
            wrapper.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    onError(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    parseResponse(response, rpc.parser, onSuccess, onError)
                }
            })
        } catch (e: Exception) {
            onError(e)
        }
    }

    private fun prepareRequest(rpc: RPC<*>): Request {
        val request = Request.Builder()

        if (rpc.queryParameters != null) {
            request.url(parseQueryParametersIntoUrl(rpc))
        } else {
            request.url(baseUrl + rpc.endpoint)
        }

        clientHeaders.forEach {
            request.addHeader(it.field, it.value)
        }

        rpc.header.forEach {
            request.addHeader(it.field, it.value)
        }

        rpc.payload?.apply {
            // Use RequestBody constructor that doesn't add "; charset=utf-8"
            val body = toByteArray().toRequestBody(Header.JSON_MEDIATYPE)
            request.post(body)
        }

        return request.build()
    }

    private fun <T> parseResponse(
        response: Response,
        parser: Parser<T>,
        onSuccess: (T?) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        // Only get the body once
        val body = response.body?.string()
        if (response.code == 200) {
            try {
                val parsedResponse = parseResponseBody(body, parser)
                onSuccess(parsedResponse)
            } catch (e: Throwable) {
                onError(e)
            }
        } else {
            onError(response.parseError(body))
        }
    }

    private fun <T> parseResponseBody(body: String?, parser: Parser<T>): T? {
        return if (body == null || body.startsWith("null")) {
            null
        } else {
            parser.parse(body)
        }
    }

    private fun parseQueryParametersIntoUrl(rpc: RPC<*>): HttpUrl {
        val httpBuilder = (baseUrl + rpc.endpoint).toHttpUrlOrNull()?.newBuilder()
            ?: throw TezosError(
                TezosErrorType.RPC_ERROR,
                exception = IllegalArgumentException("$baseUrl${rpc.endpoint} is not a valid Http url")
            )
        rpc.queryParameters?.forEach { (key, value) ->
            httpBuilder.addEncodedQueryParameter(key, value)
        }
        return httpBuilder.build()
    }
}

/**
 * Extract a [TezosError] from an OKHttp's [Response]
 */
fun Response.parseError(errorBody: String?): TezosError {
    val rpcErrors = if (errorBody.isNullOrBlank()) {
        emptyList()
    } else {
        RPCErrorParser().parseGeneralErrors(errorBody)
    }

    val counterError = rpcErrors?.firstOrNull {
        it.message?.contains("Counter") == true && it.message.contains("already used for contract")
    }
    val errorType = counterError?.let { TezosErrorType.COUNTER } ?: TezosErrorType.RPC_ERROR

    return TezosError(
        errorType,
        rpcErrors,
        this.code,
        IOException("${this.code}: ${this.message}")
    )
}

/**
 * Provides an OKHttp client implementation to be used.
 *
 * @param executorService to set a custom Dispatcher used to handle asynchronous requests. By default null, so we use OkHttp's default.
 * @param debug Log network calls
 */
class DefaultClientWrapper(
    private val executorService: ExecutorService? = null,
    private val secondsTimeout: Long = 30,
    private val debug: Boolean = false
) : ClientWrapper<OkHttpClient> {
    /**
     * Value of client is lazy loaded to only provide one instance.
     *
     * OkHttp performs best when you create a single OkHttpClient instance and reuse it for all of your HTTP calls. This
     * is because each client holds its own connection pool and thread pools. Reusing connections and threads reduces
     * latency and saves memory. Conversely, creating a client for each request wastes resources on idle pools.
     *
     * @see http://square.github.io/okhttp/3.x/okhttp/okhttp3/OkHttpClient.html
     */
    override val client: OkHttpClient by lazy {
        val builder = defaultClientBuilder(secondsTimeout, debug)
        executorService?.let {
            builder.dispatcher(Dispatcher(it))
        }

        builder.build()
    }
}
