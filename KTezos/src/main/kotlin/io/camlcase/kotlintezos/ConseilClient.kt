/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos

import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.Header
import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType
import io.camlcase.kotlintezos.data.conseil.GetOperationsRPC
import io.camlcase.kotlintezos.data.conseil.GetTransactionType
import io.camlcase.kotlintezos.data.conseil.OperationInfoRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.conseil.ConseilOperation
import io.camlcase.kotlintezos.model.conseil.ConseilTransaction
import io.camlcase.kotlintezos.model.conseil.OperationGroup
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.network.DefaultClientWrapper
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import io.github.vjames19.futures.jdk8.zip
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

/**
 * A client for a Conseil Server.
 */
class ConseilClient(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService = Executors.newCachedThreadPool()
) {

    /**
     * Initialize client with default configuration
     * @param nodeUrl The path to the remote node
     * @param debug Log network calls
     */
    constructor(nodeUrl: String, network: TezosNetwork, apiKey: String, debug: Boolean = false) : this(
        initNetworkClient(nodeUrl, network, apiKey, debug)
    )

    /**
     * Retrieve the operation details of a given operation [hash].
     *
     * Keep in mind it might take a while for the operation to be included. If the callback returns null after ~90 seconds, it can mean the operation was not included.
     * @param callback With [OperationGroup] info. If it's null means the block is not found yet.
     */
    fun getOperationDetails(hash: String, callback: TezosCallback<OperationGroup>) {
        return networkClient.send(OperationInfoRPC(hash), callback)
    }

    /**
     * Retrieve operations sent, received and called from a smart contract including an account.
     *
     * @param account The account to query.
     * @param limit The number of transactions to return, defaults to 100.
     * @param sentTypes Types of operations to query with [account] as source
     * @param receivedTypes Types of operations to query with [account] as destination. By default same as Sent.
     * @param status Transaction statuses to query. By default, all of them.
     */
    fun operations(
        from: Address,
        limit: Int = 100,
        sentTypes: List<OperationType>,
        receivedTypes: List<OperationType> = sentTypes,
        status: List<ConseilPredicateType.OperationStatus> = emptyList(),
        callback: TezosCallback<List<ConseilOperation>>
    ) {
        operationsReceived(from, limit, receivedTypes, status)
            .zip(operationsSent(from, limit, sentTypes, status), executorService) { received, sent ->
                val operations = ArrayList<ConseilOperation>()

                if (!received.isNullOrEmpty()) {
                    operations.addAll(received)
                }
                if (!sent.isNullOrEmpty()) {
                    operations.addAll(sent)
                }
                operations
            }
            .flatMap(executorService) { operations ->
                operationsCalled(from, limit, emptyList(), status)
                    .map(executorService) {
                        if (!it.isNullOrEmpty()) {
                            // Some of the smart contracts operations might be already in the previous search
                            val filteredCalled = it.filter { conseilCalled ->
                                operations.firstOrNull { conseilSearched ->
                                    conseilSearched.operationGroupHash == conseilCalled.operationGroupHash
                                } == null
                            }
                            operations.addAll(filteredCalled)
                        }
                        operations
                    }
            }
            .map(executorService) { operations ->
                operations.sortByDescending { it.timestamp }
                operations.take(limit)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Retrieve transactions both sent and received from an account.
     *
     * @param account The account to query.
     * @param limit The number of transactions to return, defaults to 100.
     * @param status Transaction statuses to query. By default, all of them.
     */
    fun transactions(
        from: Address,
        limit: Int = 100,
        status: List<ConseilPredicateType.OperationStatus> = emptyList(),
        callback: TezosCallback<List<ConseilTransaction>>
    ) {
        val type = listOf(OperationType.TRANSACTION)
        operations(from, limit, type, status = status, callback = object : TezosCallback<List<ConseilOperation>> {
            override fun onSuccess(item: List<ConseilOperation>?) {
                val transactions = item?.mapNotNull { it as? ConseilTransaction }
                callback.onSuccess(transactions)
            }

            override fun onFailure(error: TezosError) {
                callback.onFailure(error)
            }
        })
    }

    /**
     * Retrieve operations received to an account.
     * @param from The account to query.
     * @param limit The number of transactions to return, defaults to 100.
     * @param status Transaction statuses to query. By default, all of them.
     */
    fun operationsReceived(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus> = emptyList(),
        callback: TezosCallback<List<ConseilOperation>>
    ) {
        operationsReceived(from, limit, types, status)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    private fun operationsReceived(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus>
    ): CompletableFuture<List<ConseilOperation>?> {
        return networkClient.send(GetOperationsRPC(from, limit, types, status, getType = GetTransactionType.RECEIVED))
    }

    /**
     * Retrieve operations sent from an account.
     * @param from The account to query.
     * @param limit The number of transactions to return, defaults to 100.
     * @param status Transaction statuses to query. By default, all of them.
     */
    fun operationsSent(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus> = emptyList(),
        callback: TezosCallback<List<ConseilOperation>>
    ) {
        operationsSent(from, limit, types, status)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    private fun operationsSent(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus>
    ): CompletableFuture<List<ConseilOperation>?> {
        return networkClient.send(GetOperationsRPC(from, limit, types, status, getType = GetTransactionType.SENT))
    }

    /**
     * Retrieve smart contract operations where the account is part of the parameters.
     * @param from The account to query.
     * @param limit The number of transactions to return, defaults to 100.
     * @param status Transaction statuses to query. By default, all of them.
     */
    fun operationsCalled(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus> = emptyList(),
        callback: TezosCallback<List<ConseilOperation>>
    ) {
        operationsCalled(from, limit, types, status)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    private fun operationsCalled(
        from: Address,
        limit: Int = 100,
        types: List<OperationType>,
        status: List<ConseilPredicateType.OperationStatus>
    ): CompletableFuture<List<ConseilOperation>?> {
        return networkClient.send(GetOperationsRPC(from, limit, types, status, getType = GetTransactionType.CALL))
    }

    companion object {
        const val CONSEIL_MAINNET_URL = "https://conseil-prod.cryptonomic-infra.tech"
        const val CONSEIL_TESTNET_URL = "https://conseil-dev.cryptonomic-infra.tech"

        private fun initNetworkClient(
            nodeUrl: String,
            network: TezosNetwork,
            apiKey: String,
            debug: Boolean = false
        ): NetworkClient {
            val fullUrl = "$nodeUrl/v2/data/tezos/${network.name.toLowerCase(Locale.US)}"
            return DefaultNetworkClient(
                fullUrl,
                listOf(Header("apiKey", apiKey)),
                DefaultClientWrapper(debug = debug)
            )
        }
    }

}
