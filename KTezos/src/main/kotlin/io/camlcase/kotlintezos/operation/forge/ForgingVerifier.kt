/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.forge

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.data.ParseOperationRPC
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Check validity of operation data. This protects against blind-sig attack.
 */
interface ForgingVerifier {
    /**
     * Use preferred method to parse the forged result and validate against
     *
     * @param operationHash The hash of block (height) of which you want to make the query.
     * @return CompletableFuture with Boolean. True if data is verified, false if it's NOT.
     */
    fun verify(
        blockId: String,
        operationHash: String,
        payload: OperationPayload
    ): CompletableFuture<Boolean>

    /**
     * Strip [parsedPayload] from the "signature" field to check against one sent to /forge.
     * Both arguments will be sorted to ensure they can be compared.
     *
     * @return true if data is verified, false if it's NOT.
     */
    fun verify(forgedPayload: Map<String, Any>, parsedPayload: Map<String, Any?>): Boolean {
        val strippedParsedPayload = parsedPayload.filter { it.key != "signature" }
        return forgedPayload.toSortedMap() == strippedParsedPayload.toSortedMap()
    }
}

/**
 * Implementation using a second, separate node to verify operation contents. This protects against forgery due to a compromised node.
 *
 * @see https://www.reddit.com/r/tzlibre/comments/aezek1/kyctezos_wallets_vulnerable_to_blind_sig_attack/
 */
class RemoteForgingVerifier(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) : ForgingVerifier {
    override fun verify(
        blockId: String,
        operationHash: String,
        payload: OperationPayload
    ): CompletableFuture<Boolean> {
        return networkClient.send(ParseOperationRPC(blockId, operationHash))
            .map(executorService) { forgedPayload ->
                if (forgedPayload == null) {
                    throw TezosError(
                        TezosErrorType.FORGING_ERROR,
                        exception = IllegalArgumentException("[ForgingVerifier] Parsing operations returned null")
                    )
                }
                verify(payload.payload, forgedPayload)
            }
    }
}

