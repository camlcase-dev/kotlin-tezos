/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.fees

import io.camlcase.kotlintezos.model.NanoTez
import io.camlcase.kotlintezos.model.Tez

/**
 * Safety margins that will be added to fee estimations.
 */
object SafetyMargin {
    /**
     * Extra gas units added to the limit of gas.
     * [More info](https://gitlab.com/tezos/tezos/-/issues/995)
     */
    const val GAS = 400
    const val STORAGE = 257
}

/**
 * Maximum values for limits in OperationFees.
 */
object Maximums {
    const val GAS = 800_000
    const val STORAGE = 60_000
}

/**
 * Constants that are used in fee calculations.
 */
@Suppress("MagicNumber")
object FeeConstants {
    /**
     * Minimal fee on docs is 100_000 but is too low for transacting with certain FA1.2 tokens.
     * We double it to ensure enough padding for all kinds of operations
     */
    const val MINIMAL_FEE: NanoTez = 200_000
    const val FEE_PER_GAS_UNIT: NanoTez = 100
    const val FEE_PER_STORAGE_BYTE: NanoTez = 1_000

    /**
     * The allocation is a one time burn, payed by the sender, for the first transaction received by an account.
     * The revelation has a fixed cost of 0.257 ꜩ.
     * [See more](https://tezos.stackexchange.com/a/2165/1953)
     */
    val allocationFee = Tez(0.257)

    /**
     * As per [Protocol 4 updates](https://tezos.gitlab.io/protocols/004_Pt24m4xi.html#gas-and-fees), expected minimum value of fee.
     */
    enum class BakerExpectedFees(val fee: Tez) {
        /**
         * The usual fee is 0.001281 but it can add up to 0.001284 depending if we are sending high amounts of ꜩ.
         */
        TRANSACTION(Tez(0.001_284)),
        ORIGINATION(Tez(0.001_265)),
        DELEGATION(Tez(0.001_257)),
        REVEAL(Tez(0.001_268))
    }
}
