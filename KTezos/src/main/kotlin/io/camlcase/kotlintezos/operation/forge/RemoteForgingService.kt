/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.forge

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade
import io.camlcase.kotlintezos.wallet.crypto.Prefix
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.camlcase.kotlintezos.data.ForgeOperationRPC
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Service for forging [OperationPayload] remotely using two nodes.
 *
 * Supports only operations of type [OperationType] for now.
 *
 * @param networkClient For /forge/operations call.
 * @param verifier Protects against forgery due to a compromised node.
 */
class RemoteForgingService(
    private val networkClient: NetworkClient,
    private val verifier: ForgingVerifier,
    private val executorService: ExecutorService
) {

    /**
     * Use central  [networkClient] for forging and provide a [verifierNodeUrl] to create a
     * new client that will make the unforging calls.
     *
     * @param executorService to manage network requests and calculations asynchronously.
     * @param debug Log network calls
     */
    constructor(
        networkClient: NetworkClient,
        verifierNodeUrl: String,
        executorService: ExecutorService,
        debug: Boolean = false
    ) : this(
        networkClient,
        RemoteForgingVerifier(
            DefaultNetworkClient(verifierNodeUrl, debug),
            executorService
        ),
        executorService
    )

    /**
     * Forge the given operation remotely on a node and validate its contents.
     *
     * @return Completable with a String representing the result of the forge.
     * @throws TezosError [TezosErrorType.FORGING_ERROR] If tampering during the forge is detected.
     */
    fun remoteForge(payload: OperationPayload, metadata: BlockchainMetadata): CompletableFuture<String?> {
        return networkClient.send(ForgeOperationRPC(metadata.blockHash, payload))
            .flatMap(executorService) { verifyForge(it, payload, metadata) }
    }

    /**
     * @throws TezosError [TezosErrorType.FORGING_ERROR] If verification couldn't be completed
     */
    private fun verifyForge(
        result: String?,
        payload: OperationPayload,
        metadata: BlockchainMetadata
    ): CompletableFuture<String?> {
        val (branch, operationHash) = stripBranch(result)
        return verifier.verify(metadata.blockHash, operationHash, payload)
            .map(executorService) {
                if (!it) {
                    throw TezosError(
                        TezosErrorType.FORGING_ERROR,
                        exception = IllegalArgumentException("[ForgingVerifier] Forging couldn't be verified")
                    )
                }
                result
            }
    }

    /**
     * Separates branch and operation forge result.
     *
     * @return (Branch, Forged operation hash)
     * @throws TezosError [TezosErrorType.FORGING_ERROR]
     */
    private fun stripBranch(operation: String?): Pair<String, String> {
        if (operation == null || operation.length <= 64) {
            throw TezosError(
                TezosErrorType.FORGING_ERROR,
                exception = IllegalArgumentException("[RemoteForgingService] Operation hash returned by the forging service is invalid")
            )
        }

        val part = operation.substring(0, 64)
        val decoded = part.hexStringToByteArray()
        val branch = EncoderFacade.encodeCheckWithPrefix(decoded, Prefix.branch)

        return Pair(branch, operation.substring(64))
    }
}
