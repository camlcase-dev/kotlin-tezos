/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.metadata

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.blockchain.GetAddressCounterRPC
import io.camlcase.kotlintezos.data.blockchain.GetAddressManagerKeyRPC
import io.camlcase.kotlintezos.data.blockchain.GetChainHeadRPC
import io.camlcase.kotlintezos.data.blockchain.GetNetworkConstantsRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.function.Function

/**
 * Fetches metadata through a node using [networkClient].
 */
class BlockchainMetadataService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) : MetadataService {
    /**
     * Retrieve metadata needed to forge / pre-apply / sign / inject an operation.
     *
     * @throws [TezosError] through the Completable stream if something goes wrong
     */
    override fun getMetadata(address: Address): CompletableFuture<BlockchainMetadata> {
        val headHashCompletable = getHeadHash()
        val managerKeyCompletable = getManagerKey(address)
        val counterCompletable = getCounter(address)
        val constantsCompletable = getNetworkConstants()
        return bundleOperations(headHashCompletable, managerKeyCompletable, counterCompletable, constantsCompletable)
    }

    /**
     * Handles the three Completables to create a [BlockchainMetadata]
     *
     * @param headHashCompletable As returned by [getHeadHash]
     * @param managerKeyCompletable As returned by [getManagerKey]
     * @param counterCompletable As returned by [counterCompletable]
     *
     * @throws [TezosError] through the Completable stream if any of the completables return a null value
     */
    @Suppress("ComplexCondition")
    fun bundleOperations(
        headHashCompletable: CompletableFuture<Triple<String?, String?, String?>>,
        managerKeyCompletable: CompletableFuture<String?>,
        counterCompletable: CompletableFuture<Int?>,
        constantsCompletable: CompletableFuture<NetworkConstants?>
    ): CompletableFuture<BlockchainMetadata> {
        return CompletableFuture.allOf(
            headHashCompletable,
            counterCompletable,
            managerKeyCompletable,
            constantsCompletable
        )
            .thenApplyAsync(Function {
                val (hash, protocol, chainId) = headHashCompletable.join()
                val counter = counterCompletable.join()
                val key = managerKeyCompletable.join()
                val constants = constantsCompletable.join()
                if (hash == null || protocol == null || chainId == null || counter == null || constants == null) {
                    throw IllegalStateException(
                        "[OperationMetadata] Invalid response for: {hash : $hash}," +
                                "\n{protocol: $protocol},\n{chain_id: $chainId},\n{counter: $counter}" +
                                "\n{constants: $constants}"
                    ).wrap()
                }
                BlockchainMetadata(
                    hash,
                    protocol,
                    chainId,
                    counter,
                    key,
                    constants
                )
            }, executorService)
    }

    /**
     * Retrieve chain info counter for the given address.
     * @return CompletableFuture of Triple of nullable (hash, protocol, chain id).
     */
    override fun getHeadHash(): CompletableFuture<Triple<String?, String?, String?>> {
        return networkClient.send(GetChainHeadRPC())
            .map {
                val hash = it?.get("hash") as? String
                val chainId = it?.get("chain_id") as? String
                val protocol = it?.get("protocol") as? String
                Triple(hash, protocol, chainId)
            }
    }

    override fun getCounter(address: Address): CompletableFuture<Int?> {
        return networkClient.send(GetAddressCounterRPC(address))
    }

    /**
     * Retrieve the base58check encoded public key for the given address.
     */
    override fun getManagerKey(address: Address): CompletableFuture<String?> {
        return networkClient.send(GetAddressManagerKeyRPC(address))
    }

    override fun getNetworkConstants(): CompletableFuture<NetworkConstants?> {
        return networkClient.send(GetNetworkConstantsRPC())
    }
}
