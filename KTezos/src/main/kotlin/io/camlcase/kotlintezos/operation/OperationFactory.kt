/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation

import io.camlcase.kotlintezos.exchange.DexterEntrypoint
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.*
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.IntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import java.math.BigInteger

/**
 * A factory which produces operations
 */
object OperationFactory {

    /**
     * Given a [type], returns an [Operation]
     *
     * @return null if [params] are not the correct instantiation for that operation
     */
    fun createOperation(
        type: OperationType,
        params: OperationParams,
        fees: OperationFees
    ): Operation? {
        return when (type) {
            OperationType.TRANSACTION -> {
                (params as? OperationParams.Transaction)?.let {
                    return createTransactionOperation(it, fees)
                }
                (params as? OperationParams.ContractCall)?.let {
                    return createSmartContractOperation(it, fees)
                }
            }
            OperationType.DELEGATION ->
                (params as? OperationParams.Delegation)?.let {
                    return createDelegationOperation(it, fees)
                }
            OperationType.REVEAL -> {
                (params as? OperationParams.Reveal)?.let {
                    return createRevealOperation(it, fees)
                }
            }
            OperationType.ORIGINATION -> {
                (params as? OperationParams.Origination)?.let {
                    return createOriginationOperation(it, fees)
                }
            }
        }
    }

    private fun createSmartContractOperation(
        params: OperationParams.ContractCall,
        fees: OperationFees
    ): SmartContractCallOperation {
        return SmartContractCallOperation(
            params.amount,
            params.from,
            params.to,
            fees,
            params.parameter,
            params.entrypoint
        )
    }

    private fun createTransactionOperation(
        params: OperationParams.Transaction,
        fees: OperationFees
    ): TransactionOperation {
        return TransactionOperation(
            params.amount,
            params.from,
            params.to,
            fees
        )
    }

    private fun createOriginationOperation(
        params: OperationParams.Origination,
        fees: OperationFees
    ): OriginationOperation {
        return OriginationOperation(params.source, fees, params.script)
    }

    private fun createDelegationOperation(
        params: OperationParams.Delegation,
        fees: OperationFees
    ): DelegationOperation {
        return DelegationOperation(params.source, params.delegate, fees)
    }

    private fun createRevealOperation(params: OperationParams.Reveal, fees: OperationFees): RevealOperation {
        return RevealOperation(params.source, params.publicKey, fees)
    }

    /**
     * Given a [type], returns a list of [Operation] for Token functionality
     *
     * @return null if [params] are not the correct instantiation for that operation
     * @see TokenOperationType
     */
    fun createTokenOperation(
        type: TokenOperationType,
        params: TokenOperationParams,
        fees: OperationFees
    ): List<SmartContractCallOperation>? {
        return when (type) {
            TokenOperationType.TRANSFER -> {
                (params as? TokenOperationParams.Transfer)?.let {
                    return createTokenTransferOperations(it, fees)
                }
            }
            TokenOperationType.TOKEN_TO_TEZ -> {
                (params as? TokenOperationParams.TokenToTez)?.let {
                    return createTokenToTezOperation(it, fees)
                }
            }
            TokenOperationType.TEZ_TO_TOKEN -> {
                (params as? TokenOperationParams.TezToToken)?.let {
                    return createTezToTokenOperation(it, fees)
                }
            }
            TokenOperationType.ADD_LIQUIDITY -> {
                (params as? TokenOperationParams.AddLiquidity)?.let {
                    return createAddLiquidityOperation(it, fees)
                }
            }
        }

    }

    private fun createAddLiquidityOperation(
        params: TokenOperationParams.AddLiquidity,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        val approveOperations =
            createApproveOperations(
                params.from,
                params.tokenAddress,
                params.dexterAddress,
                params.dexterAllowance,
                params.minLiquidity,
                fees
            )

        val parameter = PairMichelsonParameter(
            PairMichelsonParameter(
                StringMichelsonParameter(params.from),
                BigIntegerMichelsonParameter(params.minLiquidity)
            ),
            PairMichelsonParameter(
                BigIntegerMichelsonParameter(params.maxToken),
                StringMichelsonParameter(params.deadline)
            )
        )

        val operation = SmartContractCallOperation(
            params.amount,
            params.from,
            params.dexterAddress,
            fees,
            parameter,
            DexterEntrypoint.ADD_LIQUIDITY.entrypoint
        )

        val operations = approveOperations.toMutableList()
        operations.add(operation)
        return operations
    }

    /**
     * List all the operations needed to approve the transfer of [tokenAmount] through DEXter.
     *
     * @param tokenContract Tezos address for that FA1.2 token
     * @param dexterContract Tezos address for the Dexter smart contract of [tokenContract]
     * @param dexterAllowance The current number of allowance DEXter has for that token.
     */
    private fun createApproveOperations(
        from: Address,
        tokenContract: Address,
        dexterContract: Address,
        dexterAllowance: BigInteger,
        tokenAmount: BigInteger,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        if (dexterAllowance >= tokenAmount) {
            // No approve operations necessary
            return emptyList()
        }

        val approveParameter = PairMichelsonParameter(
            StringMichelsonParameter(dexterContract),
            BigIntegerMichelsonParameter(tokenAmount)
        )
        val approval = SmartContractCallOperation(
            Tez.zero,
            from,
            tokenContract,
            fees,
            approveParameter,
            SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE
        )

        return if (dexterAllowance.compareTo(BigInteger.ZERO) == 0) {
            listOf(approval)
        } else {
            // when the approval is not zero, set it to zero first, then the target value
            val resetParameter = PairMichelsonParameter(
                StringMichelsonParameter(dexterContract),
                IntegerMichelsonParameter(0)
            )

            val resetApproval = SmartContractCallOperation(
                Tez.zero,
                from,
                tokenContract,
                fees,
                resetParameter,
                SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE
            )
            listOf(resetApproval, approval)
        }
    }

    /**
     * Operations needed to send token from one account to another
     */
    private fun createTokenTransferOperations(
        params: TokenOperationParams.Transfer,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        // Send operation
        val sendParameter = PairMichelsonParameter(
            StringMichelsonParameter(params.from),
            PairMichelsonParameter(
                StringMichelsonParameter(params.to),
                BigIntegerMichelsonParameter(params.amount)
            )
        )
        val sendOperation = SmartContractCallOperation(
            Tez.zero,
            params.from,
            params.tokenAddress,
            fees,
            sendParameter,
            SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_TRANSFER
        )

        return listOf(sendOperation)
    }

    /**
     * Operation for [TokenOperationType.TOKEN_TO_TEZ]
     */
    private fun createTokenToTezOperation(
        params: TokenOperationParams.TokenToTez,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        val tezAmount: BigInteger = try {
            params.amount.stringRepresentation.toBigInteger()
        } catch (e: NumberFormatException) {
            throw TezosError(TezosErrorType.INTERNAL_ERROR, exception = e)
        }

        val parameter = PairMichelsonParameter(
            PairMichelsonParameter(
                StringMichelsonParameter(params.owner),
                StringMichelsonParameter(params.destination)
            ),
            PairMichelsonParameter(
                BigIntegerMichelsonParameter(params.tokenAmount),
                PairMichelsonParameter(
                    BigIntegerMichelsonParameter(tezAmount),
                    StringMichelsonParameter(params.deadline)
                )
            )
        )

        val operation = SmartContractCallOperation(
            Tez.zero,
            params.from,
            params.dexterAddress,
            fees,
            parameter,
            DexterEntrypoint.TOKEN_TO_TEZ.entrypoint
        )

        return if (params.needsApproval) {
            val approveOperation =
                createApproveOperations(
                    params.from,
                    params.tokenAddress,
                    params.dexterAddress,
                    params.dexterAllowance,
                    params.tokenAmount,
                    fees
                )
            val operations = approveOperation.toMutableList()
            operations.add(operation)
            operations
        } else {
            listOf(operation)
        }
    }

    /**
     * Operation for [TokenOperationType.TEZ_TO_TOKEN]
     */
    private fun createTezToTokenOperation(
        params: TokenOperationParams.TezToToken,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        val parameter = PairMichelsonParameter(
            StringMichelsonParameter(params.destination),
            PairMichelsonParameter(
                BigIntegerMichelsonParameter(params.tokenAmount),
                StringMichelsonParameter(params.deadline)
            )
        )
        val operation = SmartContractCallOperation(
            params.amount,
            params.source,
            params.dexterAddress,
            fees,
            parameter,
            DexterEntrypoint.TEZ_TO_TOKEN.entrypoint
        )
        return listOf(operation)
    }
}
