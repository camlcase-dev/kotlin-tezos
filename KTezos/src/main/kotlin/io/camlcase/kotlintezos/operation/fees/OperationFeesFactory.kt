/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.fees

import io.camlcase.kotlintezos.core.ext.badArgumentsError
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationParams
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.SimulationService
import io.github.vjames19.futures.jdk8.ImmediateFuture
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.toCompletableFuture
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Given an [OperationFeesPolicy], returns an [OperationFees] to use to run the given operation(s)
 */
class OperationFeesFactory(
    private val feeEstimator: FeeEstimatorService
) {
    constructor(
        executorService: ExecutorService,
        simulationService: SimulationService,
        forgingService: ForgingService
    ) : this(FeeEstimatorService(executorService, simulationService, forgingService))

    fun calculateFees(
        type: OperationType,
        params: OperationParams,
        from: Address,
        operationFeePolicy: OperationFeesPolicy,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        return when (operationFeePolicy) {
            is OperationFeesPolicy.Default -> ImmediateFuture {
                DefaultFeeEstimator.calculateFees(metadata.tezosProtocol, type)
            }
            is OperationFeesPolicy.Custom -> ImmediateFuture { operationFeePolicy.fees }
            is OperationFeesPolicy.Estimate -> {
                OperationFactory.createOperation(type, params, OperationFees.simulationFees)?.let { operation ->
                    feeEstimator.estimate(listOf(operation), from, metadata, signatureProvider)
                        .map { it.accumulatedFees }
                }
                    ?: return badArgumentsError("[FeeEstimator] Invalid parameters: { $type, $params } to create an operation")
                        .toCompletableFuture()
            }
        }
    }

    fun calculateFees(
        operation: Operation,
        from: Address,
        operationFeePolicy: OperationFeesPolicy,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        return when (operationFeePolicy) {
            is OperationFeesPolicy.Default -> ImmediateFuture {
                DefaultFeeEstimator.calculateFees(metadata.tezosProtocol, operation.type)
            }
            is OperationFeesPolicy.Custom -> ImmediateFuture { operationFeePolicy.fees }
            is OperationFeesPolicy.Estimate -> {
                feeEstimator.estimate(listOf(operation), from, metadata, signatureProvider)
                    .map { it.accumulatedFees }
            }
        }
    }

    /**
     * Return fees for several operations.
     * @return [CalculatedFees] in a future.
     */
    fun calculateFees(
        operations: List<Operation>,
        from: Address,
        operationFeePolicy: OperationFeesPolicy,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<CalculatedFees> {
        return when (operationFeePolicy) {
            is OperationFeesPolicy.Default -> ImmediateFuture {
                DefaultFeeEstimator.calculateFees(metadata.tezosProtocol, operations)
            }
            is OperationFeesPolicy.Custom -> ImmediateFuture { CalculatedFees(listOf(operationFeePolicy.fees)) }
            is OperationFeesPolicy.Estimate -> {
                feeEstimator.estimate(operations, from, metadata, signatureProvider)
            }
        }
    }
}
