/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet

import io.camlcase.kotlintezos.core.ext.toJson
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.SigningResponse
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.model.operation.payload.SignedOperationPayload
import io.camlcase.kotlintezos.model.operation.payload.SignedProtocolOperationPayload
import io.camlcase.kotlintezos.wallet.crypto.toHexString


/**
 * An opaque object which implements public key cryptography functions.
 */
interface SignatureProvider {
    val publicKey: PublicKey
    fun sign(hex: String): ByteArray?
}

/**
 * A signature provider that does nothing.
 */
class SimulatedSignatureProvider : SignatureProvider {
    override val publicKey: PublicKey = PublicKey(simulationSignature)

    override fun sign(hex: String): ByteArray? {
        return null
    }

    companion object {
        private const val SIGNATURE_LENGTH = 64
        val simulationSignature: ByteArray = ByteArray(SIGNATURE_LENGTH)
    }

}

/**
 * Manages signing of operations.
 */
internal object SigningService {
    /**
     * Sign an operation payload
     * @throws TezosError If signing could not be completed
     */
    fun sign(
        operationPayload: OperationPayload,
        metadata: BlockchainMetadata,
        forgeResult: String?,
        signatureProvider: SignatureProvider
    ): SigningResponse {
        if (forgeResult.isNullOrBlank()) {
            throw TezosError(
                TezosErrorType.SIGNING_ERROR,
                exception = IllegalArgumentException("No valid forge result for signing")
            )
        }

        try {
            val signature = signatureProvider.sign(forgeResult)
            val signatureHex = signature!!.toHexString()
            val signedBytesForInjection = (forgeResult + signatureHex).toJson()!!
            val signedOperationPayload = SignedOperationPayload(operationPayload, metadata.chainId, signature)
            val signedProtocolOperationPayload =
                SignedProtocolOperationPayload(signedOperationPayload, metadata.protocol)

            return SigningResponse(signedBytesForInjection, signedProtocolOperationPayload)
        } catch (e: Exception) {
            throw TezosError(TezosErrorType.SIGNING_ERROR, exception = e)
        }
    }
}
