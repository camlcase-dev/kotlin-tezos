/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet

import io.camlcase.kotlintezos.wallet.crypto.EllipticCurve
import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade
import io.camlcase.kotlintezos.wallet.crypto.KeyFormat
import io.camlcase.kotlintezos.wallet.crypto.Prefix
import io.camlcase.kotlintezos.wallet.crypto.SodiumFacade
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.camlcase.kotlintezos.wallet.crypto.watermarkAndHash
import javax.crypto.SecretKey

/**
 * Encapsulation of a secret key.
 */
data class SecretKey(
    val bytes: ByteArray
) : SecretKey {
    /**
     * Base58Check representation of the key, prefixed with 'edsk'.
     */
    val base58Representation: String
        get() {
            return EncoderFacade.encodeCheckWithPrefix(bytes, Prefix.edsk)
        }

    override fun getAlgorithm(): String {
        return EllipticCurve.ED25519.name
    }

    override fun getEncoded(): ByteArray {
        return bytes
    }

    override fun getFormat(): String {
        return KeyFormat.BASE58.name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as io.camlcase.kotlintezos.wallet.SecretKey

        if (!bytes.contentEquals(other.bytes)) return false
        if (base58Representation != other.base58Representation) return false

        return true
    }

    override fun hashCode(): Int {
        return bytes.contentHashCode()
    }

    /**
     * Sign the given hexadecimal encoded string with this secret key.
     * @param hex The string to sign.
     * @return A signature from the input.
     */
    fun sign(hex: String): ByteArray? {
        return sign(hex.hexStringToByteArray())
    }

    /**
     * Sign the given [bytes] with this secret key.
     */
    fun sign(bytes: ByteArray): ByteArray? {
        return bytes.watermarkAndHash()?.let { bytesToSign ->
            SodiumFacade.sign(bytesToSign, this.bytes)
        }
    }

    companion object {
        /**
         * Constructor: Initialize a key with the given hex seed string.
         */
        operator fun invoke(seedString: String) = run {
            try {
                val seed = seedString.hexStringToByteArray()
                SodiumFacade.createSecretKey(seed)?.let {
                    SecretKey(it)
                }
            } catch (e: NumberFormatException) {
                return null
            }
        }
    }
}
