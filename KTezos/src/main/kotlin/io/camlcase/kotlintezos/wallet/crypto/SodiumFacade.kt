/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet.crypto

import com.goterl.lazysodium.LazySodiumAndroid
import com.goterl.lazysodium.SodiumAndroid
import com.goterl.lazysodium.exceptions.SodiumException
import com.goterl.lazysodium.interfaces.Sign
import com.goterl.lazysodium.utils.KeyPair

/**
 * Wrapper for Libsodium cryptographic utils.
 */
object SodiumFacade {
    private val sodiumProvider = LazySodiumAndroid(SodiumAndroid())

    /**
     *  Create a pair of keys with the given hex seed string.
     *  @return null if error creating the pair
     */
    fun keyPair(seed: ByteArray): KeyPair? {
        return try {
            sodiumProvider.cryptoSignSeedKeypair(seed)
        } catch (e: SodiumException) {
            null
        }
    }

    /**
     * Create a secret key with the given hex seed string.
     * @return null if error creating the pair
     */
    fun createSecretKey(seed: ByteArray): ByteArray? {
        return keyPair(seed)?.secretKey?.asBytes
    }

    /**
     * Hash a specific [bytes] array to an [outputLenght]
     */
    fun hash(bytes: ByteArray, outputLenght: Int): ByteArray? {
        val hash = sodiumProvider.randomBytesBuf(outputLenght)
        val hashed = sodiumProvider.cryptoGenericHash(hash, hash.size, bytes, bytes.size.toLong())
        return if (hashed) {
            return hash
        } else {
            null
        }
    }

    /**
     * Sign the given [message] bytes with the given [secretKeyBytes].
     */
    fun sign(message: ByteArray, secretKeyBytes: ByteArray): ByteArray? {
        val signedMessage = sodiumProvider.randomBytesBuf(message.size + Sign.BYTES)
        val signed = sodiumProvider.cryptoSign(signedMessage, message, message.size.toLong(), secretKeyBytes)
        return if (signed) {
            signedMessage.sliceArray(0..63)
        } else {
            null
        }
    }

    /**
     * Verifies the detached signature of a message with the sender's public key.
     */
    fun verify(message: ByteArray, publicKeyBytes: ByteArray, signature: ByteArray): Boolean {
        return sodiumProvider.cryptoSignVerifyDetached(signature, message, message.size, publicKeyBytes)
    }
}
