/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.wallet.crypto.TrustWalletFacade
import io.camlcase.kotlintezos.wallet.crypto.get32Hex
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.camlcase.kotlintezos.wallet.crypto.watermarkAndHash
import wallet.core.jni.CoinType
import wallet.core.jni.Curve
import wallet.core.jni.HDWallet
import wallet.core.jni.PrivateKey
import java.util.regex.Pattern

/**
 * A representation of a wallet in the Tezos ecosystem using a Hierarchical Deterministic approach.
 *
 * It's generated using [BIP44_DERIVATION_PATH].
 *
 * @param coreWallet Wallet-core model where the seed is saved. Generates private and public keys.
 */
data class HDWallet(
    private val coreWallet: HDWallet,
    private val derivationPath: String = DEFAULT_TEZOS_BIP44_DERIVATION_PATH
) : Wallet {
    /**
     * List of english words used to generate the seed of the Wallet.
     */
    override val mnemonic: List<String> = coreWallet.mnemonic().split(" ")

    /**
     * Default constructor.
     * @param mnemonic List of english words used to generate the seed of the Wallet.
     * If null or empty, it will generate a mnemonic with strength of [TrustWalletFacade.DEFAULT_MNEMONIC_STRENGTH].
     * @param passphrase Optional password to be used with [mnemonic] to generate the seed.
     * If no mnemonic given, it will used to generate a default one.
     */
    constructor(mnemonic: List<String>? = null, passphrase: String? = null, derivationPath: String? = null) : this(
        TrustWalletFacade().loadCoreWallet(
            mnemonic,
            passphrase
        ),
        derivationPath?.sanitiseDerivationPath() ?: DEFAULT_TEZOS_BIP44_DERIVATION_PATH
    )

    override val mainAddress: Address = address(derivationPath)

    /**
     * Address generated using sequential deterministic logic instead of hierarchical as if it were a [SDWallet].
     */
    val linearAddress: Address?
        get() {
            val secretKey = SecretKey(seedString = coreWallet.seed().get32Hex())
            val publicKey = PublicKey(secretKey)
            return publicKey?.hash
        }

    fun address(accountIndex: Int): Address {
        val indexedDerivationPath = parseDerivationPathWith(accountIndex)
        return address(indexedDerivationPath)
    }

    fun address(derivationPath: String): Address {
        val key = coreWallet.getKey(CoinType.TEZOS, derivationPath.sanitiseDerivationPath())
        return CoinType.TEZOS.deriveAddress(key)
    }

    @Suppress("MagicNumber")
    private fun parseDerivationPathWith(accountIndex: Int): String {
        val first = TEZOS_BIP44_DERIVATION_PATH_PREFIX + accountIndex

        // Get the account type, change and index
        val split = derivationPath.split(TEZOS_BIP44_DERIVATION_PATH_PREFIX)
        if (split.size < 2 && split[1].length < 4) {
            throw TezosError(
                TezosErrorType.WALLET_CREATION,
                exception = IllegalArgumentException("The given derivation path for this wallet is not valid to generate the key: $derivationPath")
            )
        }
        // Drop the account index, "first" already has it.
        val second = split[1].drop(1)
        return first + second
    }

    override val publicKey: PublicKey
        get() {
            val key = derivedKey().publicKeyEd25519
            return PublicKey(key.data())
        }

    val secretKey: SecretKey
        get() {
            return SecretKey(derivedKey().data())
        }

    override fun sign(hex: String): ByteArray? {
        val bytesToSign = hex.hexStringToByteArray().watermarkAndHash()
            ?: throw TezosError(
                TezosErrorType.SIGNING_ERROR,
                exception = IllegalArgumentException("The given hexadecimal string could not be watermarked and hashed.")
            )
        return coreWallet.getKeyForCoin(CoinType.TEZOS).sign(bytesToSign, Curve.ED25519)
    }

    private fun derivedKey(): PrivateKey {
        return coreWallet.getKey(CoinType.TEZOS, derivationPath)
    }

    companion object {
        const val DEFAULT_TEZOS_BIP44_DERIVATION_PATH = "m/44\'/1729\'/0\'/0\'"
        const val TEZOS_BIP44_REGEX = "m\\/44'\\/1729'\\/\\d*'?\\/\\d'?(\\/\\d*'?)?"
        private const val TEZOS_BIP44_DERIVATION_PATH_PREFIX = "m/44\'/1729\'/"

        /**
         * @throws TezosError If [String] does not conform to the Bip44 Tezos derivation path.
         */
        private fun String.sanitiseDerivationPath(): String {
            if (!validDerivationPath(this)) {
                throw TezosError(
                    TezosErrorType.WALLET_CREATION,
                    exception = IllegalArgumentException("$this is not a valid derivation path for Tezos")
                )
            }

            return if (!this.contains("\'")) {
                this.replace("'", "\"")
            } else {
                this
            }
        }

        /**
         * @return true if [path] conforms to the Tezos BIP44 derivation path.
         */
        fun validDerivationPath(path: String): Boolean {
            return Pattern.compile(TEZOS_BIP44_REGEX)
                .matcher(path)
                .matches()
        }
    }
}

