/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet

import io.camlcase.kotlintezos.wallet.crypto.EllipticCurve
import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade.encodeCheckWithPrefix
import io.camlcase.kotlintezos.wallet.crypto.KeyFormat
import io.camlcase.kotlintezos.wallet.crypto.Prefix
import io.camlcase.kotlintezos.wallet.crypto.SodiumFacade
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.camlcase.kotlintezos.wallet.crypto.watermarkAndHash

/**
 * Encapsulation of a Public Key.
 * @param bytes Underlying bytes.
 */
data class PublicKey(
    val bytes: ByteArray
) : java.security.PublicKey {

    /**
     * Public key hash representation of the key.
     * Will be null if hash couldn't be calculated.
     */
    val hash: String?
        get() {
            SodiumFacade.hash(bytes, 20)?.let {
                // For EllipticCurve.ED25519
                return encodeCheckWithPrefix(it, Prefix.tz1)
            }
            return null
        }
    /**
     * Base58Check representation of the key, prefixed with 'edpk'.
     */
    val base58Representation: String
        get() {
            return encodeCheckWithPrefix(bytes, Prefix.edpk)
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PublicKey

        if (!bytes.contentEquals(other.bytes)) return false
        if (hash != other.hash) return false

        return true
    }

    override fun getFormat(): String {
        return KeyFormat.BASE58.name
    }

    override fun getAlgorithm(): String {
        return EllipticCurve.ED25519.name // Not supported in Java yet
    }

    override fun getEncoded(): ByteArray {
        return bytes
    }

    override fun hashCode(): Int {
        var result = bytes.contentHashCode()
        result = 31 * result + hash.hashCode()
        return result
    }

    /**
     * Verify that the given [signature] matches the given input [hex].
     */
    fun verify(signature: ByteArray, hex: String): Boolean {
        return verify(signature, hex.hexStringToByteArray())
    }

    /**
     * Verify that the given [signature] matches the given input [bytes].
     *
     * @param bytes The bytes to check.
     * @param signature The proposed signature of the bytes.
     */
    fun verify(signature: ByteArray, bytes: ByteArray): Boolean {
        return bytes.watermarkAndHash()?.let {
            // For EllipticalCurve.ED25519
            return SodiumFacade.verify(it, this.bytes, signature)
        } ?: false
    }

    companion object {
        /**
         * Constructor: Initialize a key from the given secret key
         */
        operator fun invoke(secretKey: SecretKey?) = run {
            secretKey?.let {
                PublicKey(secretKeyBytes = secretKey.bytes)
            }
        }

        /**
         * Constructor: Initialize a key from the given secret key bytes
         */
        operator fun invoke(secretKeyBytes: ByteArray) = run {
            val end = secretKeyBytes.size - 1
            val byte: ByteArray = secretKeyBytes.sliceArray(32..end)
            PublicKey(byte)
        }
    }
}
