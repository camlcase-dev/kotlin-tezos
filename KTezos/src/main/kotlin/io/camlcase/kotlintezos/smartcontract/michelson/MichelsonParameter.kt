/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.smartcontract.michelson

import io.camlcase.kotlintezos.data.Payload

/**
 * An abstract representation of a Michelson param.
 */
interface MichelsonParameter : Payload

/**
 * String constants used in Micheline param JSON encoding.
 */
enum class MichelineConstants(val rpcName: String) {
    ANNOTATIONS("annots"),
    ARGS("args"),
    PRIMITIVE("prim"),
    BYTES("bytes"),
    INTEGER("int"),
    LEFT("Left"),
    FALSE("False"),
    NONE("None"),
    PAIR("Pair"),
    RIGHT("Right"),
    SOME("Some"),
    STRING("string"),
    TRUE("True"),
    UNIT("Unit");

    companion object {
        fun get(name: String): MichelineConstants {
            return values().firstOrNull { it.rpcName == name } ?: NONE
        }
    }
}

/**
 * A comparable Michelson type.
 */
enum class MichelsonComparable {
    PRIM,
    ADDRESS,
    BOOL,
    BYTES,
    INT,
    KEY_HASH,
    MUTEZ,
    NAT,
    STRING,
    TIMESTAMP;
    companion object {
        fun get(name: String): MichelsonComparable {
            return values().firstOrNull { it.name.equals(name, true) } ?: PRIM
        }
    }
}
