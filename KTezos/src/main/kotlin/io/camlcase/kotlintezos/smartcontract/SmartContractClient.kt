/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.smartcontract

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationParams
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.wallet.SignatureProvider

/**
 * Protocol to call on smart contracts
 */
interface SmartContractClient {
    /**
     * Call a smart contract.
     *
     * @param source The address invoking the contract.
     * @param contract The smart contract to invoke.
     * @param amount The amount of Tez to transfer with the invocation. Default is 0.
     * @param parameter An optional parameter to send to the smart contract. Default is none.
     * @param entrypoint An optional entrypoint to use for the transaction. If nil, the default entry point is used.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source].
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun call(
        source: Address,
        contract: Address,
        amount: Tez = Tez.zero,
        parameter: MichelsonParameter? = null,
        entrypoint: String? = null,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    )

    /**
     * Inspect the value of a big map in a smart contract.
     * @param address The address of a smart contract with a big map.
     * @param key The key in the big map to look up.
     */
    fun getBigMapValue(
        address: Address,
        key: MichelsonParameter,
        type: MichelsonComparable,
        callback: TezosCallback<Map<String, Any?>>
    )

    /**
     * Retrieve the data of a smart contract.
     * @param address The address of the smart contract to inspect.
     * @param callback Completion callback which will be called with the storage.
     */
    fun getContractStorage(address: Address, callback: TezosCallback<Map<String, Any?>>)

    /**
     * Runs a given type of operation.
     *
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun runContractOperation(
        source: Address,
        params: OperationParams.ContractCall,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    )

    /**
     * Runs a given list of operations.
     *
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun runContractOperations(
        operations: List<SmartContractCallOperation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    )

    /**
     * Returns the fees calculated for the given [operations] using the [feesPolicy]
     *
     * @param signatureProvider The object which will sign the operations if [feesPolicy] is Estimate
     * @param callback With an [CalculatedFees] object
     */
    fun calculateFees(
        operations: List<Operation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy,
        callback: TezosCallback<CalculatedFees>
    )

    /**
     * Retrieve the balance of [Tez] of a given [Address]
     */
    fun getBalance(address: Address, callback: TezosCallback<Tez>)

    /**
     * Dry-run a list of operations. It will use an empty [SignatureProvider] and won't check for reveals.
     *
     * @param source To query metadata
     * @param signatureProvider In case of reveal, provides the public key.
     */
    fun simulateOperations(
        operations: List<SmartContractCallOperation>,
        source: Address,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<SimulationResponse>
    )
}
