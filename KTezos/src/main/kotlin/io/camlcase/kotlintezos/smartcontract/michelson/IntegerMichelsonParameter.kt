/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.smartcontract.michelson

import java.math.BigInteger

/**
 * A representation of an integer parameter in Michelson.
 */
data class IntegerMichelsonParameter(
    val value: Int,
    override val annotations: List<MichelsonAnnotation>? = null
) : BaseMichelsonParameter {
    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            payload[MichelineConstants.INTEGER.rpcName] = value.toString()
            return payload
        }
}

/**
 * A representation of an integer parameter in Michelson.
 *
 * As FA1.2 token value can surpass a Java integer value, we use a safer format.
 */
data class BigIntegerMichelsonParameter(
    val value: BigInteger,
    override val annotations: List<MichelsonAnnotation>? = null
) : BaseMichelsonParameter {
    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            payload[MichelineConstants.INTEGER.rpcName] = value.toString()
            return payload
        }
}
