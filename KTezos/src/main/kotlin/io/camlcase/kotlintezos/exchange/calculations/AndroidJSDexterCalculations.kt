/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.exchange.calculations

import io.camlcase.kotlintezos.core.js.JavascriptContext
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import java.math.BigDecimal
import java.math.BigInteger

/**
 * Uses the Dexter-calculations JS library to get different important calculations for Dexter.
 * The library is used by the official Dexter website and should be considered the best source of truth for use of the smart contracts.
 *
 * Important: JS floating point precision for long decimals [has a known bug](https://stackoverflow.com/questions/1458633/how-to-deal-with-floating-point-number-precision-in-javascript) and might be imprecise.
 *
 * For non-Android usage, you can refer to the native implementation [NativeDexterCalculations].
 *
 * [See more](https://gitlab.com/camlcase-dev/dexter-calculations)
 */
@Suppress("TooManyFunctions")
class AndroidJSDexterCalculations(
    private val jsContext: JavascriptContext
) : DexterCalculations {

    /**
     * Load the context and the file at the beginning. This ensures faster call of functions.
     * Remember to call [closeReading] when finished to avoid leaks!
     */
    fun initReading() {
        jsContext.loadFile(DEXTER_CALCULATIONS_FILE_NAME)
    }

    /**
     * [jsContext] is loaded at the beginning to improve calling times but [closeReading] should be
     * called when process is finished to avoid leaks.
     */
    fun closeReading() {
        jsContext.finish()
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun xtzToTokenTokenOutput(
        xtzIn: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigInteger {
        val tokenOut =
            callJS(
                functionName = XTZ_TO_TOKEN_FUNCTION_NAME,
                params = arrayOf(
                    xtzIn.stringRepresentation,
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return BigInteger(tokenOut)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun xtzToTokenExchangeRate(
        xtzIn: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val exchangeRate =
            callJS(
                functionName = XTZ_TO_TOKEN_EXCHANGE_RATE_FUNCTION_NAME,
                params = arrayOf(
                    xtzIn.stringRepresentation,
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return BigDecimal(exchangeRate)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun xtzToTokenMarketRate(xtzPool: Tez, tokenPool: BigInteger, decimals: Int): BigDecimal {
        val marketRate =
            callJS(
                functionName = XTZ_TO_TOKEN_MARKET_RATE_FUNCTION_NAME,
                params = arrayOf(
                    xtzPool.stringRepresentation,
                    tokenPool.toString(),
                    decimals.toString()
                )
            )
        return BigDecimal(marketRate)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun xtzToTokenPriceImpact(xtzIn: Tez, xtzPool: Tez, tokenPool: BigInteger): BigDecimal {
        val marketRate =
            callJS(
                functionName = XTZ_TO_TOKEN_PRICE_IMPACT_FUNCTION_NAME,
                params = arrayOf(
                    xtzIn.stringRepresentation,
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return BigDecimal(marketRate)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun xtzToTokenMinimumTokenOutput(
        tokenOut: BigInteger,
        allowedSlippage: Double
    ): BigInteger {
        val out =
            callJS(
                functionName = XTZ_TO_TOKEN_MINIMUM_OUT_FUNCTION_NAME,
                params = arrayOf(
                    tokenOut.toString(),
                    allowedSlippage.toString()
                )
            )
        return BigInteger(out)
    }


    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun tokenToXtzXtzOutput(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): Tez {
        val tezOut =
            callJS(
                functionName = TOKEN_TO_XTZ_FUNCTION_NAME,
                params = arrayOf(
                    tokenIn.toString(),
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return Tez(tezOut)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun tokenToXtzExchangeRate(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val rate =
            callJS(
                functionName = TOKEN_TO_XTZ_EXCHANGE_RATE_FUNCTION_NAME,
                params = arrayOf(
                    tokenIn.toString(),
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return BigDecimal(rate)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun tokenToXtzMarketRate(xtzPool: Tez, tokenPool: BigInteger, decimals: Int): BigDecimal {
        val rate =
            callJS(
                functionName = TOKEN_TO_XTZ_MARKET_RATE_FUNCTION_NAME,
                params = arrayOf(
                    xtzPool.stringRepresentation,
                    tokenPool.toString(),
                    decimals.toString()
                )
            )
        return BigDecimal(rate)
    }

    /**
     * @throws TezosError If JS logic could not be completed
     */
    override fun tokenToXtzPriceImpact(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val slippage =
            callJS(
                functionName = TOKEN_TO_XTZ_PRICE_IMPACT_FUNCTION_NAME,
                params = arrayOf(
                    tokenIn.toString(),
                    xtzPool.stringRepresentation,
                    tokenPool.toString()
                )
            )
        return BigDecimal(slippage)
    }

    override fun tokenToXtzMinimumXtzOutput(xtzOut: Tez, allowedSlippage: Double): Tez {
        val slippage =
            callJS(
                functionName = TOKEN_TO_XTZ_MINIMUM_OUT_FUNCTION_NAME,
                params = arrayOf(
                    xtzOut.stringRepresentation,
                    allowedSlippage.toString()
                )
            )
        return Tez(slippage)
    }

    private fun callJS(functionName: String, params: Array<Any>): String {
        val result = jsContext.callFunction("dexterCalculations", functionName, params)

        if (result.isBlank() || result == "null") {
            throw TezosError(TezosErrorType.UNEXPECTED_RESPONSE)
        }
        return result
    }


    companion object {
        private const val DEXTER_CALCULATIONS_FILE_NAME = "dexter-calculations_index-mobile.min.js"

        private const val XTZ_TO_TOKEN_FUNCTION_NAME = "xtzToTokenTokenOutput"
        private const val XTZ_TO_TOKEN_EXCHANGE_RATE_FUNCTION_NAME = "xtzToTokenExchangeRate"
        private const val XTZ_TO_TOKEN_MARKET_RATE_FUNCTION_NAME = "xtzToTokenMarketRate"
        private const val XTZ_TO_TOKEN_PRICE_IMPACT_FUNCTION_NAME = "xtzToTokenPriceImpact"
        private const val XTZ_TO_TOKEN_MINIMUM_OUT_FUNCTION_NAME = "xtzToTokenMinimumTokenOutput"

        private const val TOKEN_TO_XTZ_FUNCTION_NAME = "tokenToXtzXtzOutput"
        private const val TOKEN_TO_XTZ_EXCHANGE_RATE_FUNCTION_NAME = "tokenToXtzExchangeRate"
        private const val TOKEN_TO_XTZ_MARKET_RATE_FUNCTION_NAME = "tokenToXtzMarketRate"
        private const val TOKEN_TO_XTZ_PRICE_IMPACT_FUNCTION_NAME = "tokenToXtzPriceImpact"
        private const val TOKEN_TO_XTZ_MINIMUM_OUT_FUNCTION_NAME = "tokenToXtzMinimumXtzOutput"
    }

}
