/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.exchange.calculations

import io.camlcase.kotlintezos.model.Tez
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import kotlin.math.pow

/**
 * Native implementation of the Dexter calculations.
 * This class is under minimal maintenance, it's recommended you use [AndroidJSDexterCalculations].
 */
@Suppress("MagicNumber", "TooManyFunctions")
object NativeDexterCalculations : DexterCalculations {

    private val dexterLiquidityFee = 997.toBigDecimal()
    private const val scale = 12
    private const val tezScale = 6
    private const val roundingMode = BigDecimal.ROUND_HALF_UP

    override fun xtzToTokenTokenOutput(
        xtzIn: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigInteger {
        return xtzToTokenTradeAmount(
            xtzIn,
            xtzPool,
            tokenPool,
            dexterLiquidityFee
        ).toBigInteger()
    }

    fun xtzToTokenTradeAmount(
        amount: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger,
        liquidityFee: BigDecimal = dexterLiquidityFee
    ): BigDecimal {
        // Scaling up to deal with issues of Tez and FA1.2 tokens having different decimal precision
        val scaleUp = 1000.toBigDecimal()
        val amountNumber = amount.signedDecimalRepresentation
        val xtzPoolDecimal = xtzPool.signedDecimalRepresentation
        val tokenPoolDecimal = tokenPool.toBigDecimal()

        val numerator = amountNumber * tokenPoolDecimal * liquidityFee
        val denominator = xtzPoolDecimal * scaleUp + amountNumber * liquidityFee

        val result = numerator.divide(denominator, RoundingMode.DOWN)
        return result
    }


    private fun getTokenToXtzAmount(
        amount: BigDecimal,
        xtzPool: Tez,
        tokenPool: BigDecimal,
        liquidityFee: BigDecimal = dexterLiquidityFee
    ): BigDecimal {
        // Scaling up to deal with issues of Tez and FA1.2 tokens having different decimal precision
        val scaleUp = 1000.toBigDecimal()
        val xtzPoolDecimal = xtzPool.signedDecimalRepresentation

        val numerator = amount * xtzPoolDecimal * liquidityFee
        val denominator = tokenPool * scaleUp + amount * liquidityFee

        val result = numerator.divide(denominator, RoundingMode.DOWN)
        return result
    }

    /**
     * @throws ArithmeticException
     */
    override fun xtzToTokenExchangeRate(
        xtzIn: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val xtzToToken = xtzToTokenTokenOutput(xtzIn, xtzPool, tokenPool)
        return xtzToToken.toBigDecimal()
            .divide(BigDecimal(xtzIn.stringRepresentation), scale, BigDecimal.ROUND_HALF_UP)
    }

    /**
     * @return BigDecimal with a [scale]
     * @throws ArithmeticException
     */
    fun getXtzToTokenMarketRate(xtzPool: Tez, tokenPool: BigDecimal): BigDecimal {
        return tokenPool.divide(xtzPool.signedDecimalRepresentation, scale, roundingMode)

    }

    /**
     * @return BigDecimal with a [scale]
     * @throws ArithmeticException
     */
    override fun xtzToTokenMarketRate(
        xtzPool: Tez,
        tokenPool: BigInteger,
        decimals: Int
    ): BigDecimal {
        val xtzPoolDec =
            BigDecimal(xtzPool.stringRepresentation).multiply(10.0.pow(-6).toBigDecimal())
        val tokenPoolDec = tokenPool.toBigDecimal().multiply(10.0.pow(decimals * -1).toBigDecimal())
        return tokenPoolDec
            .divide(xtzPoolDec, scale, roundingMode)
    }

    /**
     * @return BigDecimal with a [scale] and [roundingMode]
     * @throws ArithmeticException
     */
    override fun tokenToXtzMarketRate(xtzPool: Tez, tokenPool: BigInteger, decimals: Int): BigDecimal {
        val xtzPoolDec =
            BigDecimal(xtzPool.stringRepresentation).multiply(10.0.pow(-6).toBigDecimal())
        val tokenPoolDec = tokenPool.toBigDecimal().multiply(10.0.pow(decimals * -1).toBigDecimal())
        return xtzPoolDec
            .divide(tokenPoolDec, scale, roundingMode)
    }

    /**
     * @throws ArithmeticException
     */
    fun tokenToXtzExchangeRate(
        amount: BigDecimal,
        xtzPool: Tez,
        tokenPool: BigDecimal
    ): BigDecimal {
        val tokenToXtz = getTokenToXtzAmount(amount, xtzPool, tokenPool)
        return tokenToXtz
            .setScale(tezScale, RoundingMode.DOWN) // XTZ only has 6 decimals
            .divide(amount, scale, BigDecimal.ROUND_DOWN)
    }

    /**
     * @return BigDecimal with a [scale]
     * @throws ArithmeticException
     */
    @Deprecated("Use xtzToTokenPriceImpact instead")
    fun xtzToTokenSlippage(amount: Tez, xtzPool: Tez, tokenPool: BigInteger): BigDecimal {
        val rate = xtzToTokenExchangeRate(amount, xtzPool, tokenPool)
        val marketRate = tokenPool.toBigDecimal()
            .divide(BigDecimal(xtzPool.stringRepresentation), scale, roundingMode)
        return (rate.subtract(marketRate).abs())
            .divide(marketRate, scale, BigDecimal.ROUND_DOWN)
    }

    override fun xtzToTokenPriceImpact(
        xtzIn: Tez,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        TODO("Use AndroidJSDexterCalculations or the dexter-calculations library directly.")
    }

    /**
     * @return BigDecimal with [scale]
     * @throws ArithmeticException
     */
    @Deprecated("Use tokenToXtzPriceImpact instead")
    fun tokenToXtzSlippage(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val rate = tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool)
        val marketRate = BigDecimal(xtzPool.stringRepresentation)
            .divide(tokenPool.toBigDecimal(), scale, roundingMode)
        return ((rate.subtract(marketRate).abs())
            .divide(marketRate, scale, BigDecimal.ROUND_HALF_UP))
    }

    override fun tokenToXtzPriceImpact(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        TODO("Use AndroidJSDexterCalculations or the dexter-calculations library directly.")
    }

    override fun tokenToXtzMinimumXtzOutput(xtzOut: Tez, allowedSlippage: Double): Tez {
        val xtzMinimumOut = calculateMinimumOut(
            xtzOut.signedDecimalRepresentation,
            allowedSlippage
        )
        return Tez(xtzMinimumOut)
    }

    override fun xtzToTokenMinimumTokenOutput(
        tokenOut: BigInteger,
        allowedSlippage: Double
    ): BigInteger {
        return calculateMinimumOut(tokenOut.toBigDecimal(), allowedSlippage).toBigInteger()
    }

    override fun tokenToXtzXtzOutput(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): Tez {
        val amountNumber = tokenIn.toBigDecimal()
        val result = getTokenToXtzAmount(
            amountNumber,
            xtzPool,
            tokenPool.toBigDecimal(),
            dexterLiquidityFee
        )
        return Tez(result)
    }

    override fun tokenToXtzExchangeRate(
        tokenIn: BigInteger,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal {
        val tokenToXtz: Tez = tokenToXtzXtzOutput(tokenIn, xtzPool, tokenPool)
        return BigDecimal(tokenToXtz.stringRepresentation)
            .divide(tokenIn.toBigDecimal(), scale, BigDecimal.ROUND_DOWN)
    }

    /**
     * Return the minimum value that should be sent to the Dexter contract entrypoint xtzToToken and tokenToXtz to get [amount].
     * It is the minimum of XTZ or token that the trader will receive.
     *
     * @param amount XTZ/FA1.2 Token amount desired. By default 1.
     * @param allowedSlippage % of slippage allowed in decimal format. E.g. 0.01 for 0.1%
     * @return BigDecimal with a scale of [scale]
     */
    fun calculateMinimumOut(
        amount: BigDecimal = BigDecimal.ONE,
        allowedSlippage: Double
    ): BigDecimal {
        val subtrahend = amount.multiply(allowedSlippage.toBigDecimal())
        return amount.subtract(subtrahend)
            .setScale(scale, BigDecimal.ROUND_DOWN)
    }
}
