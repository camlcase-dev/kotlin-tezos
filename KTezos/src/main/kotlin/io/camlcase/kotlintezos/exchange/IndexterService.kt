/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.exchange

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.causeIs
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.dexter.GetAllowanceAPI
import io.camlcase.kotlintezos.data.dexter.GetCurrencies
import io.camlcase.kotlintezos.data.dexter.GetFABalance
import io.camlcase.kotlintezos.data.dexter.IndexterEnvironmentParam
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.indexter.IndexterCurrency
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import java.math.BigInteger
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Kotlin implementation for inDEXter, a cache for FA1.2 contracts to operate with DEXter.
 * [See documentation](https://gitlab.com/camlcase-dev/indexter).
 *
 * @param networkClient A client connected to the Indexter server.
 * @param executorService To manage network requests and calculations asynchronously.
 */
class IndexterService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {

    /**
     * Gets the allowance of a [dexterContractAddress] for an [source] in an FA1.2 [tokenContractAddress]
     *
     * @return CompletableFuture with nullable BigInteger value of number of allowances.
     */
    fun getAllowance(
        tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
        tokenContractAddress: Address,
        dexterContractAddress: Address,
        source: Address
    ): CompletableFuture<BigInteger?> {
        return networkClient.send(
            GetAllowanceAPI(
                tezosNetwork,
                tokenContractAddress,
                dexterContractAddress,
                source
            )
        )
            .exceptionally {
                if (it.causeIs(GetAllowanceAPI.NO_ALLOWANCE_ERROR)) {
                    BigInteger.ZERO
                } else {
                    throw it
                }
            }
    }

    /**
     * Gets the allowance of a [dexterContractAddress] for an [source] in an FA1.2 [tokenContractAddress]
     *
     * @param callback with BigInteger value of number of allowances.
     */
    fun getAllowance(
        tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
        tokenContractAddress: Address,
        dexterContractAddress: Address,
        source: Address,
        callback: TezosCallback<BigInteger>
    ) {
        getAllowance(tezosNetwork, tokenContractAddress, dexterContractAddress, source)
            .onComplete(
                executorService,
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    if (result != null) {
                        callback.onSuccess(result)
                    } else {
                        callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                    }
                })
    }

    /**
     * Retrieves the amount of FA1.2 token ([tokenContractAddress]) that an [address] holds.
     *
     * @param tezosNetwork Adds "network" parameter for testnet.
     * @param forceRefresh InDEXter has an inbuilt delay to refreshing balances for performance.
     * Use true to force a refresh of those balances. By default false.
     * @param callback with BigInteger value. FA1.2 tokens vary on decimal places. It's the responsibility
     * of the receiver to parse the integer value to its appropriate decimal representation.
     */
    fun getTokenBalance(
        tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
        tokenContractAddress: Address,
        address: Address,
        forceRefresh: Boolean = false,
        callback: TezosCallback<BigInteger>
    ) {
        networkClient.send(GetFABalance(tezosNetwork, tokenContractAddress, address, forceRefresh))
            .onComplete(
                executorService,
                onFailure = { throwable ->
                    if (throwable.causeIs(GetFABalance.NO_BALANCE_ERROR)) {
                        callback.onSuccess(BigInteger.ZERO)
                    } else {
                        callback.onFailure(throwable.wrap())
                    }
                },
                onSuccess = { result ->
                    if (result != null) {
                        callback.onSuccess(result)
                    } else {
                        callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                    }
                })

    }

    /**
     * Retrieves a map of FA1.2 tokens with their balances for a given [address].
     * This method will run all queries at once and only return values if all are successful.
     *
     * @param tezosNetwork Adds "network" parameter for testnet.
     * @param tokenContractAddresses List of FA1.2 token addresses
     * @param forceRefresh InDEXter has an inbuilt delay to refreshing balances for performance.
     * Use true to force a refresh of those balances. By default false.
     *
     * @param callback with a Map of [Address] given in [tokenContractAddresses] and its BigInteger value.
     * FA1.2 tokens vary on decimal places. It's the responsibility of the receiver to parse the integer value to its appropriate decimal representation.
     */
    fun getTokenBalances(
        tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
        tokenContractAddresses: List<Address>,
        address: Address,
        forceRefresh: Boolean = false,
        callback: TezosCallback<Map<Address, BigInteger>>
    ) {
        val futures: List<CompletableFuture<Pair<Address, BigInteger>>> = tokenContractAddresses.map { tokenAddress ->
            Future { tokenAddress }
                .flatMap(executorService) { networkClient.send(GetFABalance(tezosNetwork, it, address, forceRefresh)) }
                .map(executorService) { Pair(tokenAddress, it ?: BigInteger.ZERO) }
        }

        Future.allAsList(futures)
            .map(executorService) { it.toMap() }
            .onComplete(
                executorService,
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Fetch FA1.2 tokens supported by InDexter.
     * @param environment The development environment type of tokens.
     * @param callback A list of tokens
     */
    fun getCurrencies(
        environment: IndexterEnvironmentParam,
        callback: TezosCallback<List<IndexterCurrency>>
    ) {
        networkClient.send(GetCurrencies(environment))
            .onComplete(
                executorService,
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    if (result != null) {
                        callback.onSuccess(result)
                    } else {
                        callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                    }
                })
    }
}
