/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos

import io.camlcase.kotlintezos.TzKtClient.Companion.SUPPORTED_VERSION
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.tzkt.GetOperationByHashRPC
import io.camlcase.kotlintezos.data.tzkt.GetOperationsByAddressRPC
import io.camlcase.kotlintezos.data.tzkt.GetOperationsParameter
import io.camlcase.kotlintezos.data.tzkt.GetTransactionsByParameter
import io.camlcase.kotlintezos.data.tzkt.GetTransactionsRPC
import io.camlcase.kotlintezos.data.tzkt.GetTzKtAccountRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.tzkt.TzKtAccount
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.map
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import io.github.vjames19.futures.jdk8.zip
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * TzKT Explorer provides a free API for accessing detailed Tezos blockchain data and to help developers build
 * more services and applications on top of Tezos.
 *
 * If your application or service uses the TzKT API in any forms: directly on frontend or indirectly on
 * backend, you should mention that fact on your website or application by placing the label "Powered by TzKT API"
 * with a direct link to tzkt.io.
 *
 * [TzKtClient] supports [SUPPORTED_VERSION] of the tzkt API.
 *
 * See [TzKt Api](https://api.tzkt.io/)
 */
class TzKtClient(
        private val networkClient: NetworkClient,
        private val executorService: ExecutorService = Executors.newCachedThreadPool()
) {

    /**
     * @param tzKtApiUrl The url to your TzKt indexer (without final /)
     */
    constructor(tzKtApiUrl: String) : this(DefaultNetworkClient(tzKtApiUrl))

    /**
     * Get a TzKt account
     *
     * @param address Tezos address of the account
     * @param callback with a [TzKtAccount]
     */
    fun getAccount(address: Address, callback: TezosCallback<TzKtAccount>) {
        networkClient.send(GetTzKtAccountRPC(address))
                .map(executorService) {
                    it?.map() ?: throw TezosError(TezosErrorType.UNEXPECTED_RESPONSE)
                }
                .onComplete(
                        onFailure = { throwable ->
                            callback.onFailure(throwable.wrap())
                        },
                        onSuccess = { result ->
                            callback.onSuccess(result)
                        })
    }

    /**
     * Retrieve the operation details of a given [hash].
     *
     * @param hash Operation hash
     * @param callback With a list of implementations of [TzKtOperation]. Empty if nothing was found.
     */
    fun getOperationDetails(hash: String, callback: TezosCallback<List<TzKtOperation>>) {
        networkClient.send(GetOperationByHashRPC(hash), callback)
    }

    /**
     * Retrieve active operations sent, received and called from a smart contract including an account.
     *
     * @param source The account to query.
     * @param filters Filters for the operations. By default, all operations supported by KotlinTezos.
     * @param filtersForTokenReceive Filters for the FA1.2 Receive call. By default, none.
     * @param callback With a list of implementations of [TzKtOperation]. Empty if nothing was found.
     */
    fun operations(
        source: Address,
        filters: Map<GetOperationsParameter, String> =
                    mapOf(GetOperationsParameter.TYPE to GetOperationsByAddressRPC.QUERY_PARAMS_TYPE_VALUE),
        filtersForTokenReceive: Map<GetTransactionsByParameter, String> = emptyMap(),
        callback: TezosCallback<List<TzKtOperation>>
    ) {
        networkClient.send(GetOperationsByAddressRPC(source, filters))
                .zip(
                        tokenOperationReceived(source, filtersForTokenReceive),
                        executorService
                ) { all, tokensReceived ->
                    val operations = ArrayList<TzKtOperation>()
                    if (!tokensReceived.isNullOrEmpty()) {
                        operations.addAll(tokensReceived)
                    }
                    if (!all.isNullOrEmpty()) {
                        operations.addAll(all)
                    }

                    operations.sortedByDescending { it.timestamp }
                    operations
                }
                .onComplete(
                        onFailure = { throwable ->
                            callback.onFailure(throwable.wrap())
                        },
                        onSuccess = { result ->
                            callback.onSuccess(result)
                        })

    }

    /**
     * Fetch all transactions that match an FA1.2 token receive.
     */
    private fun tokenOperationReceived(
            source: Address,
            filters: Map<GetTransactionsByParameter, String>
    ): CompletableFuture<List<TzKtOperation>?> {
        return networkClient.send(GetTransactionsRPC(source, filters))
    }

    /**
     * Retrieve transaction type of operations.
     * @param filters Filters for the query. See all you can use in the [docs](https://api.tzkt.io/#operation/Operations_GetTransactions)
     */
    fun transactions(
            filters: Map<GetTransactionsByParameter, String>,
            callback: TezosCallback<List<TzKtOperation>>
    ) {
        networkClient.send(GetTransactionsRPC(filters), callback)
    }

    companion object {
        internal const val SUPPORTED_VERSION = "/v1"

        const val DELPHINET_TZKT_URL = "https://api.delphi.tzkt.io"
        const val EDO2NET_TZKT_URL = "https://api.edo2net.tzkt.io"
        const val FLORENCENET_TZKT_URL = "https://api.florencenet.tzkt.io"
        const val GRANADANET_TZKT_URL = "https://api.granadanet.tzkt.io"

        const val MAINNET_TZKT_URL = "https://api.tzkt.io"
    }

}
