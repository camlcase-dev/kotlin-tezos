/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.token

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.badArgumentsError
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.operation.TokenOperationParams
import io.camlcase.kotlintezos.model.operation.TokenOperationType
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.smartcontract.SmartContractClient
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.token.balance.TokenBalanceService
import io.camlcase.kotlintezos.token.balance.TokenBalanceService.Companion.revealSignature
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.map
import java.math.BigInteger
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * A client for an FA1.2 Token Contract.
 *
 * @param tokenContractAddress The address of a FA1.2 token contract.
 * @param client An accessor to operate with smart contracts in the blockchain. E.g.: TezosNodeClient
 * @param balanceService Balance provider.
 */
class TokenContractClient(
    private val tokenContractAddress: Address,
    private val client: SmartContractClient,
    private val balanceService: TokenBalanceService,
    private val executorService: ExecutorService = Executors.newCachedThreadPool()
) {

    /**
     * @param tezosNetwork [TezosNetwork] to connect to. Default is [TezosNetwork.MAINNET]
     * @param tokenContractAddress The address of a FA1.2 token contract.
     * @param networkClient implementation for connection.
     */
    constructor(
        tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
        tokenContractAddress: Address,
        client: SmartContractClient,
        networkClient: NetworkClient,
        executorService: ExecutorService = Executors.newCachedThreadPool()
    ) : this(
        tokenContractAddress,
        client,
        TokenBalanceService(tezosNetwork, networkClient, executorService),
        executorService
    )


    /**
     * Retrieves the balance of [address] of a certain [tokenContractAddress].
     *
     * For FA1.2 tokens means dry-run of a transaction and forcing an error.
     *
     * @param address Owner of the balance
     * @param publicKey In case reveal needs to be attached.
     *
     * @return Callback with BigInteger value. FA1.2 tokens vary on decimal places. It's responsibility of the
     * receiver to parse the integer value to its appropriate decimal representation.
     */
    fun getTokenBalance(address: Address, publicKey: PublicKey, callback: TezosCallback<BigInteger>) {
        balanceService.getTokenBalance(tokenContractAddress, address, revealSignature(publicKey), callback)
    }

    /**
     * Transact token between accounts
     *
     * @param amount of Tokens to send. Will be passed as an [BigIntegerMichelsonParameter].
     * @param to The address which will receive the Token.
     * @param from The address sending the balance.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun send(
        amount: BigInteger,
        to: Address,
        from: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        Future {
            val params = TokenOperationParams.Transfer(amount, tokenContractAddress, to, from)
            OperationFactory.createTokenOperation(TokenOperationType.TRANSFER, params, OperationFees.simulationFees)
        }
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("Could not create operation for $tokenContractAddress")
                }
                client.runContractOperations(it, from, signatureProvider, feesPolicy, callback)
            }
    }

    /**
     * Calculate the fees for the given [type]
     *
     * @param callback with a [CalculatedFees] object
     */
    fun calculateFees(
        type: TokenOperationType,
        params: TokenOperationParams,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<CalculatedFees>
    ) {
        Future { OperationFactory.createTokenOperation(type, params, OperationFees.simulationFees) }
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("Could not create operation for $type and $params")
                }
                client.calculateFees(it, source, signatureProvider, feesPolicy, callback)
            }
    }
}
