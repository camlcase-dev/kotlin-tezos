/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

/**
 * A human readable error returned by the RPC
 *
 * @param contractArgs If its script related, arguments returned
 *
 * @see http://tezos.gitlab.io/api/errors.html
 */
data class RPCErrorResponse(
    val type: RPCErrorType,
    val cause: String,
    val message: String? = null,
    val contract: String? = null,
    val contractArgs: Map<String, Any?>? = null
)

enum class RPCErrorType {
    BRANCH,
    TEMPORARY,
    PERMANENT
}

enum class OperationResultStatus {
    FAILED,
    BACKTRACKED,
    SKIPPED,
    SUCCESS,
    APPLIED,
    UNKNOWN;

    companion object {
        fun get(status: String?): OperationResultStatus {
            val found = values().firstOrNull { it.name.equals(status, true) }
            return found ?: UNKNOWN
        }

        fun get(status: Any?): OperationResultStatus {
            return if (status is String) {
                get(status)
            } else {
                UNKNOWN
            }
        }
    }
}

/**
 * Translates RPC error messages to more human readable causes. Keep in mind some of them might share error messages
 * and other context checks might be needed to ensure the match.
 *
 * Search for [values] inside the [RPCErrorResponse.cause] attribute.
 *
 * @see https://tezos.gitlab.io/api/errors.html
 */
enum class RPCErrorCause(vararg val values: String) {
    /**
     * Sending to correctly-formatted but nonexistent address.
     *
     * - When sending XTZ we receive a verbose error that shows a:
     *    * Unhandled error (Failure \"Invalid contract notation\")
     *  message in between.
     *
     * - When sending FA1.2 tokens, the smart contract should return both:
     *   * contract.invalid_contract_notation
     *   * invalidSyntacticConstantError
     *
     */
    INVALID_CONTRACT(
        "contract.invalid_contract_notation",
        "invalidSyntacticConstantError",
        "Invalid contract notation"
    ),

    /**
     * - When delegating, we receive a verbose error that shows a:
     *      * Unexpected data (Signature.Public_key_hash)
     * message in between if the address is correctly-formatted but nonexistent.
     *
     * - When delegating to an address NOT registered as a baker
     * (A contract cannot be delegated to an unregistered delegate):
     *      * contract.manager.unregistered_delegate
     */
    INVALID_DELEGATE(
        "Unexpected data (Signature.Public_key_hash)",
        "contract.manager.unregistered_delegate"
    ),

    /**
     * - When user is registered as baker, they cannot delegate to another address
     */
    BAKER_CANT_DELEGATE(
        "delegate.no_deletion"
    ),

    /**
     * Contract already delegated to the given delegate
     */
    UNCHANGED_DELEGATED(
        "delegate.unchanged"
    ),

    /**
     * Not enough XTZ balance to cover fees and gas of an operation.
     */
    INSUFFICIENT_XTZ_BALANCE(
        "contract.balance_too_low",
        "contract.cannot_pay_storage_fee",
        "empty_implicit_contract",
        "empty_implicit_delegated_contract"
    ),

    /**
     * Dexter error: User tried to proceed with a trade that results in zero tokens returned.
     */
    EXCHANGE_ZERO_TOKENS(
        "minTokensBought must be greater than zero"
    ),

    /**
     * Dexter errors regarding tokens amounts
     */
    EXCHANGE_INVALID_SWAP(
        // (xtz_to_token): The minimum asked for is higher than what you will get back.
        "tokens_bought must be greater than or equal to min_tokens_bought",
        "tokensBought is less than minTokensBought."
    ),

    /**
     * Dexter exchange timed out before it could be completed.
     */
    EXCHANGE_INVALID_DEADLINE(
        "NOW is greater than deadline"
    ),

    /**
     * Dexter errors related to user balance
     */
    EXCHANGE_INSUFFICIENT_FUNDS(
        /**
         * When trying to do a tokenToXTZ swap with funds you don't have
         */
        "NotEnoughBalance"
    ),

    /**
     * Tried to run an operation with invalid metadata
     */
    INVALID_METADATA(
        "contract.counter_in_the_future",
        "contract.counter_in_the_past"
    ),

    /**
     * The storage_limit for that operation is higher than current limits
     */
    OPERATION_STORAGE(
        "storage_limit_too_high"
    ),

    /**
     * Smart contract call failed for some reason. Usually listed somewhere else.
     */
    UNKNOWN_SMART_CONTRACT_ERROR(
        "michelson_v1.runtime_error",
        "michelson_v1.script_rejected"
    )
}
