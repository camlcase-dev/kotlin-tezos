/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.bcd

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.dto.BCDError
import io.camlcase.kotlintezos.model.bcd.dto.BCDOperationEntity
import io.camlcase.kotlintezos.model.bcd.dto.BCDResult
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import kotlinx.serialization.Serializable

data class BCDOrigination(
    override val id: String,
    override val hash: String,
    override val protocol: String,
    override val network: TezosNetwork,
    override val timestamp: String,
    override val status: OperationResultStatus,
    override val fees: OperationFees?,
    override val errors: List<BCDError>?,
    val source: Address,
): BCDOperation{
    override val kind: OperationType = OperationType.ORIGINATION
}

@Serializable
data class BCDOriginationEntity(
    override val kind: String,
    override val id: String,
    override val hash: String,
    override val protocol: String,
    override val timestamp: String,
    override val status: String,
    override val network: String,
    override val result: BCDResult? = null,
    override val fee: Int = 0,
    override val gas_limit: Int = 0,
    override val storage_limit: Int = 0,
    override val allocated_destination_contract_burned: Int? = null,
    override val errors: List<BCDError>? = null,
    val source: String,
): BCDOperationEntity
