/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

enum class TezosNetwork {
    ZERONET,
    MAINNET,
    BABYLONNET,
    CARTHAGENET,

    /**
     * 007 Delphi
     * - [Official release](https://blog.nomadic-labs.com/delphi-official-release.html)
     * - [Changelog](https://blog.nomadic-labs.com/delphi-changelog.html)
     */
    DELPHINET,

    /**
     * 008 Edo
     * - [Protocol info](https://tezos.gitlab.io/protocols/008_edo.html)
     * - [Proposal](https://forum.tezosagora.org/t/announcing-the-edo-release/2443)
     */
    EDONET,

    /**
     * 009 Florence (No Baking Accounts)
     * - [Protocol info](http://doc.tzalpha.net/protocols/009_florence.html)
     */
    FLORENCENET,

    /**
     * 010 Granada
     * - [Protocol info](https://tezos.gitlab.io/protocols/010_granada.html)
     */
    GRANADANET
}
