/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.blockchain

import io.camlcase.kotlintezos.model.TezosProtocol

/**
 * A lightweight property bag containing data about the current chain state in order to forge / preapply / inject / operations on the Tezos blockchain.
 */
data class BlockchainMetadata(
    /**
     * The hash of the head of the chain being operated on.
     */
    val blockHash: String,
    /**
     * The hash of the protocol being operated on.
     */
    val protocol: String,
    /**
     * The chain unique identifier.
     */
    val chainId: String,
    /**
     * The counter for the address being operated on.
     */
    val counter: Int,
    /**
     * The base58encoded public key for the address, or nil if the key is not yet revealed.
     */
    val key: String?,

    /**
     * Network constants for operations
     */
    val constants: NetworkConstants
) {
    /**
     * The protocol being operated on. Extracted from [protocol]
     */
    val tezosProtocol: TezosProtocol
        get() = TezosProtocol.get(protocol)

}
