/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * An operation to transact XTZ between addresses.
 *
 * @param parameter An optional parameter to include in the transaction if the call is being made to a smart contract.
 */
data class TransactionOperation(
    val amount: Tez,
    override val source: Address,
    val destination: Address,
    override val fees: OperationFees
) : BaseOperation {
    override val type: OperationType
        get() = OperationType.TRANSACTION

    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            payload[PAYLOAD_ARG_AMOUNT] = amount.stringRepresentation
            payload[PAYLOAD_ARG_DESTINATION] = destination
            return payload
        }

    override fun copy(newFees: OperationFees): Operation {
        return TransactionOperation(amount, source, destination, newFees)
    }

    companion object {
        const val PAYLOAD_ARG_AMOUNT = "amount"
        const val PAYLOAD_ARG_DESTINATION = "destination"

    }
}
