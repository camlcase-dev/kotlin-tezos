/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.bcd.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Representation of an error on the Better Call Dev api.
 *
 * @param id RPC name for the error
 * @param title Overview description of the error
 * @param description Cause of the error
 * @param kind Temporary or Permanent
 * @param contractMessage Smart contract error message
 */
@Serializable
data class BCDError(
    val id: String,
    val title: String,
    @SerialName("descr")
    val description: String,
    val kind: String,
    val location: Int? = null,
    @SerialName("with")
    val contractMessage: String? = null
)

/**
 * Representation of the costs of the operation results on the Better Call Dev api
 *
 * @param storageDiff Burn fees (Optional)
 */
@Serializable
data class BCDResult(
    @SerialName("consumed_gas")
    val consumedGas: Int = 0,
    @SerialName("storage_size")
    val storageSize: Int = 0,
    @SerialName("paid_storage_size_diff")
    val storageDiff: Int? = null,
    @SerialName("allocated_destination_contract")
    val allocated: Boolean = false
)
