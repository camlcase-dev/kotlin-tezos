/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.indexter

import io.camlcase.kotlintezos.model.Address
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Representation of a Token as InDexter returns it.
 *
 * [See service model](https://gitlab.com/camlcase-dev/indexter/-/blob/master/internal/currency/currency.go#L21)
 *
 * @param symbol Symbol of the token lower cased, e.g. "tzbtc"
 * @param image data:image/png;base64,<BASE64_ENCODED_IMAGE> The image part of the object can be wrapped directly in an html <img> tag. After data:image/png;base64, is a base64 encoding of the image.
 * @param tokenContractAddress KT1 address of the smart contract of the token
 * @param dexterContractAddress KT1 address of the Dexter smart contract of the token
 */
data class IndexterCurrency(
    val symbol: String,
    val decimals: Int,
    val tokenContractAddress: Address,
    val dexterContractAddress: Address,
    val image: String,
    val bigMapId: Int
)


@Serializable
internal data class IndexterCurrencyResponse(
    val currency: String,
    val image: String,
    @SerialName("token-contract")
    val tokenContractAddress: String,
    @SerialName("dexter-contract")
    val dexterContractAddress: String,
    val decimals: Int,
    @SerialName("big-map-id")
    val bigMapId: Int
)

internal fun IndexterCurrencyResponse.map(): IndexterCurrency {
    return IndexterCurrency(
        this.currency,
        this.decimals,
        this.tokenContractAddress,
        this.dexterContractAddress,
        this.image,
        this.bigMapId
    )
}
