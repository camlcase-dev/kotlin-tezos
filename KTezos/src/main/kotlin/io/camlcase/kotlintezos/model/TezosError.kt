/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

import io.camlcase.kotlintezos.operation.forge.ForgingVerifier

/**
 * Kinds of errors
 *
 * @see [TezosError]
 */
enum class TezosErrorType {
    INTERNAL_ERROR,
    INVALID_URL,
    LOCAL_FORGING_NOT_SUPPORTED,

    /**
     * Tried to calculate the fees by dry-running the operation but it failed.
     */
    SIMULATION_FAILED,

    /**
     * Verifying a forged response failed.
     * @see [ForgingVerifier]
     */
    FORGING_ERROR,
    FAILED_OPERATION,
    PREAPPLICATION_ERROR,

    /**
     * Network or response error
     */
    RPC_ERROR,

    /**
     * For:
     * Error while applying operation oop()...):\nbranch refused (Error:\n Counter N already used for contract (...) (expected N+1)
     * Returned when an operations is trying to be injected to the chain after another that has not been included yet.
     */
    COUNTER,
    SIGNING_ERROR,
    UNEXPECTED_RESPONSE,
    MNEMONIC_GENERATION,
    WALLET_CREATION,
    UNKNOWN
}

/**
 * Wrapper for an exception thrown during a Tezos operation.
 *
 * @param rpcErrors If Errors returned by the RPC, will be parsed here
 * @param code If HTTP Error, the code response (404, 500)
 * @param exception  If Exception thrown, will be returned here
 *
 * @see [TezosErrorType]
 */
data class TezosError(
    val type: TezosErrorType,
    val rpcErrors: List<RPCErrorResponse>? = null,
    val code: Int? = null,
    val exception: Throwable? = null
) : Throwable(exception?.message, exception?.cause)
