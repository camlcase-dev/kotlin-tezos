/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.blockchain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * All constants for the node and network.
 *
 * [See rpc documentation](https://tezos.gitlab.io/008/rpc.html#get-block-id-context-constants)
 */
@Serializable
data class NetworkConstants(
    @SerialName("hard_gas_limit_per_operation")
    val gasLimitPerOperation: String,
    @SerialName("hard_storage_limit_per_operation")
    val storageLimitPerOperation: String,
    @SerialName("hard_gas_limit_per_block")
    val gasLimitPerBlock: String,
    @SerialName("origination_size")
    val originationSize: Int,
    @SerialName("cost_per_byte")
    val costPerByte: String
)
