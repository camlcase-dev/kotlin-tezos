/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt.dto

import io.camlcase.kotlintezos.model.Address
import kotlinx.serialization.Serializable

@Serializable
data class TzKtAlias(
    val alias: String? = null,
    val address: Address
) {
    companion object {
        val empty = TzKtAlias(null, "")
    }
}

@Serializable
data class TzKtOperationError(
    val type: String
)

/**
 * TzKt representation of an originated contract
 *
 * @param kind Kind of the contract (delegator_contract or smart_contract), where delegator_contract - manager.tz smart contract for delegation purpose only
 * @param alias Name of the project behind the contract or contract description
 * @param address Public key hash of the contract
 */
@Serializable
data class TzKtOriginatedContract(
    val kind: String = "smart_contract",
    val alias: String? = null,
    val address: Address
)
