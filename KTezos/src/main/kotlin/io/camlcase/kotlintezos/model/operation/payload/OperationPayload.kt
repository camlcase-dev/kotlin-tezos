/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation.payload

import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.data.Payload
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.operation.RevealService

/**
 * A payload that can be forged into operation bytes.
 *
 * @param operations An array of dictionaries representing operations.
 * @param branch The hash of the head of the chain to apply the operation on.
 */
data class OperationPayload(
    val operations: Iterable<OperationWithCounter>,
    val branch: String
) : Payload {

    constructor(operations: Iterable<OperationWithCounter>, metadata: BlockchainMetadata) : this(
        operations,
        metadata.blockHash
    )

    override val payload: MutableMap<String, Any>
        get() {
            val operationsPayload: List<Map<String, Any>> = operations.map { it.payload }
            val map = HashMap<String, Any>()
            map["contents"] = operationsPayload
            map["branch"] = branch
            return map
        }

    companion object {

        /**
         * Constructor: Create an operation payload from the given inputs.
         */
        operator fun invoke(
            operations: Iterable<Operation>,
            source: Address,
            metadata: BlockchainMetadata,
            signatureProvider: SignatureProvider
        ) = run {
            val checkedOperations = RevealService.checkRevealOperation(
                operations,
                metadata.key,
                source,
                signatureProvider.publicKey,
                metadata.tezosProtocol
            )
            val operationsWithCounter = parseOperations(checkedOperations, metadata)
            OperationPayload(operationsWithCounter, metadata)
        }

        /**
         * Process all operations to have increasing counters and place them in the contents array.
         */
        fun parseOperations(
            operations: Iterable<Operation>,
            metadata: BlockchainMetadata
        ): Iterable<OperationWithCounter> {
            val list = ArrayList<OperationWithCounter>()
            var nextCounter = metadata.counter + 1
            operations.forEach {
                list.add(OperationWithCounter(it, nextCounter))
                nextCounter += 1
            }

            return list
        }
    }
}
