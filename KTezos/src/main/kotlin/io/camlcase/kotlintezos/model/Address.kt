/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

import java.util.regex.Pattern

typealias Address = String

/**
 * Check a given string is:
 * - Not empty
 * - 36 characters
 * - Starts with TZ1, TZ2, TZ3 or KT1
 * - Does not contain ~`!#$%^&*+=-[]';,./{}|":<>? (and space)
 */
fun Address?.validTezosAddress(): Boolean {
    if (this.isNullOrBlank()) {
        return false
    }

    val prefixPattern = Pattern.compile("^(tz1|tz2|tz3|kt1|TZ1|TZ2|TZ3|KT1)")
    if (!prefixPattern.matcher(this).lookingAt()) {
        return false
    }

    val validCharsPattern =
        Pattern.compile("^([a-zA-Z0-9~%@#$^*/\"`'()!_+=\\[\\]\\{}|\\\\,.?: -\\\\s]{36})$")
    if (!validCharsPattern.matcher(this).matches()) {
        return false
    }

    val spacesPattern = Pattern.compile("[^\\w]")
    if (spacesPattern.matcher(this).find()) {
        return false
    }

    return true
}
