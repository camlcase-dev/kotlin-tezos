/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees

/**
 * @param operations A list of operations with their simulated fees
 * @param simulationErrors If there were any errors while simulating.
 */
data class BundledFees(
    val operations: List<Operation>,
    val simulationErrors: List<RPCErrorResponse>? = null
) {
    constructor(operations: List<Operation>, calculatedFees: CalculatedFees) : this(
        operations.mapIndexed { index, operation -> operation.copy(calculatedFees.operationsFees[index]) },
        calculatedFees.internalErrors
    )
}
