/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.UnitMichelsonParameter

/**
 * An operation to invoke a smart contract.
 */
data class SmartContractCallOperation(
    val amount: Tez,
    override val source: Address,
    val destination: Address,
    override val fees: OperationFees,
    val parameter: MichelsonParameter? = null,
    val entrypoint: String? = null
) : BaseOperation {
    override val type: OperationType
        get() = OperationType.TRANSACTION

    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            // Same as TransactionOperation
            payload[TransactionOperation.PAYLOAD_ARG_AMOUNT] = amount.stringRepresentation
            payload[TransactionOperation.PAYLOAD_ARG_DESTINATION] = destination

            val parameters = HashMap<String, Any>()
            parameters[PAYLOAD_ARG_SMART_CONTRACT_ENTRYPOINT] =
                entrypoint ?: SMART_CONTRACT_ENTRYPOINT_DEFAULT
            parameters[PAYLOAD_ARG_SMART_CONTRACT_VALUE] =
                parameter?.listPayload() ?: parameter?.payload ?: UnitMichelsonParameter().payload
            payload[PAYLOAD_ARG_SMART_CONTRACT_PARAMETERS] = parameters

            return payload
        }

    override fun copy(newFees: OperationFees): Operation {
        return SmartContractCallOperation(amount, source, destination, newFees, parameter, entrypoint)
    }

    companion object {
        const val PAYLOAD_ARG_SMART_CONTRACT_PARAMETERS = "parameters"
        const val PAYLOAD_ARG_SMART_CONTRACT_ENTRYPOINT = "entrypoint"
        const val PAYLOAD_ARG_SMART_CONTRACT_VALUE = "value"

        const val SMART_CONTRACT_ENTRYPOINT_DEFAULT = "default"
        const val SMART_CONTRACT_ENTRYPOINT_APPROVE = "approve"
        const val SMART_CONTRACT_ENTRYPOINT_TRANSFER = "transfer"
    }

}
