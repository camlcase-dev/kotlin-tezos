/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.bcd.dto

import io.camlcase.kotlintezos.model.bcd.BCDAccountToken
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.math.BigInteger

/**
 * Batch account token balances
 */
@Serializable
data class BCDAccountTokenBalancesResponse(
    val balances: List<BCDTokenResponse>,
    val total: Int
)

/**
 * [See docs](https://better-call.dev/docs#operation/get-batch-token-balances)
 */
@Serializable
data class BCDTokenResponse(
    @SerialName("token_id")
    val tokenId: Int,
    val contract: String,
    val network: String,
    val balance: String,
    val decimals: Int? = null,
    val symbol: String? = null,
    val name: String? = null,
    val description: String? = null,
    @SerialName("display_uri")
    val displayUri: String? = null,
    @SerialName("thumbnail_uri")
    val thumbnailUri: String? = null,
    @SerialName("is_transferable")
    val isTransferable: Boolean? = null
)

fun BCDTokenResponse.map(): BCDAccountToken {
    return BCDAccountToken(
        this.tokenId,
        this.contract,
        BigInteger(this.balance),
        this.decimals,
        this.symbol,
        this.name,
        this.displayUri,
    )
}
