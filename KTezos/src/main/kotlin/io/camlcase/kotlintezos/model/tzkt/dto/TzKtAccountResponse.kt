/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt.dto

import kotlinx.serialization.Serializable

/**
 * Representation of an account response by the TzKt api
 * @see TzKtAccount
 */
@Serializable
data class TzKtAccountResponse(
    val type: String? = null,
    val address: String? = null,
    val publicKey: String? = null,
    val revealed: Boolean? = null,
    val balance: Long? = null,
    val delegate: TzKtAlias? = null,
    val numContracts: Int? = null,
    val numActivations: Int? = null,
    val numDelegations: Int? = null,
    val numOriginations: Int? = null,
    val numTransactions: Int? = null,
    val numReveals: Int? = null,
    val numMigrations: Int? = null,
)
