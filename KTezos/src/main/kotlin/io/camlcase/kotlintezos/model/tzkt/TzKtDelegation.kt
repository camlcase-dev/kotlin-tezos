/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt

import io.camlcase.kotlintezos.data.parser.BigIntegerSerializer
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtOperationResponse
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtOperationError
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.math.BigInteger

/**
 * Representation of a Delegation by TzKt
 *
 * @param source Information about the delegated account
 * @param previousDelegate Information about the previous delegate of the account. null if there is no previous delegate
 * @param newDelegate Information about the delegate to which the operation was sent. null if there is no new delegate (an undelegation operation)
 * @param delegatedAmount Sender's balance at the time of delegation operation
 */
@Suppress("LongParameterList")
data class TzKtDelegation(
    override val id: String,
    override val level: Int,
    override val hash: String,
    override val timestamp: String,
    override val block: String?,
    override val status: OperationResultStatus,
    override val fees: OperationFees?,
    override val errors: List<TzKtOperationError>?,
    val source: TzKtAlias,
    val previousDelegate: TzKtAlias?,
    val newDelegate: TzKtAlias?,
    val delegatedAmount: Tez
) : TzKtOperation {
    override val kind: OperationType = OperationType.DELEGATION
}

/**
 * Data layer class to be mapped as [TzKtDelegation] on the domain layer
 */
@Serializable
@SerialName("TzKtDelegation")
internal data class TzKtDelegationResponse(
    override val type: String,
    override val id: String,
    override val level: Int,
    override val hash: String,
    override val timestamp: String,
    override val block: String? = null,
    override val status: String? = null,
    override val bakerFee: Int,
    override val gasLimit: Int,
    override val errors: List<TzKtOperationError>? = null,
    val sender: TzKtAlias,
    val prevDelegate: TzKtAlias? = null,
    val newDelegate: TzKtAlias? = null,
    @Serializable(with = BigIntegerSerializer::class)
    val amount: BigInteger
) : TzKtOperationResponse

internal fun TzKtDelegationResponse.map(): TzKtDelegation {
    return TzKtDelegation(
        this.id,
        this.level,
        this.hash,
        this.timestamp,
        this.block,
        OperationResultStatus.get(this.status),
        OperationFees(
            Tez(this.bakerFee.toString()),
            gasLimit,
            0,
            null
        ),
        this.errors,
        this.sender,
        this.prevDelegate,
        this.newDelegate,
        Tez(amount.toString())
    )
}
