/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos

import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.bcd.GetAccountBalanceRPC
import io.camlcase.kotlintezos.data.bcd.GetAccountTokenBalanceRPC
import io.camlcase.kotlintezos.data.bcd.GetOperationByHashRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.bcd.BCDOperation
import io.camlcase.kotlintezos.model.bcd.dto.map
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import io.github.vjames19.futures.jdk8.zip
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Better Call Dev API wrapper (BCD for short). The Tezos contract explorer and developer dashboard exposes an API that
 * can be consumed through this client.
 *
 * [See the API](https://api.better-call.dev/v1/docs/index.html)
 */
class BCDClient(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService = Executors.newCachedThreadPool(),
) {

    /**
     * Constructor which uses [BETTER_CALL_DEV_URL].
     */
    constructor() : this(DefaultNetworkClient(BETTER_CALL_DEV_URL), Executors.newCachedThreadPool())

    /**
     * Retrieve the operation details of a given [hash].
     *
     * @param hash Operation hash
     * @param callback With a list of implementations of [TzKtOperation]. Empty if nothing was found.
     */
    fun getOperationDetails(hash: String, callback: TezosCallback<List<BCDOperation>>) {
        networkClient.send(GetOperationByHashRPC(hash), callback)
    }

    /**
     * Retrieve the balance and details of a certain [address].
     *
     * @param address Tezos address to query
     * @param network Tezos network
     * @param callback To return the [BCDAccount] entity.
     */
    fun getAccountBalance(
        address: Address,
        network: TezosNetwork,
        callback: TezosCallback<BCDAccount>
    ) {
        networkClient.send(GetAccountBalanceRPC(network, address))
            .zip(
                networkClient.send(GetAccountTokenBalanceRPC(network, address)),
                executorService
            ) { accountResponse, tokensResponse ->
                val tokens = tokensResponse?.balances?.map {
                    it.map()
                } ?: emptyList()

                accountResponse?.map(tokens)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    result?.let {
                        callback.onSuccess(result)
                    } ?: callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                })
    }

    /**
     * Retrieve the user account by Better-call.dev.
     * To fetch also the token balances, use [getAccountBalance]
     *
     * @param address Tezos address of the account
     * @param network Tezos network
     * @param callback To return the [BCDAccount] entity.
     */
    fun getAccount(
            address: Address,
            network: TezosNetwork,
            callback: TezosCallback<BCDAccount>
    ) {
        networkClient.send(GetAccountBalanceRPC(network, address))
                .map(executorService) {
                    it?.map(emptyList()) ?: throw TezosError(TezosErrorType.UNEXPECTED_RESPONSE)
                }
                .onComplete(
                        onFailure = { throwable ->
                            callback.onFailure(throwable.wrap())
                        },
                        onSuccess = { result ->
                            callback.onSuccess(result)
                        })
    }

    companion object {
        const val BETTER_CALL_DEV_URL = "https://api.better-call.dev/v1"
    }
}
