/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.core.ext

import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.NetworkClient
import io.github.vjames19.futures.jdk8.onComplete
import java.util.concurrent.CompletableFuture

/**
 * Avoid using a [TezosCallback] and call a higher-order function instead.
 */
fun <T> NetworkClient?.send(rpc: RPC<T>, onSuccess: (T?) -> Unit, onError: (TezosError) -> Unit) {
    this?.send(rpc, object : TezosCallback<T> {
        override fun onSuccess(item: T?) {
            onSuccess(item)
        }

        override fun onFailure(error: TezosError) {
            onError(error)
        }
    })
}

/**
 * Convert a Java [CompletableFuture] to a [TezosCallback]
 */
fun <T> CompletableFuture<T>.toCallback(callback: TezosCallback<T>) {
    this.onComplete(
        onFailure = { throwable ->
            callback.onFailure(throwable.wrap())
        },
        onSuccess = { result ->
            callback.onSuccess(result)
        })
}
