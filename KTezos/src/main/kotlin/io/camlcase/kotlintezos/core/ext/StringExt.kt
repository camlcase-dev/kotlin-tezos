/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.core.ext

import io.camlcase.kotlintezos.core.parser.toJsonArray
import io.camlcase.kotlintezos.core.parser.toJsonElement
import io.camlcase.kotlintezos.core.parser.toJsonObject
import io.camlcase.kotlintezos.data.Payload
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String.removeWhitespaces() = this.replace("\\s".toRegex(), "")

internal fun String?.equalsToWildcard(stringWithWildcard: String): Boolean {
    val regex = stringWithWildcard.replace("?", ".?")
        .replace("*", ".*?")
    return this?.matches(Regex(regex)) == true
}

/**
 * Returns a JSON encoded string representation of a given string.
 */
internal fun String?.toJson(): String? {
    return this?.let { "\"" + it + "\"" }
}

/**
 * Converts a map of content to a [JsonArray] of one element.
 */
internal fun Map<String, Any>?.toJsonArray(): JsonArray? {
    return try {
        val jsonObject = this.toJsonElement() as JsonObject
        return JsonArray(listOf(jsonObject))
    } catch (e: Exception) {
        null
    }

}

internal fun Payload?.jsonPayload(): String? {
    val listPayload = this?.listPayload()
    return if (listPayload.isNullOrEmpty()) {
        this?.payload.toJson()
    } else {
        try {
            val jsonArray = listPayload.toJsonArray()
            jsonArray.toString()
        } catch (e: Exception) {
            null
        }
    }
}

internal fun Map<String, Any?>?.toJson(): String? {
    return try {
        val jsonObject = this?.toJsonObject()
        jsonObject?.toString()
    } catch (e: Exception) {
        null
    }
}

// ISO 8601 format
// Quoted "Z" to indicate UTC, no timezone offset
const val UTC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

/**
 * Map [TZKT_DATE_FORMAT] to Date
 */
fun String.getDate(format: String = UTC_DATE_FORMAT): Date? {
    return try {
        val df: DateFormat = SimpleDateFormat(format, Locale.getDefault())
        df.timeZone = TimeZone.getTimeZone("UTC")
        df.parse(this)
    } catch (e: ParseException) {
        return null
    }
}

fun String.toLowerCaseUS(): String {
    return this.toLowerCase(Locale.US)
}

fun String.toUpperCaseUS(): String {
    return this.toUpperCase(Locale.US)
}

