/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser

import io.camlcase.kotlintezos.core.ext.toUpperCaseUS
import io.camlcase.kotlintezos.core.parser.toPrimitiveMap
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.RPCErrorType
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

/**
 * Parse a JSON String to a readable [RPCErrorResponse].
 * Uses Kotlinx.serialisation.
 */
internal class RPCErrorParser : Parser<RPCErrorResponse> {
    override fun parse(jsonData: String): RPCErrorResponse? {
        val parsedMap = Json.parseToJsonElement(jsonData) as? JsonObject
        return parsedMap?.let {
            parse(it)
        }
    }

    /**
     * @param map A Kotlinx serialised Map
     */
    fun parse(map: Map<String, Any?>): RPCErrorResponse? {
        return try {
            val kind = (map["kind"] as JsonPrimitive).content.toUpperCaseUS()
            val message = (map["msg"] as? JsonPrimitive)?.content?.replace("\n", " ")
            val cause = (map["id"] as JsonPrimitive).content
            val contract =
                (map["contract"] as? JsonPrimitive)?.content
                    ?: (map["contract_handle"] as? JsonPrimitive)?.content

            var contractMessage: String? = null
            var scriptArgs: JsonObject? = null
            (map["with"] as? JsonObject)?.apply {
                scriptArgs = this
                contractMessage = (this["string"] as? JsonPrimitive)?.content
            }
            RPCErrorResponse(
                RPCErrorType.valueOf(kind),
                cause,
                message ?: contractMessage,
                contract,
                scriptArgs?.toPrimitiveMap()
            )
        } catch (e: Exception) {
            null
        }
    }
}
