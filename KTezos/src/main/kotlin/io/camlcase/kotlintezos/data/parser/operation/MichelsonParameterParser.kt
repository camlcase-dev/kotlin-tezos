/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.operation

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.BooleanMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.BytesMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.LeftMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MichelineConstants
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MultiPairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.NoneMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.RightMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.SomeMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.UnitMichelsonParameter
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import java.math.BigInteger

/**
 * Recursive parsing of Michelson parameters of a smart contract.
 */
class MichelsonParameterParser : Parser<MichelsonParameter> {
    override fun parse(jsonData: String): MichelsonParameter {
        val parsedMap = Json.parseToJsonElement(jsonData) as? JsonObject
        return parse(parsedMap!!)
    }

    fun parse(map: Map<String, Any?>): MichelsonParameter {
        val prim = (map["prim"] as? JsonPrimitive)?.content ?: map["prim"] as? String
        return if (prim != null) {
            parsePrim(prim, map)
        } else {
            val key = map.keys.elementAt(0)
            parseArg(key, map)
        }
    }

    private fun parsePrim(prim: String, map: Map<String, Any?>): MichelsonParameter {
        fun parseArgs(map: Map<String, Any?>): MichelsonParameter {
            return (map["args"] as? JsonArray)?.let {
                parse(it[0] as JsonObject)
            } ?: (map["args"] as ArrayList<Map<String, Any?>>).let {
                parse(it[0])
            }
        }

        return when (MichelineConstants.get(prim)) {
            MichelineConstants.LEFT -> {
                LeftMichelsonParameter(parseArgs(map))
            }
            MichelineConstants.NONE -> {
                NoneMichelsonParameter()
            }
            MichelineConstants.PAIR -> {
                parsePair(map)
            }
            MichelineConstants.RIGHT ->
                RightMichelsonParameter(parseArgs(map))
            MichelineConstants.SOME ->
                SomeMichelsonParameter(parseArgs(map))
            MichelineConstants.FALSE,
            MichelineConstants.TRUE ->
                BooleanMichelsonParameter(prim.toBoolean() ?: false)
            else -> {
                UnitMichelsonParameter()
            }
        }
    }

    private fun parseArg(key: String, map: Map<String, Any?>): MichelsonParameter {
        return when (MichelsonComparable.get(key)) {
            MichelsonComparable.PRIM -> {
                parsePrim(key, map)
            }
            MichelsonComparable.BOOL -> {
                val boolean = (map[key] as? JsonPrimitive)?.content ?: map[key] as String
                BooleanMichelsonParameter(boolean.equals("TRUE", true))
            }
            MichelsonComparable.BYTES -> {
                val bytes = (map[key] as? JsonPrimitive)?.content ?: map[key] as String
                BytesMichelsonParameter(bytes.hexStringToByteArray())
            }
            MichelsonComparable.INT,
            MichelsonComparable.NAT -> {
                val integer = (map[key] as? JsonPrimitive)?.content ?: map[key] as String
                BigIntegerMichelsonParameter(BigInteger(integer))
            }

            MichelsonComparable.STRING,
            MichelsonComparable.ADDRESS,
            MichelsonComparable.KEY_HASH,
            MichelsonComparable.TIMESTAMP,
            MichelsonComparable.MUTEZ -> {
                val string = (map[key] as? JsonPrimitive)?.content ?: map[key] as String
                StringMichelsonParameter(string)
            }
            else -> UnitMichelsonParameter()
        }
    }

    private fun parsePair(map: Map<String, Any?>): MichelsonParameter {
        return (map["args"] as? JsonArray)?.let { args ->
            if (args.size > 2) {
                val listArgs: List<MichelsonParameter> = args.map {
                    parse((it as JsonObject).toMap())
                }
                 MultiPairMichelsonParameter(listArgs)
            } else {
                PairMichelsonParameter(
                    parse((args.get(0) as JsonObject).toMap()),
                    parse((args.get(1) as JsonObject).toMap())
                )
            }

        } ?: (map["args"] as ArrayList<Map<String, Any>>).let { args ->
            if (args.size > 2) {
                val listArgs: List<MichelsonParameter> = args.map {
                    parse(it)
                }
                 MultiPairMichelsonParameter(listArgs)
            } else {
                PairMichelsonParameter(
                    parse(args[0]),
                    parse(args[1])
                )
            }
        }
    }

}
