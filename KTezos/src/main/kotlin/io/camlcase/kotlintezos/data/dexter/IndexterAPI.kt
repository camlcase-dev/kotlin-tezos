/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.dexter

import io.camlcase.kotlintezos.core.ext.toLowerCaseUS
import io.camlcase.kotlintezos.data.Header
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.BigIntegerParser
import io.camlcase.kotlintezos.data.parser.indexter.IndexterBalanceParser
import io.camlcase.kotlintezos.data.parser.indexter.IndexterCurrencyListParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.indexter.IndexterCurrency
import java.math.BigInteger

class GetAllowanceAPI(
    tezosNetwork: TezosNetwork = TezosNetwork.MAINNET,
    tokenContractAddress: Address,
    dexterContractAddress: Address,
    source: Address
) : RPC<BigInteger>(
    endpoint = String.format(
        ENDPOINT_PATH_ALLOWANCE,
        tokenContractAddress,
        source,
        dexterContractAddress
    ),
    header = listOf(Header.plainTextHeader),
    parser = BigIntegerParser(),
    queryParameters = tezosNetwork.getIndexterParameter()?.mapKeys { it.key.paramName }
) {
    companion object {
        // /fa12/{contract}/allowance/{owner}/{spender}
        const val ENDPOINT_PATH_ALLOWANCE = "/fa12/%s/allowance/%s/%s"

        const val NO_ALLOWANCE_ERROR = "\"failed to parse allowance from response\""
    }
}

/**
 * Get the [source] balance of FA1.2 token's [tokenContractAddress].
 *
 * @param force InDEXter has an inbuilt delay to refreshing balances for performance.
 * Use true to force a refresh of those balances. By default false.
 */
class GetFABalance(
    network: TezosNetwork,
    tokenContractAddress: Address,
    source: Address,
    force: Boolean = false,
) : RPC<BigInteger>(
    endpoint = parseEndpoint(tokenContractAddress, source, force),
    header = listOf(Header.plainTextHeader),
    parser = IndexterBalanceParser(),
    queryParameters = network.getIndexterParameter()?.mapKeys { it.key.paramName }
) {
    companion object {
        // /fa12/{contract}/balance/{owner}
        const val ENDPOINT_PATH_BALANCE = "/fa12/%s/balance/%s"

        // /fa12/{contract}/balance/{owner}/now
        const val ENDPOINT_PATH_BALANCE_NOW = "$ENDPOINT_PATH_BALANCE/now"

        const val NO_BALANCE_ERROR = "\"failed to get fa1.2 balance for"

        internal fun parseEndpoint(
            tokenContractAddress: Address,
            source: Address,
            force: Boolean
        ): String {
            return if (force) {
                String.format(ENDPOINT_PATH_BALANCE_NOW, tokenContractAddress, source)
            } else {
                String.format(ENDPOINT_PATH_BALANCE, tokenContractAddress, source)
            }
        }
    }
}

/**
 * List tokens supported, depending on [environment]
 */
class GetCurrencies(
    private val environment: IndexterEnvironmentParam
) : RPC<List<IndexterCurrency>>(
    endpoint = ENDPOINT_PATH_CURRENCIES,
    header = listOf(Header.plainTextHeader),
    parser = IndexterCurrencyListParser(),
    queryParameters = mapOf(IndexterParameter.ENVIRONMENT.name to environment.name.toLowerCaseUS())
) {
    companion object {
        const val ENDPOINT_PATH_CURRENCIES = "/currencies"
    }
}
