/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser

import io.camlcase.kotlintezos.core.ext.parseErrors
import io.camlcase.kotlintezos.core.ext.parseGeneralErrors
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.SimulatedFees
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.booleanOrNull
import kotlinx.serialization.json.intOrNull

/**
 * Parse the resulting JSON from a run-operation to a [SimulationResponse].
 *
 * @param constants Network constants to parse the burn fees (if any).
 */
internal class SimulationResponseParser(
    private val constants: NetworkConstants
) : Parser<SimulationResponse> {
    private val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    /**
     * @throws TezosError When the response is of type Error.
     * If there's operation errors, they will be found in[SimulationResponse.errors]
     */
    override fun parse(jsonData: String): SimulationResponse? {
        val errors = parseGeneralErrors(jsonData)
        if (!errors.isNullOrEmpty()) {
            throw TezosError(TezosErrorType.RPC_ERROR, errors)
        }

        val parsedMap = format.parseToJsonElement(jsonData) as? JsonObject
        parsedMap?.let { map ->
            return try {
                parseSimulation(map)
            } catch (e: TezosError) {
                throw e
            } catch (e: Exception) {
                null
            }
        } ?: return null
    }

    /**
     * @throws TezosError
     */
    @Suppress("NestedBlockDepth")
    private fun parseSimulation(map: JsonObject): SimulationResponse {
        val simulations = ArrayList<SimulatedFees>()
        val internalErrors: ArrayList<RPCErrorResponse> = ArrayList()

        val contents = map["contents"] as JsonArray
        for (content in contents) {
            val contentMap = content as JsonObject

            val type: OperationType =
                OperationType.get((contentMap["kind"] as JsonPrimitive).content)
            val metadata = contentMap["metadata"] as Map<*, *>
            val results = metadata["operation_result"] as Map<*, *>

            val resultsErrors = checkResultErrors(results)
            if (!resultsErrors.isNullOrEmpty()) {
                internalErrors.addAll(resultsErrors)
            }

            var operationExtraFees = ExtraFees()
            parseAllocationFee(results)?.apply {
                operationExtraFees.add(this)
            }
            parseBurnFee(results)?.apply {
                operationExtraFees.add(this)
            }

            var consumedGas = (results["consumed_gas"] as? JsonPrimitive)?.intOrNull ?: 0
            var consumedStorage = (results[STORAGE_FEE] as? JsonPrimitive)?.intOrNull ?: 0

            val internalResults = metadata["internal_operation_results"] as? List<*>
            internalResults?.let {
                for (internalResult in it) {
                    val parsedResult = parseInternalOperationResult(internalResult)

                    consumedGas += parsedResult.consumedGas
                    consumedStorage += parsedResult.consumedStorage
                    operationExtraFees += parsedResult.extraFees

                    if (!parsedResult.errors.isNullOrEmpty()) {
                        internalErrors.addAll(parsedResult.errors)
                    }
                }
            }
            simulations.add(
                SimulatedFees(
                    type,
                    operationExtraFees,
                    consumedGas,
                    consumedStorage
                )
            )
        }

        return SimulationResponse(
            simulations,
            internalErrors
        )
    }

    private fun parseBurnFee(results: Map<*, *>): BurnFee? {
        try {
            val sizeDiff = (results["paid_storage_size_diff"] as? JsonPrimitive)?.intOrNull ?: 0
            if (sizeDiff > 0) {
                val burn = sizeDiff * constants.costPerByte.toInt()
                val burnFee = Tez(burn.toString())
                return BurnFee(burnFee)
            }

        } catch (e: NumberFormatException) {
            throw TezosError(TezosErrorType.UNEXPECTED_RESPONSE, exception = e)
        }
        return null
    }

    private fun parseAllocationFee(results: Map<*, *>): AllocationFee? {
        // Sender is charged a burn fee to keep destination active/allocated when destination's balance is empty
        val allocateDestination =
            (results["allocated_destination_contract"] as? JsonPrimitive)?.booleanOrNull == true
        if (allocateDestination) {
            val updates = results["balance_updates"] as List<*>
            if (updates.size > 2) {
                val value = ((updates[2] as Map<*, *>)["change"] as JsonPrimitive).content
                val fee = Tez(value.drop(1)) // Format is "-XTZ", drop first char
                return AllocationFee(fee)
            }
        }
        return null
    }

    private fun checkResultErrors(results: Map<*, *>): List<RPCErrorResponse>? {
        val status = (results["status"] as JsonPrimitive).content
        return when (OperationResultStatus.get(status)) {
            OperationResultStatus.SUCCESS,
            OperationResultStatus.APPLIED -> {
                null
            }
            else -> parseErrors(results)
        }
    }

    private fun parseInternalOperationResult(internalResult: Any?): InternalOperationResult {
        val result = (internalResult as Map<*, *>)["result"] as Map<*, *>

        val internalConsumedGas = (result["consumed_gas"] as? JsonPrimitive)?.intOrNull ?: 0
        val internalConsumedStorage = (result[STORAGE_FEE] as? JsonPrimitive)?.intOrNull ?: 0
        val extraFees = ExtraFees()
        parseAllocationFee(result)?.apply {
            extraFees.add(this)
        }
        parseBurnFee(result)?.apply {
            extraFees.add(this)
        }

        val moreResult = internalResult["result"] as Map<*, *>
        val status = OperationResultStatus.get((moreResult["status"] as JsonPrimitive).content)
        val errors = when (status) {
            OperationResultStatus.SUCCESS,
            OperationResultStatus.APPLIED -> {
                null
            }
            else -> parseErrors(moreResult)
        }

        return InternalOperationResult(
            internalConsumedGas,
            internalConsumedStorage,
            extraFees,
            status,
            errors
        )
    }

    private data class InternalOperationResult(
        val consumedGas: Int,
        val consumedStorage: Int,
        val extraFees: ExtraFees,
        val status: OperationResultStatus,
        val errors: List<RPCErrorResponse>?
    )


    companion object {
        private const val STORAGE_FEE = "paid_storage_size_diff"
    }

}


