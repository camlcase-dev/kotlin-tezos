/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.conseil

import io.camlcase.kotlintezos.data.parser.conseil.ConseilOperationListParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.conseil.ConseilOperation
import io.camlcase.kotlintezos.model.operation.OperationType
import java.util.*
import kotlin.collections.ArrayList

/**
 * An RPC which fetches operations for a Tezos account.
 *
 * @param source The account to query.
 * @param types Operation types to query. Default: transactions.
 * @param status Operation status to query. Default: fetch all of them.
 * @param orderBy Sorting param for the query. Default: timestamp DESC
 * @param getType Either RECEIVED or SENT
 */
class GetOperationsRPC(
    source: Address,
    limit: Int,
    types: List<OperationType> = listOf(OperationType.TRANSACTION),
    status: List<ConseilPredicateType.OperationStatus> = ConseilPredicateType.OperationStatus.values().toList(),
    orderBy: ConseilSortBy = ConseilSortBy(ConseilSort.TIMESTAMP),
    getType: GetTransactionType
) : ConseilRPC<List<ConseilOperation>>(
    createQuery(source, limit, types, status, orderBy, getType),
    ConseilQueryEntity.OPERATIONS,
    ConseilOperationListParser()
) {

    companion object {
        private fun createQuery(
            source: Address,
            limit: Int,
            types: List<OperationType>,
            status: List<ConseilPredicateType.OperationStatus>,
            orderBy: ConseilSortBy,
            getType: GetTransactionType
        ): ConseilQuery {
            val predicates = ArrayList<ConseilPredicate>()
            if (!types.isEmpty()) {
                // kind IN ['transaction', 'delegation', ...]
                predicates.add(getKindPredicate(types))
            }

            // destination/source EQ ['tz1...']
            predicates.add(getSourcePredicate(source, getType))

            if (!status.isEmpty()) {
                // status IN ['applied', 'backtracked', ...]
                predicates.add(getStatusPredicate(status))
            }

            return ConseilQuery(predicates, orderBy, limit)
        }

        private fun getKindPredicate(types: List<OperationType>): ConseilPredicate {
            val typesQuery = if (types.isEmpty()) {
                listOf(OperationType.TRANSACTION.name.toLowerCase(Locale.ROOT))
            } else {
                types.map { it.name.toLowerCase(Locale.ROOT) }
            }
            return ConseilPredicate(
                ConseilPredicateType.Field.KIND,
                typesQuery,
                ConseilPredicateType.Operation.IN
            )
        }

        private fun getSourcePredicate(source: Address, getType: GetTransactionType): ConseilPredicate {
            val sourceType = when (getType) {
                GetTransactionType.RECEIVED -> ConseilPredicateType.Field.DESTINATION
                GetTransactionType.SENT -> ConseilPredicateType.Field.SOURCE
                GetTransactionType.CALL -> return ConseilPredicate(
                    ConseilPredicateType.Field.PARAMETERS,
                    listOf(source),
                    ConseilPredicateType.Operation.LIKE
                )
            }
            return ConseilPredicate(sourceType, listOf(source))
        }

        private fun getStatusPredicate(status: List<ConseilPredicateType.OperationStatus>): ConseilPredicate {
            val statusQuery = if (status.isEmpty()) {
                ConseilPredicateType.OperationStatus.values().map { it.name.toLowerCase(Locale.ROOT) }
            } else {
                status.map { it.name.toLowerCase(Locale.ROOT) }
            }
            return ConseilPredicate(
                ConseilPredicateType.Field.STATUS,
                statusQuery,
                ConseilPredicateType.Operation.IN
            )
        }
    }
}

enum class GetTransactionType {
    /**
     * Address is destination of the transaction
     */
    RECEIVED,

    /**
     * Address is source of the transaction.
     */
    SENT,

    /**
     * Address is in parameters of a smart contract transaction
     */
    CALL
}
