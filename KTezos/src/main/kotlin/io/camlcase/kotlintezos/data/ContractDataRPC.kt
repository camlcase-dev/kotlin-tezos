/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data

import io.camlcase.kotlintezos.core.ext.toJson
import io.camlcase.kotlintezos.data.blockchain.RPC_PATH_CONTRACTS
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter

const val RPC_PATH_CONTRACT_STORAGE_SUFFIX = "/storage"
const val RPC_PATH_BIG_MAP_SUFFIX = "/big_map_get"

/**
 * An RPC that will retrieve the storage of a given contract.
 * @param address KT1 address to retrieve the info about
 */
internal class GetContractStorageRPC(address: Address) : RPC<Map<String, Any?>>(
    endpoint = RPC_PATH_CONTRACTS + address + RPC_PATH_CONTRACT_STORAGE_SUFFIX,
    parser = StringMapParser()
)

/**
 * An RPC that will retrieve the value of a big map for the given key.
 * @param address The address of a smart contract with a big map.
 * @param key The key in the big map to look up.
 */
internal class GetBigMapValueRPC(
    address: Address,
    key: MichelsonParameter,
    type: MichelsonComparable
) : RPC<Map<String, Any?>>(
    endpoint = RPC_PATH_CONTRACTS + address + RPC_PATH_BIG_MAP_SUFFIX,
    header = listOf(Header.plainTextHeader),
    parser = StringMapParser(),
    payload = parsePayload(key, type)
) {

    companion object {
        const val PAYLOAD_ARG_KEY = "key"
        const val PAYLOAD_ARG_PRIM = "prim"
        const val PAYLOAD_ARG_TYPE = "type"

        private fun parsePayload(key: MichelsonParameter, type: MichelsonComparable): String? {
            val payload = HashMap<String, Any>()
            payload[PAYLOAD_ARG_KEY] = key.payload
            val primMap = HashMap<String, Any>()
            primMap[PAYLOAD_ARG_PRIM] = type.name.toLowerCase()
            payload[PAYLOAD_ARG_TYPE] = primMap
            return payload.toJson()
        }
    }
}
