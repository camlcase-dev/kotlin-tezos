/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.conseil

import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType.Operation.EQUAL


/**
 * Entities that may be queried in the Conseil API.
 * @see https://cryptonomic.github.io/ConseilJS/#/?id=entities
 */
enum class ConseilQueryEntity {
    ACCOUNTS,
    BAKERS,
    BALLOTS,
    BLOCKS,
    FEES,
    OPERATIONS,
    OPERATION_GROUPS,
    PROPOSALS
}

/**
 * How should the [ConseilQueryEntity] be sorted
 */
enum class ConseilSort {
    TIMESTAMP,
    BLOCK_LEVEL;

    /**
     * ASC for ascending, DESC for descending
     */
    enum class ConseilSortDirection {
        ASC,
        DESC
    }
}

/**
 * Filters for the query
 */
sealed class ConseilPredicateType {

    enum class Field {
        KIND,
        DESTINATION,
        SOURCE,
        STATUS,
        PARAMETERS
    }

    /**
     * Status of a blockchain transaction
     */
    enum class OperationStatus {
        APPLIED,
        FAILED,
        SKIPPED,
        BACKTRACKED;

       companion object{
           fun get(status: String?): OperationStatus {
               val found = values().firstOrNull { it.name.equals(status, true) }
               return found ?: FAILED
           }
       }
    }

    /**
     * Predicate operation
     *
     * Field [Operation] Value
     * E.g. [STATUS] [EQUAL] [APPLIED] / status = 'applied'
     */
    enum class Operation(val value: String) {
        AFTER("after"),
        EQUAL("eq"),
        GREATER_THAN("gt"),
        IN("in"),
        IS_NULL("isNull"),
        LESS_THAN("lt"),
        LIKE("like")
    }
}
