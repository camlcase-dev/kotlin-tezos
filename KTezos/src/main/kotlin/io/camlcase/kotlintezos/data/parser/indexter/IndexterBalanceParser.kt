/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.indexter

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.BigIntegerParser
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.RPCErrorType
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.indexter.IndexterError
import kotlinx.serialization.json.Json
import java.math.BigInteger

class IndexterBalanceParser : Parser<BigInteger> {
    private val errorParser = IndexterErrorParser()

    override fun parse(jsonData: String): BigInteger? {
        val error = errorParser.parse(jsonData)
        if (error != null) {
            throw TezosError(
                TezosErrorType.RPC_ERROR, listOf(
                    RPCErrorResponse(
                        RPCErrorType.TEMPORARY,
                        "${error.error} (${error.uuid})"
                    )
                ),
                error.code
            )
        }
        return BigIntegerParser().parse(jsonData)
    }
}

/**
 * Parses an error that come from Indexter
 */
class IndexterErrorParser : Parser<IndexterError> {
    private val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): IndexterError? {
        if (jsonData.contains(UUID)) {
            return format.decodeFromString(IndexterError.serializer(), jsonData)
        }
        return null
    }

    companion object {
        private const val UUID = "uuid"
    }
}
