/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.tzkt

import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation

/**
 * Returns a list of transaction operations.
 *
 * [See more](https://api.tzkt.io/#operation/Operations_GetTransactions)
 *
 * @param parameters Filters for the http query
 */
class GetTransactionsRPC(
    private val parameters: Map<GetTransactionsByParameter, String>,
) : RPC<List<TzKtOperation>>(
    endpoint = TzKtClient.SUPPORTED_VERSION + ENDPOINT_PATH,
    queryParameters = parameters.mapKeys { it.key.parameter },
    parser = TzKtOperationListParser()
) {

    /**
     * [GetOperationsByAddressRPC] doesn't return FA1.2 token receives. Use this constructor to add the needed query parameters.
     */
    constructor(source: Address, filters: Map<GetTransactionsByParameter, String>?) : this(
        parametersQueryForTokenReceives(source, filters)
    )

    companion object {
        internal const val ENDPOINT_PATH = "/operations/transactions"

        private fun parametersQueryForTokenReceives(
            source: Address,
            filters: Map<GetTransactionsByParameter, String>?
        ): Map<GetTransactionsByParameter, String> {
            val defaults = mapOf(
                GetTransactionsByParameter.ENTRYPOINT to "transfer",
                GetTransactionsByParameter.PARAMETER_TO to source,
                GetTransactionsByParameter.INITIATOR_NULL to "true",
                GetTransactionsByParameter.SORT_DESC to "level"
            )
            val parameters = HashMap<GetTransactionsByParameter, String>(defaults)
            if (!filters.isNullOrEmpty()) {
                parameters.putAll(filters)
            }
            return parameters
        }
    }
}

/**
 * Query parameters for a list of TzKt Transaction operations.
 */
enum class GetTransactionsByParameter(paramName: String, filter: String? = null) {
    /**
     * Filters transactions by entrypoint called on the target contract.
     */
    ENTRYPOINT("entrypoint"),

    /**
     * Filters transactions by parameter value. Note, this query parameter supports the following format:
     * ?parameter{.path?}{.mode?}=..., so you can specify a path to a particular field to filter by.
     */
    PARAMETER_TO("parameter", "to"),

    /**
     * Filters transactions by initiator value. Is null filter mode.
     * Use this mode to get items where the specified field is null or not.
     *
     * Example: ?initiator.null or ?initiator.null=false.
     */
    INITIATOR_NULL("initiator", "null"),

    /**
     * Filters transactions by timestamp. Specify a datetime to get items where the specified field is equal to the specified value.
     * Example: ?timestamp=2020-02-20T02:40:57Z.
     */
    TIMESTAMP_EQ("timestamp", "eq"),

    /**
     * Filters transactions by timestamp. Specify a datetime to get items where the specified field is greater than the specified value.
     * Example: ?timestamp.gt=2020-02-20T02:40:57Z
     */
    TIMESTAMP_GT("timestamp", "gt"),

    /**
     * Sorts transactions by specified field. Supported fields:
     * id (default), level, gasUsed, storageUsed, bakerFee, storageFee, allocationFee, amount.
     * Ascending sort mode.
     */
    SORT_ASC("sort", "asc"),

    /**
     * Sorts transactions by specified field. Supported fields:
     * id (default), level, gasUsed, storageUsed, bakerFee, storageFee, allocationFee, amount.
     * Descending sort mode.
     */
    SORT_DESC("sort", "desc"),

    /**
     * Maximum number of items to return.
     * Default: 100
     */
    LIMIT("limit");

    val parameter: String = if (filter.isNullOrBlank()) {
        paramName
    } else {
        "$paramName.$filter"
    }
}
