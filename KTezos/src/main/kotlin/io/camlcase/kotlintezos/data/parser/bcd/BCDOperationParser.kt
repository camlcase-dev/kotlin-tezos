/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.bcd

import io.camlcase.kotlintezos.core.ext.toUpperCaseUS
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDDelegation
import io.camlcase.kotlintezos.model.bcd.BCDDelegationEntity
import io.camlcase.kotlintezos.model.bcd.BCDOperation
import io.camlcase.kotlintezos.model.bcd.BCDOrigination
import io.camlcase.kotlintezos.model.bcd.BCDOriginationEntity
import io.camlcase.kotlintezos.model.bcd.BCDReveal
import io.camlcase.kotlintezos.model.bcd.BCDRevealEntity
import io.camlcase.kotlintezos.model.bcd.BCDTransaction
import io.camlcase.kotlintezos.model.bcd.BCDTransactionEntity
import io.camlcase.kotlintezos.model.bcd.dto.BCDOperationEntity
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

/**
 * JSON to [BCDOperation] converter. For use in [RPC]s.
 */
class BCDOperationParser : Parser<BCDOperation> {
    private val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): BCDOperation? {
        StringMapParser().parse(jsonData)?.apply {
            val type: OperationType = OperationType.get(this["kind"] as String)
            val entity = when (type) {
                OperationType.TRANSACTION -> format.decodeFromString(BCDTransactionEntity.serializer(), jsonData)
                OperationType.DELEGATION -> format.decodeFromString(BCDDelegationEntity.serializer(), jsonData)
                OperationType.REVEAL -> format.decodeFromString(BCDRevealEntity.serializer(), jsonData)
                OperationType.ORIGINATION -> format.decodeFromString(BCDOriginationEntity.serializer(), jsonData)
            }
            return mapFrom(entity)
        }

        return null
    }

    private fun mapFrom(entity: BCDOperationEntity): BCDOperation {
        return when (entity) {
            is BCDTransactionEntity -> {
                map(entity)
            }
            is BCDDelegationEntity -> {
                map(entity)
            }
            is BCDRevealEntity -> {
                map(entity)
            }
            is BCDOriginationEntity -> {
                map(entity)
            }
            else -> throw TezosError(
                TezosErrorType.INTERNAL_ERROR,
                exception = IllegalStateException("BCDOperationEntity of type ${entity.kind} not supported")
            )
        }
    }

    private fun map(entity: BCDTransactionEntity): BCDTransaction {
        return BCDTransaction(
            entity.id,
            entity.hash,
            entity.protocol,
            TezosNetwork.valueOf(entity.network.toUpperCaseUS()),
            entity.timestamp,
            OperationResultStatus.get(entity.status),
            deserializeOperationFees(entity),
            entity.errors,
            entity.internal,
            TzKtAlias(entity.sourceAlias, entity.source),
            TzKtAlias(entity.destinationAlias, entity.destination),
            Tez(entity.amount.toString()),
            entity.entrypoint
        )
    }

    private fun map(entity: BCDDelegationEntity): BCDDelegation {
        return BCDDelegation(
            entity.id,
            entity.hash,
            entity.protocol,
            TezosNetwork.valueOf(entity.network.toUpperCaseUS()),
            entity.timestamp,
            OperationResultStatus.get(entity.status),
            deserializeOperationFees(entity),
            entity.errors,
            entity.source,
            entity.delegate
        )
    }

    private fun map(entity: BCDRevealEntity): BCDReveal {
        return BCDReveal(
            entity.id,
            entity.hash,
            entity.protocol,
            TezosNetwork.valueOf(entity.network.toUpperCaseUS()),
            entity.timestamp,
            OperationResultStatus.get(entity.status),
            deserializeOperationFees(entity),
            entity.errors,
            entity.publicKey
        )
    }

    private fun map(entity: BCDOriginationEntity): BCDOrigination {
        return BCDOrigination(
            entity.id,
            entity.hash,
            entity.protocol,
            TezosNetwork.valueOf(entity.network.toUpperCaseUS()),
            entity.timestamp,
            OperationResultStatus.get(entity.status),
            deserializeOperationFees(entity),
            entity.errors,
            entity.source
        )
    }

    companion object {
        private fun deserializeOperationFees(entity: BCDOperationEntity): OperationFees {
            return OperationFees(
                Tez(entity.fee.toString()),
                entity.gas_limit,
                entity.storage_limit,
                deserializeExtraFees(entity.result?.storageDiff ?: 0, entity.allocated_destination_contract_burned ?: 0)
            )
        }

        @Suppress("MagicNumber")
        private fun deserializeExtraFees(sizeDiff: Int, allocationFee: Int): ExtraFees {
            val extras = ExtraFees()
            if (sizeDiff > 0) {
                extras.add(BurnFee(Tez(sizeDiff * 0.001)))
            }
            if (allocationFee > 0) {
                extras.add(AllocationFee(Tez(allocationFee.toString())))
            }
            return extras
        }
    }
}

/**
 * JSON to [BCDOperation] list converter. For use in [RPC]s.
 * @see BCDOperationParser
 */
class BCDOperationListParser : Parser<List<BCDOperation>> {
    private val operationParser = BCDOperationParser()

    override fun parse(jsonData: String): List<BCDOperation>? {
        val operations = ArrayList<BCDOperation>()
        val list =  Json.parseToJsonElement(jsonData) as JsonArray
        for (item in list) {
            parseOperation(item)?.apply {
                operations.add(this)
            }
        }
        return operations
    }

    @Suppress("TooGenericExceptionCaught")
    private fun parseOperation(item: Any): BCDOperation? {
        (item as? JsonObject)?.let { map ->
            try {
                return operationParser.parse(map.toString())
            } catch (e: Exception) {
                throw TezosError(TezosErrorType.INTERNAL_ERROR, exception = e)
            }
        }
        return null
    }

}
