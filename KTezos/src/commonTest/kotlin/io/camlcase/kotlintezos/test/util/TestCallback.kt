/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.util

import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import org.junit.Assert
import java.util.concurrent.CountDownLatch

/**
 * For successful callbacks
 */
internal class TestSuccessCallback<T>(
    private val countDownLatch: CountDownLatch,
    val verification: (T?) -> Unit = {}
) : TezosCallback<T> {
    override fun onSuccess(item: T?) {
        println("*** SUCCESS $item")
        Assert.assertNotNull(item)
        verification(item)
        countDownLatch.countDown()
    }

    override fun onFailure(error: TezosError) {
        println("*** FAILURE $error")
        countDownLatch.countDown()
        Assert.fail("onFailure shouldn't be called")
    }
}

/**
 * For failing callbacks
 */
internal class TestFailureCallback<T>(
    private val countDownLatch: CountDownLatch,
    val verification: (Throwable?) -> Unit = {}
) : TezosCallback<T> {
    override fun onSuccess(item: T?) {
        println("*** SUCCESS $item")
        countDownLatch.countDown()
        Assert.fail("onSuccess shouldn't be called")
    }

    override fun onFailure(error: TezosError) {
        println("*** FAILURE $error")
        Assert.assertNotNull(error)
        verification(error)
        countDownLatch.countDown()
    }
}
