/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.util

import io.camlcase.kotlintezos.data.ForgeOperationRPC.Companion.RPC_PATH_FORGE_SUFFIX
import io.camlcase.kotlintezos.data.ForgeOperationRPC.Companion.RPC_PATH_OPERATION_PREFIX
import io.camlcase.kotlintezos.data.GetDelegateRPC.Companion.RPC_PATH_DELEGATE_SUFFIX
import io.camlcase.kotlintezos.data.ParseOperationRPC.Companion.RPC_PATH_PARSE_SUFFIX
import io.camlcase.kotlintezos.data.PreapplyOperationRPC.Companion.RPC_PATH_PREAPPLY_SUFFIX
import io.camlcase.kotlintezos.data.RPC_PATH_BIG_MAP_SUFFIX
import io.camlcase.kotlintezos.data.RPC_PATH_CONTRACT_STORAGE_SUFFIX
import io.camlcase.kotlintezos.data.blockchain.RPC_PATH_CONTRACTS
import io.camlcase.kotlintezos.data.blockchain.RPC_PATH_SUFFIX_COUNTER
import io.camlcase.kotlintezos.data.blockchain.RPC_PATH_SUFFIX_MANAGER_KEY
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.kotlintezos.model.operation.OperationParams
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.model.operation.payload.OperationWithCounter
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SignatureProvider

internal class RPCConstants {
    companion object {
        const val RPC_GET_BALANCE = "/chains/main/blocks/head/context/contracts/*/balance"
        const val RPC_GET_MANAGER_KEY = "$RPC_PATH_CONTRACTS*$RPC_PATH_SUFFIX_MANAGER_KEY"
        const val RPC_GET_COUNTER = "$RPC_PATH_CONTRACTS*$RPC_PATH_SUFFIX_COUNTER"
        const val RPC_FORGE_OPERATION = "$RPC_PATH_OPERATION_PREFIX*$RPC_PATH_FORGE_SUFFIX"
        const val RPC_PREAPPLY_OPERATION = "$RPC_PATH_OPERATION_PREFIX*$RPC_PATH_PREAPPLY_SUFFIX"
        const val RPC_GET_DELEGATE = "$RPC_PATH_CONTRACTS*$RPC_PATH_DELEGATE_SUFFIX"
        const val RPC_GET_CONTRACT_STORAGE = "$RPC_PATH_CONTRACTS*$RPC_PATH_CONTRACT_STORAGE_SUFFIX"
        const val RPC_GET_BIG_MAP_VALUE = "$RPC_PATH_CONTRACTS*$RPC_PATH_BIG_MAP_SUFFIX"
        const val RPC_PARSE_OPERATION = "$RPC_PATH_OPERATION_PREFIX*$RPC_PATH_PARSE_SUFFIX"

        const val FORGE_RESULT =
            "8a700ab69b834bc5aa3b892f809bf2dc2cfdb61700fd8cd3197884954aec76e56c00b9232920ab58cabc9fa969b5cae8a87bf8827fe79d0a80d80880ea30e0d4030100007659a8eb542f8e3cfb24259dc6650dec5e9a1a3300"
        const val INJECT_RESULT = "ooB7xxBrYmZMVFfotEYFJuexVGNrFEZT9Z8FvweExj6RcYoM1x6"
        const val STORAGE_RESULT = "{\"string\":\"KT194hQVCPCsCYUhDjbeUCwpKcKyzN3AZLbc\"}"
        const val BIG_MAP_RESULT = "{\"prim\":\"Pair\",\"args\":[{\"int\":\"1000000\"}]}\n"
    }
}

internal class TestnetAddress {
    companion object {
        const val TEST_ADDRESS_1 = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"
        const val TEST_ADDRESS_2 = "tz1MBB3wYuwunkC3Mtw6FpVzc689q4bLrvNP"
    }
}

internal val fakeSignature = object : SignatureProvider {
    override val publicKey: PublicKey
        get() = PublicKey(ByteArray(8))

    override fun sign(hex: String): ByteArray {
        return ByteArray(8)
    }
}

/**
 * Testnet tokens
 */
internal object Token {
    val USDTZ_EDO = Pair(
        // contractAddress
        "KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH",
        // dexterAddress
        "KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE",
    )
}

/**
 * Align tests with camlKit
 */
internal object Wallet {
    val mnemonic = "remember smile trip tumble era cube worry fuel bracket eight kitten inform".split(" ")
    const val passphrase = "superSecurePassphrase"
    const val messageToSign = "something very interesting that needs to be signed"

    object SDWalletEd255519 {
        const val address: Address = "tz1T3QZ5w4K11RS3vy4TXiZepraV9R5GzsxG"
        const val privateKey =
            "80d4e52897c8e14fbfad4637373de405fa2cc7f27eb9f890db975948b0e7fdb0cd33a22f74d8e04977f74db15a0b1e92d21a59f351e987b9fd462bf6ef2dc253"
        const val publicKey = "cd33a22f74d8e04977f74db15a0b1e92d21a59f351e987b9fd462bf6ef2dc253"
        const val signedData =
            "c4d20c77d627d8c07e3f26ddc2e8ab9324471c65f9abd412de70a81c21ddc153dcfad1b31ab777a83c4e8a5dc021ea30d84da107dea4a192fc2ca9da9b3ede00"
        const val base58Encoded = "edpkvCbYCa6d6g9hEcK6tvwgsY9jfB4HDzp3jZSBwfuWNSvxE5T5KR"
    }

    object SDWalletEd255519_Pwd {
        const val address: Address = "tz1hQ4wkVfNAh3eGeaDpoTBmQ9KjX9ZMzc6q"
        const val privateKey =
            "b17877f6b326bf75e8a5bf2bd7e457a03b103d469c869ef4e3b0473d9b9d50b1482c29dcbfc1f94c185e9d8da1ee7e06b16239a5d4e15a64a6f4150c298ab029"
        const val publicKey = "482c29dcbfc1f94c185e9d8da1ee7e06b16239a5d4e15a64a6f4150c298ab029"
    }

    object HDWallet {
        const val address: Address = "tz1bQnUB6wv77AAnvvkX5rXwzKHis6RxVnyF"
        const val privateKey =
            "3f54684924468bc4a7044729ee578176aef5933c69ad16ff3340412e4a5ca396"
        const val publicKey = "1e4291f2501ce283e55ce583d4388ec8d247dd6c72fff3ff2d48b2af84cc9a23"
        const val signedData =
            "3882ed4ec04ca29e12f3aecea23f50297f8c569c21092dfcef3196eff55ac39a637c2da93d348935a88110f30b177c052f17c432d888a06c9eba434cc3707601"
        const val base58Encoded = "edpktsYtWnwnuiLkHySdNSihWEChdeFzyz9v8Vdb8aAWRibsPH7g7E"
    }

    object HDWallet_Pwd {
        const val address: Address = "tz1dcAi3sVtc5xLyNu53avkEiPvhi9cN2idj"
        const val privateKey =
            "f76414eebdfea880ada1dad22186f150335547a124d64a65a10fbda25d70dac1"
        const val publicKey = "9ce8d8d68df863ed6e2c7a8172726f3bd71ddf7e968ba0095bfdf549fea7d67c"
    }

    object HDWallet_Non_Hardened {
        const val address: Address = "tz1gr7B9AsXJMMSBg7NugNvDZpGgvcvX4Q8M"
        const val privateKey =
            "2cd4e64b16e9bb9fa7c19aa272bd2dba0d9ae3ee3f764e15351ab7ec11fc37ad"
        const val publicKey = "caa6f27ebb93808caf3ee7ecbff0fc482bbde9e123293bf34d78dbca105e02c2"
        val derivationPath = "m/44'/1729'/0/0"
    }

    object HDWallet_Pwd_Hardened_Path {
        const val address: Address = "tz1iKnE1sKYvB6F42XAKAf9iR6YauEv2ENzZ"
        const val privateKey =
            "8ad51941e26afcdd6408ff59d94188449180bdbda71d8529fafd1948eca8af04"
        const val publicKey = "ace4ba9cc11a961a78324700ed36feb0277ee005d7cb79ccdb821503029bc672"
        val derivationPath = "m/44'/1729'/0'/1'"
    }
}

internal class Params {
    companion object {
        val MNEMONIC = listOf(
            "soccer",
            "click",
            "number",
            "muscle",
            "police",
            "corn",
            "couch",
            "bitter",
            "gorilla",
            "camp",
            "camera",
            "shove",
            "expire",
            "praise",
            "pill"
        )

        const val fakeAddress = "tz1ADDRESS"
        val fakeFees = OperationFees(Tez(1.0), 200, 300)

        val testNetworkConstants = NetworkConstants(
            "1040000",
            "60000",
            "10400000",
            257,
            "250"
        )
        val fakeMetadata =
            BlockchainMetadata(
                "FakeBranch",
                "FakeProtocol",
                "FakeChainId",
                1234,
                null,
                testNetworkConstants
            )
        val fakeTransactionParams = OperationParams.Transaction(Tez(1.0), "tz1FROM", "tz1TO")
        val fakeTransactionOperation =
            OperationFactory.createOperation(
                OperationType.TRANSACTION,
                fakeTransactionParams,
                fakeFees
            )!!
        val fakeOperationWithCounter = OperationWithCounter(fakeTransactionOperation, 1234)
        val fakeOperationPayload = OperationPayload(listOf(fakeOperationWithCounter), "FakeBranch")


        // HD Test Wallet 1
        val hdMnemonic = "zone north mass twist smile bullet afford grab kick spell fresh defy".split(" ")
    }
}
