/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.util

import io.camlcase.kotlintezos.TezosNodeClient.Companion.EDONET_GIGA_NODE
import io.camlcase.kotlintezos.model.TezosError
import io.github.vjames19.futures.jdk8.onComplete
import org.junit.Assert
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CountDownLatch

fun <T> CompletableFuture<T>.testFailure(countDownLatch: CountDownLatch, verification: (Throwable?) -> Unit = {}) {
    this.onComplete(
        CurrentThreadExecutor(),
        onFailure = {
            println("*** FAILURE $it")
            Assert.assertNotNull(it)
            Assert.assertTrue(it is TezosError)
            verification(it)
            countDownLatch.countDown()
        },
        onSuccess = {
            println("*** SUCCESS $it")
            countDownLatch.countDown()
            Assert.fail("onSuccess shouldn't be called")
        })
}


const val VALIDATOR_NODE_URL = EDONET_GIGA_NODE
