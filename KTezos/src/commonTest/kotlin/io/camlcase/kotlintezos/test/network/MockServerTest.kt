package io.camlcase.kotlintezos.test.network

import io.camlcase.kotlintezos.network.DefaultNetworkClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import java.util.concurrent.CountDownLatch

/**
 * Base class to inherit on integration tests with a mock server.
 */
open class MockServerTest {
    internal lateinit var webServer: MockWebServer
    val mockClient = DefaultNetworkClient(TEST_URL, emptyList(), TestClientWrapper())
    private val mockDispatcher = MockDispatcher()
    internal lateinit var countDownLatch: CountDownLatch

    @Before
    @Throws(Exception::class)
    fun setup() {
        webServer = MockWebServer()
        webServer.start(8080)
        countDownLatch = CountDownLatch(1)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        webServer.shutdown()
    }

    fun callMockDispatcher() {
        webServer.dispatcher = mockDispatcher.getDispatcher()
    }

    fun callErrorDispatcher() {
        webServer.dispatcher = mockDispatcher.getErrorDispatcher()
    }

    companion object {
        const val TEST_URL = "http://localhost:8080"
    }
}
