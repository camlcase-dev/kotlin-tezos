/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.network

import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.network.defaultClientBuilder
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import okhttp3.Authenticator
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import java.io.InputStream
import java.util.*

/**
 * Client wrapper for camlCase Testnet node.
 */
class CamlTezosClient(
    private val debug: Boolean = false,
    private val credentials: Pair<String, String>?
) : ClientWrapper<OkHttpClient> {
    override val client: OkHttpClient by lazy {
        val builder = defaultClientBuilder(30, debug)
        credentials?.let {
            builder.authenticator(object : Authenticator {
                override fun authenticate(route: Route?, response: Response): Request? {
                    val credential = Credentials.basic(it.first, it.second)
                    return response.request.newBuilder()
                        .header("Authorization", credential)
                        .build()
                }

            })
        }

        builder.build()
    }
}

/**
 * local_test.properties:
 * - node-testnet
 * - node-testnet-parsing
 * - tzkt-testnet-url
 * - indexter-testnet-url
 */
class CamlCredentials {

    private val properties: Properties by lazy {
        val properties = Properties()
        val inputStream: InputStream =
            this.javaClass.classLoader.getResourceAsStream("local_test.properties")
        properties.load(inputStream)
        properties
    }

    fun nodeUrl(): String {
        return properties.getProperty("node-testnet")
    }

    fun nodeCredentials(): Pair<String, String>? {
//        val username = properties.getProperty("node-testnet-user")
//        val pass = properties.getProperty("node-testnet-password")
//        return Pair(username, pass)
        return null
    }

    fun verifier(): String {
        return try {
            properties.getProperty("node-testnet-parsing")
        } catch (e: java.lang.NullPointerException) {
            TezosNodeClient.GRANADANET_SMARTPY_NODE
        }
    }

    fun tzKtTestnetUrl(): String? {
        return properties.getProperty("tzkt-testnet-url")
    }

    fun createNetworkClient(nodeUrl: String? = null): DefaultNetworkClient {
        return DefaultNetworkClient(
            nodeUrl ?: nodeUrl(),
            emptyList(),
            CamlTezosClient(true, nodeCredentials())
        )
    }

    fun createValidatorNetworkClient(): DefaultNetworkClient {
        return DefaultNetworkClient(
            verifier(),
            emptyList(),
            CamlTezosClient(true, null)
        )
    }

    fun indexterUrl(): String {
        return try {
            properties.getProperty("indexter-testnet-url")
        } catch (e: NullPointerException) {
            ""
        }
    }
}

fun tezosNodeClient(): TezosNodeClient {
    val credentials = CamlCredentials()
    val executor = CurrentThreadExecutor()
    val client = credentials.createNetworkClient()
    val validatorClient = credentials.createValidatorNetworkClient()
    return TezosNodeClient(
        client,
        executor,
        BlockchainMetadataService(client, executor),
        ForgingService(client, validatorClient, executor)
    )
}
