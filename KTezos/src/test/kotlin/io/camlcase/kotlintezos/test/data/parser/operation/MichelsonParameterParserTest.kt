/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.operation

import io.camlcase.kotlintezos.core.ext.toJson
import io.camlcase.kotlintezos.data.parser.operation.MichelsonParameterParser
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.LeftMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MultiPairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class MichelsonParameterParserTest {
    @Test
    fun `Test parse map Send Fa12 tokens`() {
        val input =
            "{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1RGPPkKmW1SsGVfKdEmTQU94xciCyQPqhB\"},{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}]},{\"int\":\"2\"}]}"
        val result = MichelsonParameterParser().parse(input)
        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result is PairMichelsonParameter)
    }


    @Test
    fun `Test parse Pair sides`() {
        val michelson = LeftMichelsonParameter(
            LeftMichelsonParameter(
                PairMichelsonParameter(
                    BigIntegerMichelsonParameter(BigInteger.ONE),
                    PairMichelsonParameter(
                        BigIntegerMichelsonParameter(BigInteger("100")),
                        StringMichelsonParameter("2020-06-29T18:00:21Z")
                    )
                )
            )
        )
        val json = michelson.payload.toJson()
        val result = MichelsonParameterParser().parse(json!!)
        println(result)

        Assert.assertNotNull(result)
        Assert.assertEquals(json, result!!.payload.toJson())
    }

    @Test
    fun `test Dexter xtzToToken`() {
        val input =
            "{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"16\"},{\"string\":\"2020-11-06T09:51:02Z\"}]}]}"
        val result = MichelsonParameterParser().parse(input)
        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result is PairMichelsonParameter)

    }

    @Test
    fun `test Dexter storage MultiPair Edo`() {
        val input =
            "{\"args\":[{\"int\":\"410\"},{\"args\":[{\"prim\":\"False\"},{\"prim\":\"False\"},{\"int\":\"0\"}],\"prim\":\"Pair\"},{\"args\":[{\"string\":\"tz1RY96NB6CWNDxy1sfqB2hXUwoCYxdw48Mm\"},{\"string\":\"KT19vLuzhGpe5F64jkxgzqrfRkSVprL4cPa5\"}],\"prim\":\"Pair\"},{\"int\":\"0\"},{\"int\":\"0\"}],\"prim\":\"Pair\"}"
        val result = MichelsonParameterParser().parse(input)
        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result is MultiPairMichelsonParameter)

    }
}
