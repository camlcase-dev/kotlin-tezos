/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.model

import io.camlcase.kotlintezos.model.Tez
import org.junit.Assert
import org.junit.Test

class TezArithmeticsTest {
    @Test
    fun testAddTwoWholeNumbers() {
        var left = Tez(3.0)
        val right = Tez(2.0)
        val expected = Tez(5.0)

        val actual = left + right
        Assert.assertEquals(expected, actual)

        left += right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testAddTwoDecimalsNoCarry() {
        var left = Tez(1.1)
        val right = Tez(2.2)
        val expected = Tez(3.3)

        val actual = left + right
        Assert.assertEquals(expected, actual)

        left += right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testAddTwoDecimalsWithCarry() {
        var left = Tez(1.6)
        val right = Tez(2.7)
        val expected = Tez(4.3)

        val actual = left + right
        Assert.assertEquals(expected, actual)

        left += right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testSubtractTwoWhoNumbers() {
        var left = Tez(3.0)
        val right = Tez(2.0)
        val expected = Tez(1.0)

        val actual = left - right
        Assert.assertEquals(expected, actual)

        left -= right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testSubtractTwoDecimalsNoCarry() {
        var left = Tez(3.3)
        val right = Tez(2.2)
        val expected = Tez(1.1)

        val actual = left - right
        Assert.assertEquals(expected, actual)

        left -= right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testSubtractTwoDecimalsWithCarry() {
        var left = Tez(4.3)
        val right = Tez(1.6)
        val expected = Tez(2.7)

        val actual = left - right
        Assert.assertEquals(expected, actual)

        left -= right
        Assert.assertEquals(expected, left)
    }

    @Test
    fun testGreaterThan() {
        Assert.assertTrue(Tez(1.0) > Tez(0.5))
        Assert.assertFalse(Tez(0.5) > Tez(1.0))
    }

    @Test
    fun testLessThan() {
        Assert.assertFalse(Tez(1.0) < Tez(0.5))
        Assert.assertTrue(Tez(0.5) < Tez(1.0))
    }
}
