/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.conseil

import io.camlcase.kotlintezos.data.parser.conseil.OperationGroupParser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test

class OperationGroupParserTest {
    @Test
    fun test_Included_TransactionOperation() {
        val input = MockDispatcher.getStringFromFile("conseil_transaction_operation_detail.json")
        val result = OperationGroupParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals("NetXUdfLh6Gm88t", result!!.chainId)
        Assert.assertEquals("ooBjBXMNJf15NMBRWeNo6rxp96qLzhLzsE3PiBdVPr4qdjDjTLT", result.hash)
        Assert.assertEquals("BKw7khkHvG2JrF55fC53WSPPNNV3UeAdkvKhwoFPT4S64zK5PA4", result.branch)
        Assert.assertEquals(
            "sigswz6Qrwa7CdWhZwqtqeemDtB5MqVhPT2yhaYbft8kXsB2gT8uSRydBe6fkXASQQoUCk4gc7FPrw4ddr3zEvBqLnE9zjbd",
            result.signature
        )
        Assert.assertEquals(1, result.details.size)
        Assert.assertEquals(OperationType.TRANSACTION, result.details[0].type)
        Assert.assertEquals(null, result.details[0].originatedContract)
        Assert.assertEquals(null, result.details[0].errors)
    }

    @Test
    fun test_NotIncluded_Operation() {
        val input = "null"
        val result = OperationGroupParser().parse(input)

        Assert.assertNull(result)
    }

    @Test
    fun test_Included_Origination_Operation() {
        val input = MockDispatcher.getStringFromFile("conseil_origination_operation_detail.json")
        val result = OperationGroupParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals("NetXUdfLh6Gm88t", result!!.chainId)
        Assert.assertEquals("opYdWBU69HYZmb7BjbVuhvNWVXoyLSoGCwVzFVknjwTpewvx3Yv", result.hash)
        Assert.assertEquals(1, result.details.size)
        Assert.assertEquals(OperationType.ORIGINATION, result.details[0].type)
        Assert.assertEquals("KT1F2DowtrTnvLTWFSz7iJwYgqyfJvfLnowg", result.details[0].originatedContract)
        Assert.assertEquals(null, result.details[0].errors)
    }

    @Test
    fun test_Failed_Exchange_Operation() {
        val input = MockDispatcher.getStringFromFile("conseil_exchange_error_operation_detail.json")
        val result = OperationGroupParser().parse(input)

        val expectedResults =
            listOf("proto.006-PsCARTHA.michelson_v1.runtime_error", "proto.006-PsCARTHA.michelson_v1.script_rejected")
        Assert.assertNotNull(result)
        Assert.assertEquals("NetXjD3HPJJjmcd", result!!.chainId)
        Assert.assertEquals("onxqn7QaNuVD8yBXswyFcDYyxYvqi96gRFXqrtUkpXkmG2VUGs1", result.hash)
        Assert.assertEquals(1, result.details.size)
        Assert.assertEquals(OperationType.TRANSACTION, result.details[0].type)
        Assert.assertEquals(expectedResults, result.details[0].errors)
        Assert.assertEquals(null, result.details[0].originatedContract)
    }

    @Test
    fun test_Failed_Exchange_Operation_OneError() {
        val input =
            "{\"operation_group\":{\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"chainId\":\"NetXjD3HPJJjmcd\",\"hash\":\"onxqn7QaNuVD8yBXswyFcDYyxYvqi96gRFXqrtUkpXkmG2VUGs1\",\"branch\":\"BLYU19u2KPJU58m4fcVh3j4fcMpNKmZQXvs3BBv1dgb4ZHgN7Gt\",\"signature\":\"sigV7nqU8TygoY4J8TXUhspd5VqHqS1KNSvEsnxdXm9f3XyuSsktJRMzRGSu3Gzn9PMaUjCTugd1tRn3bV8qDLZHXPzgWNqf\",\"blockId\":\"BMMevCP5oKZ37LisqzmhHnnNXD1v9aZtTf6Kwud59HfdWjP9pXq\",\"blockLevel\":367892},\"operations\":[{\"cycle\":179,\"operationId\":4685056,\"operationGroupHash\":\"onxqn7QaNuVD8yBXswyFcDYyxYvqi96gRFXqrtUkpXkmG2VUGs1\",\"kind\":\"transaction\",\"source\":\"tz1LRwRZHvWqhM485RXrfVrRmngJfs5TGv6n\",\"fee\":60349,\"counter\":725628,\"gasLimit\":597884,\"storageLimit\":22481,\"amount\":1000000,\"destination\":\"KT1PxBTZFsBd1gUXhkTFpncDTjvrKbmTeHde\",\"parameters\":\"Pair (Pair \\\"tz1LRwRZHvWqhM485RXrfVrRmngJfs5TGv6n\\\" 25) \\\"2020-04-24T13:24:00Z\\\"\",\"parametersEntrypoints\":\"xtzToToken\",\"parametersMicheline\":\"{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"2020-04-24T12:54:15Z\\\"},{\\\"int\\\":\\\"27502\\\"}]}\",\"status\":\"failed\",\"blockHash\":\"BMMevCP5oKZ37LisqzmhHnnNXD1v9aZtTf6Kwud59HfdWjP9pXq\",\"blockLevel\":367892,\"internal\":false,\"period\":179,\"timestamp\":1587732875000,\"errors\":\"proto.006-PsCARTHA.michelson_v1.runtime_error\",\"utcYear\":2020,\"utcMonth\":4,\"utcDay\":24,\"utcTime\":\"12:54:35\"}]}"
        val result = OperationGroupParser().parse(input)

        val expectedResults =
            listOf("proto.006-PsCARTHA.michelson_v1.runtime_error")
        Assert.assertNotNull(result)
        Assert.assertEquals("NetXjD3HPJJjmcd", result!!.chainId)
        Assert.assertEquals("onxqn7QaNuVD8yBXswyFcDYyxYvqi96gRFXqrtUkpXkmG2VUGs1", result.hash)
        Assert.assertEquals(1, result.details.size)
        Assert.assertEquals(OperationType.TRANSACTION, result.details[0].type)
        Assert.assertEquals(expectedResults, result.details[0].errors)
        Assert.assertEquals(null, result.details[0].originatedContract)
    }

    @Test
    fun test_Exchange_Operation() {
        val input = MockDispatcher.getStringFromFile("conseil_exchange_success_operation_detail.json")
        val result = OperationGroupParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals("NetXjD3HPJJjmcd", result!!.chainId)
        Assert.assertEquals("onsSmHFeTTnLx7W2xLC8epna7aonMmueTd1nxpynuBUKfv9ZeUu", result.hash)
        Assert.assertEquals(2, result.details.size)
        Assert.assertEquals(OperationType.TRANSACTION, result.details[0].type)
        Assert.assertEquals(OperationResultStatus.APPLIED, result.details[0].status)
        Assert.assertEquals(null, result.details[0].originatedContract)
        Assert.assertEquals(null, result.details[0].errors)
    }
}
