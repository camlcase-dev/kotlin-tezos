/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.exchange

import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.data.dexter.GetAllowanceAPI
import io.camlcase.kotlintezos.data.dexter.GetFABalance
import io.camlcase.kotlintezos.exchange.IndexterService
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.testFailure
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class IndexterServiceTest : MockServerTest() {

    val mockTokenAddress = "KT1"
    val mockAddress = "tz1"

    val defaultDispatcher = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when {
                request.path?.equals(
                    String.format(
                        GetAllowanceAPI.ENDPOINT_PATH_ALLOWANCE,
                        mockTokenAddress,
                        mockAddress,
                        ""
                    )
                ) == true ->
                    validResponse().setBody("0")
                request.path?.equalsToWildcard(
                    String.format(
                        GetFABalance.ENDPOINT_PATH_BALANCE,
                        mockTokenAddress,
                        mockAddress
                    )
                ) == true -> validResponse().setBody("12345")
                request.path?.equalsToWildcard(
                    String.format(
                        GetFABalance.ENDPOINT_PATH_BALANCE_NOW,
                        mockTokenAddress,
                        mockAddress
                    )
                ) == true -> validResponse().setBody(
                    "54321"
                )
                else -> error404()
            }
        }
    }

    @Test
    fun test_Defaults_OK() {
        webServer.dispatcher = defaultDispatcher
        val service = IndexterService(mockClient, CurrentThreadExecutor())
        service.getTokenBalance(
            tokenContractAddress = mockTokenAddress,
            address = mockAddress,
            callback = object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertEquals(BigInteger("12345"), item)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            })

        service.getTokenBalance(
            tokenContractAddress = mockTokenAddress,
            address = mockAddress,
            forceRefresh = true,
            callback = object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertEquals(BigInteger("54321"), item)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            })

        service.getAllowance(
            TezosNetwork.EDONET,
            mockTokenAddress,
            "",
            mockAddress,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertEquals(BigInteger.ZERO, item)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    @Test
    fun test_Calls_KO() {
        callErrorDispatcher()
        val service = IndexterService(mockClient, CurrentThreadExecutor())

        service.getTokenBalance(
            tokenContractAddress = "",
            address = "",
            callback = object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertNotNull(error)
                    countDownLatch.countDown()
                }

            })
        service.getAllowance(TezosNetwork.ZERONET, "", "", "")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    @Test
    fun test_getTokenBalances_OK() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return validResponse().setBody("12345")
            }
        }
        val service = IndexterService(mockClient, CurrentThreadExecutor())

        val tokenContractAddresses = listOf("KT1", "KT2", "KT3", "KT4")
        val expected = BigInteger("12345")
        service.getTokenBalances(
            tokenContractAddresses = tokenContractAddresses,
            address = "",
            callback = object : TezosCallback<Map<Address, BigInteger>> {
                override fun onSuccess(item: Map<Address, BigInteger>?) {
                    Assert.assertNotNull(item)
                    Assert.assertTrue(item!!.size == tokenContractAddresses.size)
                    for (address in tokenContractAddresses) {
                        Assert.assertTrue(item[address] == expected)
                    }

                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

    @Test
    fun test_getTokenBalances_Half_KO() {
        // First will go OK but others won't
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return if (request.path?.equalsToWildcard(
                        String.format(
                            GetFABalance.ENDPOINT_PATH_BALANCE,
                            mockTokenAddress,
                            mockAddress
                        )
                    ) == true
                ) {
                    validResponse().setBody("12345")
                } else {
                    error404()
                }
            }
        }
        val service = IndexterService(mockClient, CurrentThreadExecutor())

        val tokenContractAddresses = listOf("KT1", "KT2", "KT3", "KT4")
        val expected = BigInteger("12345")
        service.getTokenBalances(
            tokenContractAddresses = tokenContractAddresses,
            address = mockAddress,
            callback = object : TezosCallback<Map<Address, BigInteger>> {
                override fun onSuccess(item: Map<Address, BigInteger>?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertNotNull(error)
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

    @Test
    fun test_getTokenBalances_200_KO() {
        // First will go OK but others won't
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return validResponse().setBody("{\"status_code\":200,\"uuid\":\"526d4d4b-00d8-49d1-a082-38dc3031e1d6\",\"error\":\"failed to get fa1.2 balance for 'tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5' in contract 'KT19vLuzhGpe5F64jkxgzqrfRkSVprL4cPa5': failed to parse balance from response\"}")
            }
        }
        val service = IndexterService(mockClient, CurrentThreadExecutor())

        val tokenContractAddresses = listOf("KT1", "KT2", "KT3", "KT4")
        val expected = BigInteger("12345")
        service.getTokenBalances(
            tokenContractAddresses = tokenContractAddresses,
            address = mockAddress,
            callback = object : TezosCallback<Map<Address, BigInteger>> {
                override fun onSuccess(item: Map<Address, BigInteger>?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertNotNull(error)
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

}
