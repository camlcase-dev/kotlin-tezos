/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.operation

import io.camlcase.kotlintezos.operation.ForgingPolicy
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.forge.ForgingVerifier
import io.camlcase.kotlintezos.operation.forge.RemoteForgingService
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeTransactionOperation
import io.camlcase.kotlintezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.camlcase.kotlintezos.test.util.TestFailureCallback
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.test.util.testFailure
import io.github.vjames19.futures.jdk8.Future
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * Integration tests for [ForgingService]
 */
class ForgingServiceTest : MockServerTest() {
    private val testExecutor = CurrentThreadExecutor()

    // We mock it so we can provide verified true/false
    private val verifier = mockk<ForgingVerifier>(relaxed = true)
    private val remoteForging = spyk(
        RemoteForgingService(
            mockClient,
            verifier,
            testExecutor
        )
    )
    private val verification: (String?) -> Unit = {
        Assert.assertNotNull(it)
        Assert.assertEquals(FORGE_RESULT, it)
    }

    @After
    fun after() {
        confirmVerified(verifier)
        confirmVerified(remoteForging)
    }

    private fun initForgingService(): ForgingService {
        return spyk(ForgingService(remoteForging, testExecutor))
    }

    @Test
    fun `Test forge with completable OK`() {
        every { verifier.verify(any(), any(), any()) } returns Future { true }
        callMockDispatcher()
        val service = initForgingService()
        val metadata = spyk(fakeMetadata)
        val result = service.forge(
            ForgingPolicy.REMOTE,
            "tz1_Fake",
            fakeTransactionOperation,
            metadata,
            spyk(fakeSignature)
        ).join()

        verification(result)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, any(), metadata)
            remoteForging.remoteForge(any(), metadata)
            verifier.verify(any(), any(), any())
            fakeMetadata.blockHash
            fakeMetadata.key
            fakeMetadata.counter
            fakeMetadata.protocol
            fakeSignature.publicKey
        }
    }

    @Test
    fun `Test forge with completable KO`() {
        callErrorDispatcher()
        val service = initForgingService()

        val metadata = spyk(fakeMetadata)
        service.forge(
            ForgingPolicy.REMOTE,
            "tz1_Fake",
            fakeTransactionOperation,
            metadata,
            spyk(fakeSignature)
        ).testFailure(countDownLatch)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, any(), metadata)
            remoteForging.remoteForge(any(), metadata)
        }

        countDownLatch.await()
    }

// TODO Flaky test
    //
//    @Test
//    fun `Test forge with callback OK`() {
//        every { verifier.verify(any(), any(), any()) } returns Future { true }
//        callMockDispatcher()
//        val service = initForgingService()
//
//        service.forge(
//            ForgingPolicy.REMOTE,
//            fakeOperationPayload,
//            fakeMetadata,
//            TestSuccessCallback(countDownLatch, verification)
//        )
//
//        verify(exactly = 1) {
//            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
//            remoteForging.remoteForge(any(), fakeMetadata)
//            verifier.verify(fakeMetadata.blockHash, any(), any())
//        }
//        countDownLatch.await()
//    }

    @Test
    fun testRemotePolicyWithPayloadAndCallbackKO() {
        callErrorDispatcher()
        val service = initForgingService()

        service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata,
            TestFailureCallback(countDownLatch)
        )

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
            remoteForging.remoteForge(any(), fakeMetadata)
        }
        countDownLatch.await()
    }


    @Test
    fun `Test forge with completable payload OK`() {
        every { verifier.verify(any(), any(), any()) } returns Future { true }
        callMockDispatcher()
        val service = initForgingService()

        val result = service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata
        ).join()

        verification(result)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
            remoteForging.remoteForge(any(), fakeMetadata)
            verifier.verify(fakeMetadata.blockHash, any(), any())
        }
    }

    @Test
    fun testRemotePolicyWithPayloadKO() {
        callErrorDispatcher()
        val service = initForgingService()

        service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata
        ).testFailure(countDownLatch)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
            remoteForging.remoteForge(any(), fakeMetadata)
        }
        countDownLatch.await()
    }
}
