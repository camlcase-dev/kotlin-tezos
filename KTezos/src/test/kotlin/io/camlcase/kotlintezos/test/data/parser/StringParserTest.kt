/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.data.parser.StringParser
import org.junit.Assert
import org.junit.Test

class StringParserTest {
    @Test
    fun testStringParsed() {
        val string = "String"
        val result = StringParser().parse(string)
        Assert.assertEquals(string, result)
    }

    // Expect quotes to be stripped.
    @Test
    fun testQuotedString() {
        val string = "String"
        val quotedString = "\"" + string + "\""
        val result = StringParser().parse(quotedString)
        Assert.assertEquals(string, result)
    }

    // Expect whitespace to be stripped.
    @Test
    fun testWhitespaceString() {
        val string = "String"
        val quotedString = "   $string\n   \n"
        val result = StringParser().parse(quotedString)
        Assert.assertEquals(string, result)
    }

    // Test decode fails on non utf8 string
    @Test
    fun testUnexpectedEncoding() {
        val string = "🙃"
        val result = StringParser().parse(string)
        Assert.assertNull(result)
    }

    @Test
    fun testStringNull() {
        val string = "null\n    "
        val result = StringParser().parse(string)
        Assert.assertNull(result)
    }
}
