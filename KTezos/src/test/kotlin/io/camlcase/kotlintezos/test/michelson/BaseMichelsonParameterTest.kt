/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.michelson

import io.camlcase.kotlintezos.smartcontract.michelson.BaseMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MichelineConstants
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonAnnotation
import org.junit.Assert
import org.junit.Test

class BaseMichelsonParameterTest {

    @Test
    fun testBaseParameterPayload() {
        val annotations = listOf(
            MichelsonAnnotation(":type_annot"),
            MichelsonAnnotation("@var_annot"),
            MichelsonAnnotation("%field_annot")
        )
        val payload = object : BaseMichelsonParameter {
            override val annotations: List<MichelsonAnnotation>?
                get() = annotations

        }.payload
        Assert.assertNotNull(payload)
        Assert.assertTrue(payload.containsKey(MichelineConstants.ANNOTATIONS.rpcName))
        Assert.assertTrue(payload[MichelineConstants.ANNOTATIONS.rpcName] is List<*>)
        Assert.assertEquals(
            listOf(":type_annot", "@var_annot", "%field_annot"),
            payload[MichelineConstants.ANNOTATIONS.rpcName]
        )
    }

    @Test
    fun testBaseParameterNullAnnotation() {
        val annotations = listOf(
            MichelsonAnnotation(":type_annot"),
            MichelsonAnnotation("var_annot"),
            MichelsonAnnotation("%field_annot")
        )
        val payload = object : BaseMichelsonParameter {
            override val annotations: List<MichelsonAnnotation>?
                get() = annotations

        }.payload
        Assert.assertNotNull(payload)
        Assert.assertTrue(payload.containsKey(MichelineConstants.ANNOTATIONS.rpcName))
        Assert.assertTrue(payload[MichelineConstants.ANNOTATIONS.rpcName] is List<*>)
        Assert.assertEquals(listOf(":type_annot", "%field_annot"), payload[MichelineConstants.ANNOTATIONS.rpcName])
    }
}
