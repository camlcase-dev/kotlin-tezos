/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.michelson

import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonAnnotation
import org.junit.Assert
import org.junit.Test

class MichelsonAnnotationTest {
    @Test
    fun testCorrectAnnotations() {
        var annotation = ":type_annot"
        var param = MichelsonAnnotation(annotation)
        Assert.assertNotNull(param.value)
        Assert.assertEquals(annotation, param.value)

        annotation = "@var_annot"
        param = MichelsonAnnotation(annotation)
        Assert.assertNotNull(param.value)
        Assert.assertEquals(annotation, param.value)

        annotation = "%field_annot"
        param = MichelsonAnnotation(annotation)
        Assert.assertNotNull(param.value)
        Assert.assertEquals(annotation, param.value)
    }

    @Test
    fun testIncorrectAnnotations() {
        val symbols = "ABCDEFGHIJKLMNOPQRSTUWXYZ1234567890!·$/()=?¿¡'+*^´´-._;,~ºª><}{[]"
        for (char in symbols.asIterable()) {
            val param = MichelsonAnnotation(char.toString())
            Assert.assertNull(param.value)
        }
    }
}
