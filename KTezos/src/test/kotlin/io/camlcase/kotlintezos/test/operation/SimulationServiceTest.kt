/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.operation

import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeTransactionOperation
import io.camlcase.kotlintezos.test.util.Params.Companion.testNetworkConstants
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.test.util.testFailure
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CountDownLatch

class SimulationServiceTest : MockServerTest() {
    @Test
    fun testCallOK() {
        callMockDispatcher()
        val service = SimulationService(mockClient, CurrentThreadExecutor())

        val response = service.simulate(
            fakeTransactionOperation,
            "FakeFROMAddress",
            BlockchainMetadata("", "", "", 1234, null, testNetworkConstants),
            fakeSignature
        ).join()

        Assert.assertNotNull(response)
        Assert.assertEquals(10207, response.simulations[0].consumedGas)
        Assert.assertTrue(response.simulations[0].extraFees.isEmpty())
    }

    @Test
    fun testCallKO() {
        callErrorDispatcher()
        val executorService = CurrentThreadExecutor()
        val service = SimulationService(mockClient, executorService)

        val countDownLatch = CountDownLatch(1)
        service.simulate(
            fakeTransactionOperation,
            "FakeFROMAddress",
            BlockchainMetadata("", "", "", 1234, null, testNetworkConstants),
            fakeSignature
        ).testFailure(countDownLatch)

        countDownLatch.await()
    }
}
