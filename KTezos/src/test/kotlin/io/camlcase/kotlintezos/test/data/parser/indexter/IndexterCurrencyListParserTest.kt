/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.indexter

import io.camlcase.kotlintezos.data.parser.indexter.IndexterCurrencyListParser
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test

class IndexterCurrencyListParserTest {
    @Test
    fun test_Currencies_Ok() {
        val input = MockDispatcher.getStringFromFile("indexter_currencies.json")
        val result = IndexterCurrencyListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result.size == 4)
        Assert.assertTrue(result[0].symbol == "ethtz")
    }

    @Test
    fun test_Currencies_Empty() {
        val input = "[]"
        val result = IndexterCurrencyListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result.isEmpty())
    }
}
