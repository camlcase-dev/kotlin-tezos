/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.bcd

import io.camlcase.kotlintezos.data.parser.bcd.BCDAccountResponseParser
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class BCDAccountResponseParserTest {

    @Test
    fun `parse 2 tokens`() {
        val json =
            "{\"address\":\"tz1RbQRXYrjcLyHV8nu1234sEGwLnJ234joAB\",\"network\":\"delphinet\",\"balance\":231230315,\"tx_count\":7,\"last_action\":\"2020-12-02T14:47:06Z\",\"tokens\":[{\"contract\":\"KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8\",\"network\":\"mainnet\",\"token_id\":0,\"balance\":5000000000},{\"contract\":\"KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9\",\"network\":\"mainnet\",\"token_id\":0,\"symbol\":\"USDTZ\",\"name\":\"USDtz\",\"decimals\":6,\"balance\":510500},{\"contract\":\"KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn\",\"network\":\"mainnet\",\"token_id\":0,\"symbol\":\"TZBTC\",\"name\":\"tzBTC\",\"decimals\":8,\"balance\":627}]}"
        val result = BCDAccountResponseParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result.address == "tz1RbQRXYrjcLyHV8nu1234sEGwLnJ234joAB")
        Assert.assertTrue(result.balance == BigInteger("231230315"))
        Assert.assertTrue(result.tokens?.size == 3)
        Assert.assertTrue(result.tokens!![1].decimals == 6)
        Assert.assertTrue(result.tokens!![2].decimals == 8)
    }
}
