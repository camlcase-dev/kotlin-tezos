/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test.exchange.calculations

import io.camlcase.kotlintezos.core.parser.toPrimitiveMap
import io.camlcase.kotlintezos.exchange.calculations.NativeDexterCalculations
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.test.network.MockDispatcher
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger

/**
 * Test from a centralised source of truth that trade amounts are calculated correctly
 * @see https://gitlab.com/camlcase-dev/dexter-integration
 */
class NativeDexterCalculationsTest {

    @Test
    fun test_XtzToToken_Calculations() {
        val json = MockDispatcher.getStringFromFile("xtz_to_token_old.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val xtzFrom = Tez(jsonObj["xtz_in"] as String)
            val expectedTo = BigInteger(jsonObj["token_out"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val amountOfTokenToReceive =
                NativeDexterCalculations.xtzToTokenTokenOutput(xtzFrom, xtzPool, tokenPool)

            // ASSERT
            Assert.assertEquals(expectedTo, amountOfTokenToReceive)
        }
    }

    @Test
    fun test_XtzToToken_Slippage() {
        val json = MockDispatcher.getStringFromFile("xtz_to_token_old.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val xtzFrom = Tez(jsonObj["xtz_in"] as String)
            val expectedSlippage = BigDecimal(jsonObj["slippage"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val slippage =
                NativeDexterCalculations.xtzToTokenSlippage(xtzFrom, xtzPool, tokenPool)

            // ASSERT
            val resultIn4Decimals = slippage.setScale(4, BigDecimal.ROUND_DOWN)
            Assert.assertTrue(resultIn4Decimals > (expectedSlippage.subtract(BigDecimal("0.0005"))))
            Assert.assertTrue(resultIn4Decimals < (expectedSlippage.add(BigDecimal("0.0005"))))
        }
    }

    @Test
    fun test_TokenToXTZ_Calculations() {
        val json = MockDispatcher.getStringFromFile("token_to_xtz_old.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val tokenFrom = BigInteger(jsonObj["token_in"] as String)
            val expected = Tez(jsonObj["xtz_out"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val amountOfXtzToReceive =
                NativeDexterCalculations.tokenToXtzXtzOutput(tokenFrom, xtzPool, tokenPool)

            // ASSERT
            Assert.assertEquals(expected, amountOfXtzToReceive)
        }
    }

    @Test
    fun test_TokenToXTZ_Slippage() {
        val json = MockDispatcher.getStringFromFile("token_to_xtz_old.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val tokenFrom = BigInteger(jsonObj["token_in"] as String)
            val expectedSlippage = BigDecimal(jsonObj["slippage"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val slippage =
                NativeDexterCalculations.tokenToXtzSlippage(tokenFrom, xtzPool, tokenPool)

            // ASSERT
            val resultIn4Decimals = slippage.setScale(4, BigDecimal.ROUND_HALF_UP)
            Assert.assertTrue(resultIn4Decimals > (expectedSlippage.subtract(BigDecimal("0.0005"))))
            Assert.assertTrue(resultIn4Decimals < (expectedSlippage.add(BigDecimal("0.0005"))))
        }
    }

    @Test
    fun `getXtzToTokenMarketRate Decimal Pool disparity`() {
        val tezPool = Tez(23.500004)
        val tokenPool = BigDecimal("42638477.773")

        val result = NativeDexterCalculations.getXtzToTokenMarketRate(tezPool, tokenPool)

        Assert.assertEquals(BigDecimal("1814403.000654808399"), result)
    }

    @Test
    fun test_xtzToTokenMarketRate() {
        var tezPool = Tez("1000000")
        var tokenPool = BigInteger("500000000000000000000")
        var decimals = 18
        var result = NativeDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("500.000000000000000000").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        tezPool = Tez("144621788919")
        tokenPool = BigInteger("961208019")
        decimals = 8
        result = NativeDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("0.000066463568607795").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        tezPool = Tez("20167031717")
        tokenPool = BigInteger("41063990114535450000")
        decimals = 18
        result = NativeDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("0.0020361940562586686").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        tezPool = Tez("46296642164")
        tokenPool = BigInteger("110543540642")
        decimals = 6
        result = NativeDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("2.3877226398064355").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        tezPool = Tez("58392357794")
        tokenPool = BigInteger("73989702350")
        decimals = 6
        result = NativeDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("1.2671127720347453").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )
    }


    @Test
    fun `xtzToTokenExchangeRate OK`() {
        var xtzIn = Tez("1000000")
        var xtzPool = Tez("34204881343")
        var tokenPool = BigInteger("39306268")
        // based on tzBTC test data
        val exchangeRate =
            NativeDexterCalculations.xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate > BigDecimal((0.001145 - 0.0005)))
        Assert.assertTrue(exchangeRate < BigDecimal((0.001145 + 0.0005)))

        // based on USDtz test data
        xtzIn = Tez("1000000")
        xtzPool = Tez("3003226688")
        tokenPool = BigInteger("668057425")
        val exchangeRate2 =
            NativeDexterCalculations.xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate2 > BigDecimal((0.22170500 - 0.0005)))
        Assert.assertTrue(exchangeRate2 < BigDecimal((0.22170500 + 0.0005)))
    }

    @Test
    fun `xtzToTokenMinimumTokenOutput OK`() {
        val inputs = listOf(
            Triple("10000", "0.05", "9500"),
            Triple("10000", "0.01", "9900"),
            Triple("330000", "0.005", "328350"),
            Triple("1000", "0.01", "990"),
            Triple("5000", "0.2", "4000"),
            Triple("100", "0.055", "94"),
            Triple("5846941182", "0.3142", "4009832262"),
        )

        for (input in inputs) {
            val tokenOut = BigInteger(input.first)
            val allowedSlippage = input.second.toDouble()
            val expected = BigInteger(input.third)

            val result =
                NativeDexterCalculations.xtzToTokenMinimumTokenOutput(tokenOut, allowedSlippage)
            Assert.assertEquals(expected, result)
        }
    }

    @Test
    fun `tokenToXtzExchangeRate OK`() {
        var tokenIn = BigInteger("100000000")
        var xtzPool = Tez("38490742927")
        var tokenPool = BigInteger("44366268")
        // based on tzBTC test data
        val exchangeRate =
            NativeDexterCalculations.tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate > BigDecimal((266.3723523200 - 0.0005)))
        Assert.assertTrue(exchangeRate < BigDecimal((266.3723523200 + 0.0005)))

        // based on USDtz test data
        tokenIn = BigInteger("34000000")
        xtzPool = Tez("3003926688")
        tokenPool = BigInteger("667902216")
        val exchangeRate2 =
            NativeDexterCalculations.tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate2 > BigDecimal((4.26747503 - 0.0005)))
        Assert.assertTrue(exchangeRate2 < BigDecimal((4.26747503 + 0.0005)))
    }

    @Test
    fun test_tokenToXtzMarketRate() {
        var xtzPool = Tez("1000000")
        var tokenPool = BigInteger("500000000000000000000")
        var decimals = 18
        var result = NativeDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("0.0019999999999999996").setScale(
                12,
                BigDecimal.ROUND_HALF_UP
            ), result
        )

        xtzPool = Tez("144621788919")
        tokenPool = BigInteger("961208019")
        decimals = 8
        result = NativeDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("15045.836703428501").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        xtzPool = Tez("20167031717")
        tokenPool = BigInteger("41063990114535450000")
        decimals = 18
        result = NativeDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("491.11232641421907").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        xtzPool = Tez("46296642164")
        tokenPool = BigInteger("110543540642")
        decimals = 6
        result = NativeDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("0.4188091126367452").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )

        xtzPool = Tez("58392357794")
        tokenPool = BigInteger("73989702350")
        decimals = 6
        result = NativeDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(
            BigDecimal("0.7891957385878037").setScale(12, BigDecimal.ROUND_HALF_UP),
            result
        )
    }

    @Test
    fun `tokenToXtzMinimumXtzOutput OK`() {
        val inputs = listOf(
            Triple("10000", "0.05", "9500"),
            Triple("10000", "0.01", "9900"),
            Triple("330000", "0.005", "328350"),
            Triple("2739516881", "0.36", "1753290803"),
        )

        for (input in inputs) {
            val xtzOut = Tez(input.first)
            val allowedSlippage = input.second.toDouble()
            val expected = Tez(input.third)

            val result =
                NativeDexterCalculations.tokenToXtzMinimumXtzOutput(xtzOut, allowedSlippage)
            Assert.assertEquals(expected, result)
        }
    }
}
