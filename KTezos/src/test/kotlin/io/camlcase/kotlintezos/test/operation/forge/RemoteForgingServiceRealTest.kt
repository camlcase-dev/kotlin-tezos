package io.camlcase.kotlintezos.test.operation.forge

import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.OperationParams
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.forge.RemoteForgingService
import io.camlcase.kotlintezos.operation.forge.RemoteForgingVerifier
import io.camlcase.kotlintezos.test.network.CamlCredentials
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.Params.Companion.testNetworkConstants
import io.camlcase.kotlintezos.test.util.RPCConstants
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.mockk.confirmVerified
import io.mockk.spyk
import io.mockk.verify
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CompletionException
import java.util.concurrent.Executors

/**
 * Test validation against a REAL node to ensure it works.
 *
 * @see RemoteForgingService
 */
class RemoteForgingServiceRealTest : MockServerTest() {
    private val testExecutor = Executors.newCachedThreadPool()

    // Use a real node
    private val verifier =
        spyk(RemoteForgingVerifier(CamlCredentials().createValidatorNetworkClient(), testExecutor))

    private val remoteForging = spyk(
        RemoteForgingService(
            mockClient,
            verifier,
            testExecutor
        )
    )

    @After
    fun after() {
        confirmVerified(verifier)
        confirmVerified(remoteForging)
    }

    private fun init_SendXTZ_Forge_Operation(
        forgeResult: String,
        source: Address,
        to: Address,
        verification: (OperationPayload, BlockchainMetadata) -> Unit
    ) {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> validResponse().setBody(
                        forgeResult
                    )
                    else -> error404()
                }
            }
        }

        val operation = OperationFactory.createOperation(
            OperationType.TRANSACTION,
            OperationParams.Transaction(Tez(0.1), source, to),
            OperationFees(Tez("1284"), 10_307, 257)
        )

        val metadata = BlockchainMetadata(
            "BMbVe7mADArDFU1X1SVXJGqu8rykv2D4iiyw7rDoDLgr6V9LTDS",
            "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV",
            "NetXz969SFaFn8k",
            780683,
            "edpkuiazN85nCfCAqk4hHJK5M5anykQLRhn1s6FFV9xKVmKS7dyMG1",
            testNetworkConstants
        )
        val payload = OperationPayload(listOf(operation!!).asIterable(), source, metadata, fakeSignature)

        verification(payload, metadata)

        verify {
            remoteForging.remoteForge(payload, metadata)
            verifier.verify(metadata.blockHash, any(), payload)
            verifier.verify(any<Map<String, Any>>(), any<Map<String, Any>>())
        }
    }

    @Test
    fun test_CorrectParsing_SendXTZ() {
        val source = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"
        val to = "tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn"
        init_SendXTZ_Forge_Operation(forgeResult, source, to) { payload, metadata ->
            val result = remoteForging.remoteForge(
                payload, metadata
            ).join()

            Assert.assertNotNull(result)
            Assert.assertEquals(forgeResult, result)
        }
    }

    @Test
    fun test_InvalidAddress_SendXTZ() {
        val source = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"
        val to = "tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVZZ"
        init_SendXTZ_Forge_Operation(forgeResult, source, to) { payload, metadata ->
            try {
                val result = remoteForging.remoteForge(
                    payload, metadata
                ).join()

                Assert.fail("Should fail but it returned $result")
            } catch (e: CompletionException) {
                // All exceptions thrown inside the asynchronous processing of the Supplier will get wrapped into a
                // CompletionException when calling join, except the ServerException we have already wrapped in
                // a CompletionException.
                Assert.assertTrue(e.cause is TezosError)
                Assert.assertTrue((e.cause as TezosError).type == TezosErrorType.FORGING_ERROR)
            }
        }
    }

    @Test
    fun test_InvalidResponse_DifferentBranch_SendXTZ() {
        val source = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"
        val to = "tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn"
        val randomBranch = "BM2E89f6azpuntEBk7poWhUrq8kMFUsF1NSTiAAbT9aS9pbjAnj"
        val randomForgeResult =
            "ac710abfd7b3c11d3429406ab887294190bde01376d0444e379a506733e4d8756c0081744d3ad071c4fd31913c5e80da1c6c9fb9f4b3840af8e021c3508102a08d060000cf7ea25f66b3a0c6f287c01c7e0c0e9956de542000"
        init_SendXTZ_Forge_Operation(randomForgeResult, source, to) { payload, metadata ->
            try {
                val result = remoteForging.remoteForge(
                    payload, metadata
                ).join()

                Assert.fail("Should fail but it returned $result")
            } catch (e: CompletionException) {
                // All exceptions thrown inside the asynchronous processing of the Supplier will get wrapped into a
                // CompletionException when calling join, except the ServerException we have already wrapped in
                // a CompletionException.
                Assert.assertTrue(e.cause is TezosError)
                Assert.assertTrue((e.cause as TezosError).type == TezosErrorType.FORGING_ERROR)
            }
        }
    }


    companion object {
        // The forge result for a SEND 0.1 XTZ from tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5 to tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn
        private const val forgeResult =
            "3d3d2ff72a7556302130bd93a794bdce2c5144bd5a02b6efda83eadbc3503b5c6c0089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352840a8cd32fc3508102a08d0600008e8fb7d2b514711b03303c3f76e0228b1d64443900"

    }
}
