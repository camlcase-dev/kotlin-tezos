///**
// * # Released under MIT License
// *
// * Copyright (c) 2020 camlCase
// *
// * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
// * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// * permit persons to whom the Software is furnished to do so, subject to the following conditions:
// *
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// * the Software.
// *
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//package io.camlcase.kotlintezos.test.exchange
//
//import io.camlcase.kotlintezos.NetworkClient
//import io.camlcase.kotlintezos.data.dexter.IndexterEnvironmentParam
//import io.camlcase.kotlintezos.exchange.IndexterService
//import io.camlcase.kotlintezos.model.TezosCallback
//import io.camlcase.kotlintezos.model.TezosError
//import io.camlcase.kotlintezos.model.TezosNetwork
//import io.camlcase.kotlintezos.model.indexter.IndexterCurrency
//import io.camlcase.kotlintezos.network.DefaultNetworkClient
//import io.camlcase.kotlintezos.test.network.CamlCredentials
//import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
//import io.camlcase.kotlintezos.test.util.TestnetAddress
//import io.camlcase.kotlintezos.test.util.Token
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
//import java.math.BigInteger
//import java.util.concurrent.CountDownLatch
//

// TODO Uncomment if Indexter goes live again

///**
// * To be run locally. If CI involved, you need to create local_properties there.
// * @see IndexterService
// */
//class IndexterServiceRealTest {
//    private lateinit var countDownLatch: CountDownLatch
//    private var networkClient: NetworkClient? = null
//
//    @Before
//    fun setup() {
//        countDownLatch = CountDownLatch(1)
//        val url = CamlCredentials().indexterUrl()
//        if (url.isNotBlank()) {
//            networkClient = DefaultNetworkClient(
//                url,
//                true
//            )
//        }
//    }
//
//    @Test
//    fun test_NonZeroBalance() {
//        networkClient?.apply {
//            val source = TestnetAddress.TEST_ADDRESS_1
//            val tokenContract = "KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K"
//
//            val service = IndexterService(
//                this,
//                CurrentThreadExecutor()
//            )
//            service.getTokenBalance(
//                TezosNetwork.EDONET,
//                tokenContractAddress = tokenContract,
//                address = source,
//                callback = object : TezosCallback<BigInteger> {
//                    override fun onSuccess(item: BigInteger?) {
//                        Assert.assertTrue(item!! > BigInteger.ZERO)
//                        println("** SUCCESS! $item")
//                        countDownLatch.countDown()
//                    }
//
//                    override fun onFailure(error: TezosError) {
//                        println("** ERROR! $error")
//                        countDownLatch.countDown()
//                    }
//                }
//            )
//            countDownLatch.await()
//        }
//    }
//
//    @Test
//    fun test_ZeroBalance() {
//        networkClient?.apply {
//            val source = TestnetAddress.TEST_ADDRESS_2
//            val tokenContract = Token.USDTZ_EDO.first
//
//            val service = IndexterService(
//                this,
//                CurrentThreadExecutor()
//            )
//            service.getTokenBalance(
//                TezosNetwork.EDONET,
//                tokenContractAddress = tokenContract,
//                address = source,
//                callback = object : TezosCallback<BigInteger> {
//                    override fun onSuccess(item: BigInteger?) {
//                        Assert.assertEquals(BigInteger.ZERO, item)
//                        println("** SUCCESS! $item")
//                        countDownLatch.countDown()
//                    }
//
//                    override fun onFailure(error: TezosError) {
//                        Assert.fail()
//                        println("** ERROR! $error")
//                        countDownLatch.countDown()
//                    }
//                }
//            )
//
//            countDownLatch.await()
//        }
//    }
//
//    @Test
//    fun test_ZeroAllowances() {
//        networkClient?.apply {
//            val source = "tz1gUbUnthNzEPqabcSGWnbBWckLbccgC8dN"
//            val tokenContract = "KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD" // MTZ
//            val dexterContract = "KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e"
//
//            val service = IndexterService(
//                this,
//                CurrentThreadExecutor()
//            )
//            service.getAllowance(
//                TezosNetwork.EDONET,
//                tokenContract,
//                dexterContract,
//                source,
//                object : TezosCallback<BigInteger> {
//                    override fun onSuccess(item: BigInteger?) {
//                        Assert.assertEquals(BigInteger.ZERO, item)
//                        println("** SUCCESS! $item")
//                        countDownLatch.countDown()
//                    }
//
//                    override fun onFailure(error: TezosError) {
//                        println("** ERROR! $error")
//                        countDownLatch.countDown()
//                    }
//                }
//            )
//
//            countDownLatch.await()
//        }
//    }
//
//    @Test
//    fun test_Currencies() {
//        networkClient?.apply {
//            val service = IndexterService(
//                this,
//                CurrentThreadExecutor()
//            )
//            service.getCurrencies(
//                IndexterEnvironmentParam.STAGING,
//                object : TezosCallback<List<IndexterCurrency>> {
//                    override fun onSuccess(item: List<IndexterCurrency>?) {
//                        Assert.assertTrue(item!!.size == 5)
//                        countDownLatch.countDown()
//                    }
//
//                    override fun onFailure(error: TezosError) {
//                        println("** ERROR! $error")
//                        countDownLatch.countDown()
//                    }
//                }
//            )
//
//            countDownLatch.await()
//        }
//    }
//}
