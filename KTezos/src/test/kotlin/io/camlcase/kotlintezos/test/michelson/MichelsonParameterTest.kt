/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.michelson

import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.camlcase.kotlintezos.core.ext.toJson
import io.camlcase.kotlintezos.smartcontract.michelson.*
import org.junit.Assert
import org.junit.Test
import java.util.*

class MichelsonParameterTest {

    @Test
    fun testEncodeUnitToJSON() {
        val michelson = UnitMichelsonParameter()
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_UNIT, payload)
    }

    @Test
    fun testEncodeStringToJSON() {
        val payload = stringMichelsonParameter.payload.toJson()
        Assert.assertEquals(MICHELSON_STRING, payload)
    }

    private fun createGMTDate(year: Int, month: Int, date: Int, hourOfDay: Int, minute: Int, second: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getTimeZone("GMT")
        calendar.set(year, month, date, hourOfDay, minute, second)
        return calendar.time
    }

    @Test
    fun testEncodeDateToJSON() {
        // Monday, June 29, 2020 6:00:21 PM, GMT
        val date = createGMTDate(2020, 5, 29, 18, 0, 21)
        val payload = StringMichelsonParameter(date).payload.toJson()
        val expected = "{\"string\":\"2020-06-29T18:00:21Z\"}"
        Assert.assertEquals(expected, payload)
    }

    @Test
    fun testEncodeIntToJSON() {
        val payload = integerMichelsonParameter.payload.toJson()
        Assert.assertEquals(MICHELSON_INT, payload)
    }

    @Test
    fun testEncodePairToJSON() {
        val michelson = PairMichelsonParameter(stringMichelsonParameter, integerMichelsonParameter)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_PAIR, payload)
    }

    @Test
    fun testEncodeLeftToJSON() {
        val michelson = LeftMichelsonParameter(stringMichelsonParameter)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_PAIR_LEFT, payload)
    }

    @Test
    fun testEncodeRightToJSON() {
        val michelson = RightMichelsonParameter(integerMichelsonParameter)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_PAIR_RIGHT, payload)
    }

    @Test
    fun testEncodeTrueToJSON() {
        val michelson = BooleanMichelsonParameter(true)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_TRUE, payload)
    }

    @Test
    fun testEncodeFalseToJSON() {
        val michelson = BooleanMichelsonParameter(false)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_FALSE, payload)
    }

    @Test
    fun testEncodeHexBytesToJSON() {
        val michelson = BytesMichelsonParameter("123456".hexStringToByteArray())
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_BYTES, payload)
    }

    @Test
    fun testEncodeSomeToJSON() {
        val michelson = SomeMichelsonParameter(integerMichelsonParameter)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_SOME, payload)
    }

    @Test
    fun testEncodeNoneToJSON() {
        val michelson = NoneMichelsonParameter()
        val payload = michelson.payload.toJson()
        Assert.assertEquals(MICHELSON_NONE, payload)
    }

    @Test
    fun testAnnotation() {
        val annotations = listOf(MichelsonAnnotation("@kotlintezos"))
        val expected =
            "{\"args\":[$MICHELSON_INT,$MICHELSON_STRING],\"prim\":\"Pair\",\"annots\":[\"${annotations[0].value}\"]}"

        val michelson = PairMichelsonParameter(integerMichelsonParameter, stringMichelsonParameter, annotations)
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    /**
     * DEXter test
     */
    @Test
    fun testTransferTokens() {
        val address1 = "tz1XarY7qEahQBipuuNZ4vPw9MN6Ldyxv8G3"
        val address2 = "tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW"
        val michelson = LeftMichelsonParameter(
            PairMichelsonParameter(
                StringMichelsonParameter(address1),
                PairMichelsonParameter(
                    StringMichelsonParameter(address2),
                    IntegerMichelsonParameter(10)
                )
            )
        )
        val expected =
            "{\"args\":[{\"args\":[{\"string\":\"$address1\"},{\"args\":[{\"string\":\"$address2\"},{\"int\":\"10\"}],\"prim\":\"Pair\"}],\"prim\":\"Pair\"}],\"prim\":\"Left\"}"
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    @Test
    fun testAddLiquidity() {
        val michelson = LeftMichelsonParameter(
            LeftMichelsonParameter(
                PairMichelsonParameter(
                    IntegerMichelsonParameter(1),
                    PairMichelsonParameter(
                        IntegerMichelsonParameter(100),
                        StringMichelsonParameter("2020-06-29T18:00:21Z")
                    )
                )
            )
        )
        val expected =
            "{\"args\":[{\"args\":[{\"args\":[{\"int\":\"1\"},{\"args\":[{\"int\":\"100\"},{\"string\":\"2020-06-29T18:00:21Z\"}],\"prim\":\"Pair\"}],\"prim\":\"Pair\"}],\"prim\":\"Left\"}],\"prim\":\"Left\"}"
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    @Test
    fun testBuyTokens() {
        val michelson = RightMichelsonParameter(
            LeftMichelsonParameter(
                PairMichelsonParameter(
                    IntegerMichelsonParameter(1),
                    StringMichelsonParameter("2020-06-29T18:00:21Z")
                )
            )
        )
        val expected =
            "{\"args\":[{\"args\":[{\"args\":[{\"int\":\"1\"},{\"string\":\"2020-06-29T18:00:21Z\"}],\"prim\":\"Pair\"}],\"prim\":\"Left\"}],\"prim\":\"Right\"}"
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    @Test
    fun testBuyTez() {
        val michelson = RightMichelsonParameter(
            RightMichelsonParameter(
                LeftMichelsonParameter(
                    PairMichelsonParameter(
                        IntegerMichelsonParameter(10),
                        PairMichelsonParameter(
                            IntegerMichelsonParameter(1),
                            StringMichelsonParameter("2020-06-29T18:00:21Z")
                        )
                    )
                )
            )
        )
        val expected =
            "{\"args\":[{\"args\":[{\"args\":[{\"args\":[{\"int\":\"10\"},{\"args\":[{\"int\":\"1\"},{\"string\":\"2020-06-29T18:00:21Z\"}],\"prim\":\"Pair\"}],\"prim\":\"Pair\"}],\"prim\":\"Left\"}],\"prim\":\"Right\"}],\"prim\":\"Right\"}"
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    @Test
    fun testRemoveLiquidity() {
        val michelson = LeftMichelsonParameter(
            RightMichelsonParameter(
                PairMichelsonParameter(
                    PairMichelsonParameter(
                        IntegerMichelsonParameter(100),
                        IntegerMichelsonParameter(1)
                    ),
                    PairMichelsonParameter(
                        IntegerMichelsonParameter(1),
                        StringMichelsonParameter("2020-06-29T18:00:21Z")
                    )
                )
            )
        )
        val expected = "{\"args\":[{\"args\":[{\"args\":[{\"args\":[{\"int\":\"100\"},{\"int\":\"1\"}],\"prim\":\"Pair\"},{\"args\":[{\"int\":\"1\"},{\"string\":\"2020-06-29T18:00:21Z\"}],\"prim\":\"Pair\"}],\"prim\":\"Pair\"}],\"prim\":\"Right\"}],\"prim\":\"Left\"}"
        val payload = michelson.payload.toJson()
        Assert.assertEquals(expected, payload)
    }

    companion object {
        val stringMichelsonParameter = StringMichelsonParameter("KotlinTezos SDK")
        val integerMichelsonParameter = IntegerMichelsonParameter(42)

        const val MICHELSON_UNIT = "{\"prim\":\"Unit\"}"
        const val MICHELSON_STRING = "{\"string\":\"KotlinTezos SDK\"}"
        const val MICHELSON_INT = "{\"int\":\"42\"}"
        const val MICHELSON_PAIR = "{\"args\":[$MICHELSON_STRING,$MICHELSON_INT],\"prim\":\"Pair\"}"
        const val MICHELSON_PAIR_LEFT = "{\"args\":[$MICHELSON_STRING],\"prim\":\"Left\"}"
        const val MICHELSON_PAIR_RIGHT = "{\"args\":[$MICHELSON_INT],\"prim\":\"Right\"}"
        const val MICHELSON_TRUE = "{\"prim\":\"True\"}"
        const val MICHELSON_FALSE = "{\"prim\":\"False\"}"
        const val MICHELSON_BYTES = "{\"bytes\":\"123456\"}"
        const val MICHELSON_SOME = "{\"args\":[$MICHELSON_INT],\"prim\":\"Some\"}"
        const val MICHELSON_NONE = "{\"prim\":\"None\"}"

    }
}
