/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.data.parser.StringMapParser
import org.junit.Assert
import org.junit.Test

class StringMapParserTest {

    @Test
    fun test_Storage() {
        val json =
            "{\"prim\":\"Pair\",\"args\":[{\"int\":\"410\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"False\"},{\"int\":\"0\"}]},{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1RY96NB6CWNDxy1sfqB2hXUwoCYxdw48Mm\"},{\"string\":\"KT19vLuzhGpe5F64jkxgzqrfRkSVprL4cPa5\"}]},{\"int\":\"0\"},{\"int\":\"0\"}]}"
        val result = StringMapParser().parse(json)

        Assert.assertTrue(result!!.size == 2)
        Assert.assertTrue(result["args"] is ArrayList<*>)
        Assert.assertTrue(result["prim"] == "Pair")
    }

    @Test
    fun test_Simple() {
        val json = "{\"int\":410}"
        val result = StringMapParser().parse(json)
        Assert.assertTrue(result["int"] is Long)
    }
}
