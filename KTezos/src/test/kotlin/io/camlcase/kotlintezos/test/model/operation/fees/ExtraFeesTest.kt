/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.model.operation.fees

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.*
import org.junit.Assert
import org.junit.Test

class ExtraFeesTest {
    @Test
    fun `plus Same lists`() {
        val summand1 = ExtraFees()
        summand1.add(OriginationFee(Tez(1.0)))
        summand1.add(AllocationFee(Tez(1.0)))
        val summand2 = ExtraFees()
        summand2.add(OriginationFee(Tez(2.0)))
        summand2.add(AllocationFee(Tez(2.0)))

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 2)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez(3.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == Tez(3.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == null)
    }

    @Test
    fun `plus different lists - same size`() {
        val summand1 = ExtraFees()
        summand1.add(OriginationFee(Tez(1.0)))
        summand1.add(BurnFee(Tez(1.0)))
        val summand2 = ExtraFees()
        summand2.add(OriginationFee(Tez(2.0)))
        summand2.add(AllocationFee(Tez(2.0)))

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 3)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez(2.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == Tez(3.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == Tez(1.0))
    }

    @Test
    fun `plus different lists - different size`() {
        val summand1 = ExtraFees()
        summand1.add(OriginationFee(Tez(1.0)))
        summand1.add(AllocationFee(Tez(1.0)))
        val summand2 = ExtraFees()
        summand2.add(OriginationFee(Tez(2.0)))
        summand2.add(AllocationFee(Tez(2.0)))
        summand2.add(BurnFee(Tez(1.0)))

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 3)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == Tez(3.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez(3.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == Tez(1.0))
    }

    @Test
    fun `plus different lists - different size 2`() {
        val summand1 = ExtraFees()
        summand1.add(OriginationFee(Tez(1.0)))
        summand1.add(AllocationFee(Tez(1.0)))
        summand1.add(BurnFee(Tez(1.0)))
        val summand2 = ExtraFees()
        summand2.add(BurnFee(Tez(2.0)))

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 3)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == Tez(1.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez(1.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == Tez(3.0))
    }

    @Test
    fun `plus empty + filled`() {
        val summand1 = ExtraFees()
        summand1.add(OriginationFee(Tez(1.0)))
        summand1.add(AllocationFee(Tez(1.0)))
        summand1.add(BurnFee(Tez(1.0)))
        val summand2 = ExtraFees()

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 3)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == Tez(1.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez(1.0))
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == Tez(1.0))
    }


    @Test
    fun `plus empty + filled 2`() {
        val summand1 = ExtraFees()
        val summand2 = ExtraFees()
        summand2.add(BurnFee(Tez(1.0)))

        val sum = summand1 + summand2
        val list = sum.asList
        Assert.assertTrue(list.size == 1)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ORIGINATION_FEE) == null)
        Assert.assertTrue(sum.getFee(ExtraFeeType.ALLOCATION_FEE) == null)
        Assert.assertTrue(sum.getFee(ExtraFeeType.BURN_FEE) == Tez(1.0))
    }
}
