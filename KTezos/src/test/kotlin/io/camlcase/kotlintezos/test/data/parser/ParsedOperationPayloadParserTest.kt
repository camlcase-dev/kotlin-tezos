/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.data.parser.ParsedOperationPayloadParser
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test

class ParsedOperationPayloadParserTest {
    @Test
    fun parse_Origination_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_origination_result.json")
        val result = ParsedOperationPayloadParser().parse(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 1)
        Assert.assertTrue(contents[0]["kind"] == OperationType.ORIGINATION.name.toLowerCase())
    }

    @Test
    fun parse_Transaction_SEND_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_send_result.json")
        val result = ParsedOperationPayloadParser().parse(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 1)
        Assert.assertTrue(contents[0]["kind"] == OperationType.TRANSACTION.name.toLowerCase())
    }

    @Test
    fun parse_Reveal_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_reveal_result.json")
        val result = ParsedOperationPayloadParser().parse(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 1)
        Assert.assertTrue(contents[0]["kind"] == OperationType.REVEAL.name.toLowerCase())
    }

    @Test
    fun parse_Delegate_Unrevealed_Response() {
        val input =
            "[{\"branch\":\"BMFxxeUTU1THNiCPh7rSpFvCGLMCna1Gz1zrJXxcoKCBy9XyTLR\",\"contents\":[{\"kind\":\"reveal\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1268\",\"counter\":\"553025\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktmE2AYPGiyM5pWpBzu5DTPx6rEBC1Foq3txYdxgNZGxdLs9oW8\"},{\"kind\":\"delegation\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1257\",\"counter\":\"553026\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"delegate\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationPayloadParser().parse(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 2)
        Assert.assertTrue(contents[0]["kind"] == OperationType.REVEAL.name.toLowerCase())
        Assert.assertTrue(contents[1]["kind"] == OperationType.DELEGATION.name.toLowerCase())
    }

    @Test
    fun parse_Invalid_Response() {
        // Reveal +
        // Delegation without delegate
        val input =
            "[{\"branch\":\"BMFxxeUTU1THNiCPh7rSpFvCGLMCna1Gz1zrJXxcoKCBy9XyTLR\",\"contents\":[{\"kind\":\"reveal\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1268\",\"counter\":\"553025\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktmE2AYPGiyM5pWpBzu5DTPx6rEBC1Foq3txYdxgNZGxdLs9oW8\"},{\"kind\":\"delegation\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1257\",\"counter\":\"553026\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\"}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationPayloadParser().parse(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 2)
        Assert.assertTrue(contents[0]["kind"] == OperationType.REVEAL.name.toLowerCase())
        Assert.assertTrue(contents[1]["kind"] == OperationType.DELEGATION.name.toLowerCase())
    }

    @Test
    fun parse_Transaction_SmartContract_Response() {
        val input =
            "[{\"branch\":\"BLjCqov6Sko6nMaKpbGn1ukub9JeKhyX837zZ3xPTihDeLFEsBC\",\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1RGPPkKmW1SsGVfKdEmTQU94xciCyQPqhB\",\"fee\":\"0\",\"counter\":\"832541\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"0\",\"destination\":\"KT1NisAhyEtp2pKMh5ynVPhPBfwoGFHbMe6d\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1RGPPkKmW1SsGVfKdEmTQU94xciCyQPqhB\"},{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}]},{\"int\":\"2\"}]}}}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationPayloadParser().parse(input)
        println(input)

        Assert.assertNotNull(result)
        val contents = result!!["contents"] as ArrayList<Map<String, Any>>
        Assert.assertTrue(contents.size == 1)
        Assert.assertTrue(contents[0]["kind"] == OperationType.TRANSACTION.name.toLowerCase())
        Assert.assertTrue(contents[0]["parameters"] != null)
    }
}
