/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.model.tzkt

import io.camlcase.kotlintezos.model.tzkt.dto.deserializeSmartContractCall
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import org.junit.Assert
import org.junit.Test

class TzKtOperationResponseTest {
    @Test
    fun `deserializeSmartContractCall xtzToToken OK`() {
        val json =
            "{\"entrypoint\":\"xtzToToken\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"15308\"},{\"string\":\"2020-10-28T00:40:16Z\"}]}]}}"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == "xtzToToken")
        Assert.assertTrue(result.parameters is PairMichelsonParameter)
    }
    
    @Test
    fun `deserializeSmartContractCall Token transfer OK`() {
        val json = 
            "{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"},{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},{\"int\":\"500000\"}]}]}}"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == "transfer")
        Assert.assertTrue(result.parameters is PairMichelsonParameter)
    }

    @Test
    fun `deserializeSmartContractCall entrypoint KO`() {
        val json =
            "\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"},{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},{\"int\":\"500000\"}]}]}}\""
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNull(result)
    }

    @Test
    fun `deserializeSmartContractCall value KO`() {
        val json =
            "\"parameters\":\"{\"entrypoint\":\"transfer\",\"value\":{}\""
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == json)
        Assert.assertTrue(result.parameters == null)
    }

    @Test
    fun `deserializeSmartContractCall value KO 2`() {
        val json =
            "\"parameters\":\"{\"entrypoint\":\"transfer\",\"value12\":{}\""
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == json)
        Assert.assertTrue(result.parameters == null)
    }

    @Test
    fun `deserializeSmartContractCall Invalid Json`() {
        val json = "{}"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNull(result)
    }

    @Test
    fun `deserializeSmartContractCall Invalid Json 2`() {
        val json = "Someentrypoint"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == json)
        Assert.assertTrue(result.parameters == null)
    }

    @Test
    fun `deserializeSmartContractCall Null Json`() {
        val json = null
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNull(result)
    }

    @Test
    fun `deserializeSmartContractCall value KO 3`() {
        val json =
            "{\"entrypoint\":\"xtzToToken\",\"value123\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"15308\"},{\"string\":\"2020-10-28T00:40:16Z\"}]}]}}"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == "xtzToToken")
        Assert.assertTrue(result.parameters == null)
    }

    @Test
    fun `deserializeSmartContractCall No value`() {
        val json =
            "{\"entrypoint\":\"xtzToToken\"}"
        val result = deserializeSmartContractCall(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.entrypoint == "xtzToToken")
        Assert.assertTrue(result.parameters == null)
    }
}
