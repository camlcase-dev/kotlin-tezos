/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.smartcontract

import io.camlcase.kotlintezos.smartcontract.convertToMicheline
import org.junit.Assert
import org.junit.Test

class MichelineParameterTest {

    @Test
    fun test_ConvertToMicheline_Object_KO() {
        val json = "{\"status\":\"skipped\"}"
        val result = json.convertToMicheline()
        Assert.assertTrue(result == null)
    }

    @Test
    fun test_ConvertToMicheline_Primitive_KO() {
        val json = "kotlintezos"
        val result = json.convertToMicheline()
        Assert.assertTrue(result == null)
    }

    @Test
    fun test_ConvertToMicheline_EmptyList() {
        val json = "[]"
        val result = json.convertToMicheline()
        Assert.assertTrue(result!!.isEmpty())
    }

    @Test
    fun test_ConvertToMicheline_OK() {
        val json = "[{\"status\":\"skipped\"}]"
        val result = json.convertToMicheline()
        Assert.assertTrue(result!!.isNotEmpty())
        Assert.assertTrue((result[0] as Map<String, Any>)["status"] == "skipped")
    }

    @Test
    fun test_ConvertToMicheline_TokenBalance() {
        val json =
            "[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]},{\"prim\":\"NONE\",\"args\":[{\"prim\":\"key_hash\"}]},{\"prim\":\"CREATE_CONTRACT\",\"args\":[[{\"prim\":\"parameter\",\"args\":[{\"prim\":\"nat\"}]},{\"prim\":\"storage\",\"args\":[{\"prim\":\"unit\"}]},{\"prim\":\"code\",\"args\":[[{\"prim\":\"FAILWITH\"}]]}]]},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"LAMBDA\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"address\"},{\"prim\":\"unit\"}]},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"unit\"}]},[{\"prim\":\"CAR\"},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"nat\"}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"a\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"PAIR\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"address\"},{\"prim\":\"contract\",\"args\":[{\"prim\":\"nat\"}]}]}],\"annots\":[\"%%getBalance\"]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"b\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]}]]},{\"prim\":\"TRANSFER_TOKENS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"NIL\",\"args\":[{\"prim\":\"operation\"}]}]]},{\"prim\":\"CONS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"UNIT\"}]]},{\"prim\":\"PAIR\"}]]}]]},{\"prim\":\"APPLY\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"lambda\",\"args\":[{\"prim\":\"unit\"},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"unit\"}]}]}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"c\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]}]]},{\"prim\":\"TRANSFER_TOKENS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"NIL\",\"args\":[{\"prim\":\"operation\"}]}]]},{\"prim\":\"CONS\"}]]},{\"prim\":\"CONS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"UNIT\"}]]},{\"prim\":\"PAIR\"}]"

        val result = json.convertToMicheline()
        Assert.assertTrue(result!!.size == 7)
        Assert.assertTrue((result[0] as Map<String, Any?>).size == 2)
    }
}
