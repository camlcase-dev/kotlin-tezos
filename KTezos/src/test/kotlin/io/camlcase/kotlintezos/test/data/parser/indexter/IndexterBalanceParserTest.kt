/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.indexter

import io.camlcase.kotlintezos.data.parser.indexter.IndexterBalanceParser
import io.camlcase.kotlintezos.model.TezosError
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class IndexterBalanceParserTest {

    @Test
    fun test_200_Error() {
        val input =
            "{\"status_code\":200,\"uuid\":\"3b8ccecf-f406-4b5b-a0f0-d5b1c1f77424\",\"error\":\"failed to parse supply from response\"}"

        try {
            val result = IndexterBalanceParser().parse(input)
        } catch (e: TezosError) {
            Assert.assertTrue(e.rpcErrors!![0].cause == "failed to parse supply from response (3b8ccecf-f406-4b5b-a0f0-d5b1c1f77424)")
            Assert.assertTrue(e.code == 200)
        }
    }

    @Test
    fun test_OK() {
        val input = "38475646525242413163748595959838373"
        val result = IndexterBalanceParser().parse(input)
        Assert.assertTrue(result == BigInteger(input))
    }
}
