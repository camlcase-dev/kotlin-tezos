/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.core.iterate
import io.camlcase.kotlintezos.data.parser.SimulationResponseParser
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFeeType
import io.camlcase.kotlintezos.test.network.MockDispatcher
import io.camlcase.kotlintezos.test.util.Params.Companion.testNetworkConstants
import org.junit.Assert
import org.junit.Test


class SimulationResponseParserTest {
    /**
     * A transaction which only consumes gas.
     */
    @Test
    fun testSuccessfulTransaction() {
        val input = MockDispatcher.getStringFromFile("run_operation_simulation.json")
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result?.simulations!!.size)
        Assert.assertEquals(OperationType.TRANSACTION, result!!.simulations[0].type)
        Assert.assertEquals(result?.simulations[0].consumedGas, 10207)
        Assert.assertEquals(result?.simulations[0].consumedStorage, 0)
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    @Test
    fun testSuccessfulContractInvocation() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1\",\"counter\":\"31127\",\"gas_limit\":\"100000\",\"storage_limit\":\"10000\",\"amount\":\"0\",\"destination\":\"KT1XsHrcWTmRFGyPgtzEHb4fb9qDAj5oQxwB\",\"parameters\":{\"string\":\"TezosKit\"},\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":284,\"change\":\"1\"}],\"operation_result\":{\"status\":\"applied\",\"storage\":{\"string\":\"TezosKit\"},\"consumed_gas\":\"11780\",\"storage_size\":\"49\"}}}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result?.simulations!!.size)
        Assert.assertEquals(OperationType.TRANSACTION, result!!.simulations[0].type)
        Assert.assertEquals(result?.simulations[0].consumedGas, 11_780)
        Assert.assertEquals(result?.simulations[0].consumedStorage, 0)
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    /**
     * Failed transaction - attempted to send too many Tez.
     */
    @Test
    fun testFailureOperationParameters() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1\",\"counter\":\"31127\",\"gas_limit\":\"100000\",\"storage_limit\":\"10000\",\"amount\":\"10000000000000000\",\"destination\":\"KT1D5jmrBD7bDa3jCpgzo32FMYmRDdK2ihka\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":284,\"change\":\"1\"}],\"operation_result\":{\"status\":\"failed\",\"errors\":[{\"kind\":\"temporary\",\"id\":\"proto.004-Pt24m4xi.contract.balance_too_low\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"balance\":\"400570851\",\"amount\":\"10000000000000000\"}]}}}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(0, result!!.simulations[0].consumedGas)
        Assert.assertTrue(result.simulations.size == 1)
        Assert.assertTrue(result.errors!!.size == 1)
        Assert.assertTrue(result.errors!![0].cause == "proto.004-Pt24m4xi.contract.balance_too_low")
    }

    /**
     * Failed transaction - too low of gas limit
     */
    @Test
    fun testFailureExhaustedGas() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1\",\"counter\":\"31127\",\"gas_limit\":\"0\",\"storage_limit\":\"10000\",\"amount\":\"10000000000000000\",\"destination\":\"KT1D5jmrBD7bDa3jCpgzo32FMYmRDdK2ihka\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":284,\"change\":\"1\"}],\"operation_result\":{\"status\":\"failed\",\"errors\":[{\"kind\":\"temporary\",\"id\":\"proto.004-Pt24m4xi.gas_exhausted.operation\"}]}}}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertNotNull(result)
        Assert.assertEquals(0, result!!.simulations[0].consumedGas)
        Assert.assertTrue(result.simulations.size == 1)
        Assert.assertTrue(result.errors!!.size == 1)
        Assert.assertTrue(result.errors!![0].cause == "proto.004-Pt24m4xi.gas_exhausted.operation")
    }

    @Test
    fun testBatchTransaction() {
        val input =
            "{  \"contents\": [{    \"counter\": \"776970\",    \"fee\": \"1268\",    \"gas_limit\": \"10000\",    \"kind\": \"reveal\",    \"metadata\": {      \"balance_updates\": [{        \"change\": \"-1268\",        \"contract\": \"tz1WwEvjKxdz1EFa6a7HYP14SwZSPGfFnPuc\",        \"kind\": \"contract\"      }, {        \"category\": \"fees\",        \"change\": \"1268\",        \"cycle\": 290,        \"delegate\": \"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",        \"kind\": \"freezer\"      }],      \"operation_result\": {        \"consumed_gas\": \"10000\",        \"status\": \"applied\"}    },    \"public_key\": \"edpkuG12SJVcmdNxWfXKPb24mNXSxFX4jsDPYPG7r5AwqdG5G7aACZ\",    \"source\": \"tz1WwEvjKxdz1EFa6a7HYP14SwZSPGfFnPuc\",\"storage_limit\": \"0\"}, {    \"counter\": \"776971\",    \"delegate\": \"tz1WwEvjKxdz1EFa6a7HYP14SwZSPGfFnPuc\",\"fee\": \"0\", \"gas_limit\": \"800000\", \"kind\": \"delegation\",\"metadata\": {\"balance_updates\": [],\"operation_result\": {\"consumed_gas\": \"10000\",\"status\": \"applied\"}},\"source\": \"tz1WwEvjKxdz1EFa6a7HYP14SwZSPGfFnPuc\",\"storage_limit\": \"60000\"}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(10_000, result!!.simulations[0].consumedGas)
        Assert.assertEquals(10_000, result!!.simulations[1].consumedGas)
        Assert.assertEquals(0, result!!.simulations[0].consumedStorage)
        Assert.assertEquals(0, result!!.simulations[1].consumedStorage)
        Assert.assertEquals(result!!.simulations[0].type, OperationType.REVEAL)
        Assert.assertEquals(result!!.simulations[1].type, OperationType.DELEGATION)
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    @Test
    fun test_InternalOperations() {
        val input =
            "{ \"contents\": [ { \"kind\": \"transaction\", \"source\": \"tz1XarY7qEahQBipuuNZ4vPw9MN6Ldyxv8G3\", \"fee\": \"0\", \"counter\": \"30631\", \"gas_limit\": \"800000\", \"storage_limit\": \"60000\", \"amount\": \"10000000\", \"destination\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"parameters\": { \"prim\": \"Right\", \"args\": [ { \"prim\": \"Left\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"int\": \"1\" }, { \"string\": \"2020-06-29T18:00:21Z\" } ] } ] } ] }, \"metadata\": { \"balance_updates\": [], \"operation_result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Pair\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"bytes\": \"019177bfab48c1e9991fca5f7ca6ebe99a1aa5cf5700\" }, { \"bytes\": \"018f090483da68845c926fff0e360a1154266c994b00\" } ] }, { \"prim\": \"Pair\", \"args\": [ { \"int\": \"19352385\" }, [ { \"prim\": \"Elt\", \"args\": [ { \"bytes\": \"00008307c8ce77c8f7ab711c1d3bb3570e5fbe11f5dc\" }, { \"prim\": \"Right\", \"args\": [ { \"prim\": \"Left\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"int\": \"1\" }, { \"int\": \"1593453621\" } ] } ] } ] } ] } ] ] } ] } ] }, \"big_map_diff\": [], \"balance_updates\": [ { \"kind\": \"contract\", \"contract\": \"tz1XarY7qEahQBipuuNZ4vPw9MN6Ldyxv8G3\", \"change\": \"-10000000\" }, { \"kind\": \"contract\", \"contract\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"change\": \"10000000\" } ], \"consumed_gas\": \"185075\", \"storage_size\": \"7166\" }, \"internal_operation_results\": [ { \"kind\": \"transaction\", \"source\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"nonce\": 0, \"amount\": \"10000000\", \"destination\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"parameters\": { \"prim\": \"Left\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"bytes\": \"01bd7bcfca7caa3469dfa1a0bf4863dc8de759de6b00\" }, { \"bytes\": \"018f090483da68845c926fff0e360a1154266c994b00\" } ] } ] }, \"result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Unit\" } ] }, \"big_map_diff\": [ { \"key_hash\": \"exprv3WzhTyQcC4ZmG66wfxsLYy9im9Pe8msAGxnLSdRE16QEvxHhj\", \"key\": { \"bytes\": \"018f090483da68845c926fff0e360a1154266c994b00\" }, \"value\": { \"prim\": \"Pair\", \"args\": [ { \"int\": \"10000000\" }, { \"bytes\": \"01bd7bcfca7caa3469dfa1a0bf4863dc8de759de6b00\" } ] } } ], \"balance_updates\": [ { \"kind\": \"contract\", \"contract\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"change\": \"-10000000\" }, { \"kind\": \"contract\", \"contract\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"change\": \"10000000\" } ], \"consumed_gas\": \"145880\", \"storage_size\": \"826\" } }, { \"kind\": \"transaction\", \"source\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"nonce\": 1, \"amount\": \"0\", \"destination\": \"KT1Md4zkfCvkdqgxAC9tyRYpRUBKmD1owEi2\", \"parameters\": { \"prim\": \"Right\", \"args\": [ { \"bytes\": \"01bd7bcfca7caa3469dfa1a0bf4863dc8de759de6b00\" } ] }, \"result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Pair\", \"args\": [ { \"int\": \"100\" }, { \"prim\": \"Pair\", \"args\": [ { \"string\": \"Tezos Gold\" }, { \"string\": \"TGD\" } ] } ] } ] }, \"big_map_diff\": [], \"consumed_gas\": \"49771\", \"storage_size\": \"784\" } }, { \"kind\": \"transaction\", \"source\": \"KT1Md4zkfCvkdqgxAC9tyRYpRUBKmD1owEi2\", \"nonce\": 2, \"amount\": \"0\", \"destination\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"parameters\": { \"prim\": \"Right\", \"args\": [ { \"int\": \"12\" } ] }, \"result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Unit\" } ] }, \"big_map_diff\": [], \"consumed_gas\": \"134614\", \"storage_size\": \"826\" } }, { \"kind\": \"transaction\", \"source\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"nonce\": 3, \"amount\": \"10000000\", \"destination\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"parameters\": { \"prim\": \"Right\", \"args\": [ { \"prim\": \"Right\", \"args\": [ { \"prim\": \"Right\", \"args\": [ { \"int\": \"12\" } ] } ] } ] }, \"result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Pair\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"bytes\": \"019177bfab48c1e9991fca5f7ca6ebe99a1aa5cf5700\" }, { \"bytes\": \"018f090483da68845c926fff0e360a1154266c994b00\" } ] }, { \"prim\": \"Pair\", \"args\": [ { \"int\": \"19352385\" }, [] ] } ] } ] }, \"big_map_diff\": [], \"balance_updates\": [ { \"kind\": \"contract\", \"contract\": \"KT1MqvzsEPoZnbacH18uztqvQdG8x8nKAgFi\", \"change\": \"-10000000\" }, { \"kind\": \"contract\", \"contract\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"change\": \"10000000\" } ], \"consumed_gas\": \"187633\", \"storage_size\": \"7123\" } }, { \"kind\": \"transaction\", \"source\": \"KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434\", \"nonce\": 4, \"amount\": \"0\", \"destination\": \"KT1Md4zkfCvkdqgxAC9tyRYpRUBKmD1owEi2\", \"parameters\": { \"prim\": \"Left\", \"args\": [ { \"prim\": \"Pair\", \"args\": [ { \"bytes\": \"01bd7bcfca7caa3469dfa1a0bf4863dc8de759de6b00\" }, { \"prim\": \"Pair\", \"args\": [ { \"bytes\": \"00008307c8ce77c8f7ab711c1d3bb3570e5fbe11f5dc\" }, { \"int\": \"1\" } ] } ] } ] }, \"result\": { \"status\": \"applied\", \"storage\": { \"prim\": \"Pair\", \"args\": [ [], { \"prim\": \"Pair\", \"args\": [ { \"int\": \"100\" }, { \"prim\": \"Pair\", \"args\": [ { \"string\": \"Tezos Gold\" }, { \"string\": \"TGD\" } ] } ] } ] }, \"big_map_diff\": [ { \"key_hash\": \"exprtoyixHUj8qCQE23RH3AG7ScoFMeafLGxc93M8XZZ4ckJYr2JpQ\", \"key\": { \"bytes\": \"00008307c8ce77c8f7ab711c1d3bb3570e5fbe11f5dc\" }, \"value\": { \"prim\": \"Pair\", \"args\": [ { \"int\": \"58\" }, [] ] } }, { \"key_hash\": \"exprv9SUvRRcsRJRGD8tHmKVbPNxWQ71hK6LNRCXVjkTw8pd8GRkUB\", \"key\": { \"bytes\": \"01bd7bcfca7caa3469dfa1a0bf4863dc8de759de6b00\" }, \"value\": { \"prim\": \"Pair\", \"args\": [ { \"int\": \"11\" }, [] ] } } ], \"consumed_gas\": \"30827\", \"storage_size\": \"784\" } } ] } } ] }"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(OperationType.TRANSACTION, result!!.simulations[0].type)
        Assert.assertEquals(733_800, result!!.simulations[0].consumedGas)
        Assert.assertEquals(0, result!!.simulations[0].consumedStorage)
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    @Test
    fun testWrongContentsJSON() {
        val input =
            "{\"contents\":{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1\",\"counter\":\"31127\",\"gas_limit\":\"100000\",\"storage_limit\":\"10000\",\"amount\":\"0\",\"destination\":\"KT1XsHrcWTmRFGyPgtzEHb4fb9qDAj5oQxwB\",\"parameters\":{\"string\":\"TezosKit\"},\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":284,\"change\":\"1\"}],\"operation_result\":{\"status\":\"applied\",\"storage\":{\"string\":\"TezosKit\"},\"consumed_gas\":\"11780\",\"storage_size\":\"49\"}}}}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNull(result)
    }

    @Test(expected = TezosError::class)
    fun testGeneralError() {
        val input =
            "[{\"kind\":\"branch\",\"id\":\"proto.005-PsBabyM1.contract.unrevealed_key\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}]"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertNotNull(result)
    }

    @Test
    fun test_SpendingContract_LimitError() {
        val input = MockDispatcher.getStringFromFile("run_operation_spending_limit_error.json")
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertNotNull(result)
        val errors = result!!.errors
        Assert.assertNotNull(errors)
        Assert.assertTrue(errors!![0].cause == "proto.005-PsBabyM1.michelson_v1.runtime_error")
        Assert.assertTrue(errors!![1].cause == "proto.005-PsBabyM1.michelson_v1.script_rejected")
    }

    @Test(expected = TezosError::class)
    fun test_Counter_Surpassed() {
        val input =
            "[{\"kind\":\"temporary\",\"id\":\"failure\",\"msg\":\"Error while applying operation oopQDQ9HumjwcPmtQMU75u5NiL13bE5aCFpxK85TfH8gdLDVZES:\\nbranch refused (Error:\\n                  Counter 561557 already used for contract tz1iuj52evxucSGhhyUq1S5bPN3uax1E9BNQ (expected 561558)\\n)\"}]"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertNotNull(result)
    }

    @Test
    fun test_AllocationFees() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"fee\":\"0\",\"counter\":\"553161\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"10000000\",\"destination\":\"tz1XkkUJrLaF4sc3HWe98SeEPMPbB1yvy3sG\",\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"change\":\"-10000000\"},{\"kind\":\"contract\",\"contract\":\"tz1XkkUJrLaF4sc3HWe98SeEPMPbB1yvy3sG\",\"change\":\"10000000\"},{\"kind\":\"contract\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"change\":\"-257000\"}],\"consumed_gas\":\"10207\",\"allocated_destination_contract\":true}}}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertNotNull(result)
        Assert.assertEquals(OperationType.TRANSACTION, result!!.simulations[0].type)
        val extraFees = result!!.simulations[0].extraFees
        Assert.assertNotNull(extraFees)
        Assert.assertTrue(extraFees.asList.size == 1)
        val extraFee = extraFees.asList[0]
        Assert.assertTrue(extraFee is AllocationFee)
        Assert.assertNotNull(extraFee.fee == Tez("257000"))
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    @Test
    fun testRun_SmartContract_MultipleOps_Transaction() {
        val input = MockDispatcher.getStringFromFile("run_operation_smartcontract_result.json")
        val result = SimulationResponseParser(testNetworkConstants).parse(input)

        Assert.assertNotNull(result)

        Assert.assertEquals(2, result!!.simulations.size)
        val simulatedFees1 = result.simulations[0]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees1.type)
        Assert.assertEquals(58_969, simulatedFees1.consumedGas)
        Assert.assertEquals(0, simulatedFees1.consumedStorage)
        val simulatedFees2 = result.simulations[1]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees2.type)
        Assert.assertEquals(542_003, simulatedFees2.consumedGas)
        Assert.assertEquals(0, simulatedFees2.consumedStorage)

        Assert.assertEquals(
            600_972,
            simulatedFees1.consumedGas + simulatedFees2.consumedGas
        )
        Assert.assertEquals(
            0,
            simulatedFees1.consumedStorage + simulatedFees2.consumedStorage
        )
        Assert.assertTrue(result.errors.isNullOrEmpty())
    }

    @Test
    fun `Test simulate operation for FA12 Token balance`() {
        val input = MockDispatcher.getStringFromFile("token_balance_run_operation_simulation.json")
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        Assert.assertTrue(result!!.errors!!.size == 2)
        val error = result.errors!![1].contractArgs
        val balance = error!!.iterate("int")
        Assert.assertTrue(balance == "201")
    }

    /**
     * Two operations with different burn fees
     */
    @Test
    fun `parse BurnFee with different gas costs`() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\",\"fee\":\"0\",\"counter\":\"12049580\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"0\",\"destination\":\"KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\"},{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1YX7FF2bJHKzU8Bd5RAYWymsniTdpeG3Ab\"},{\"int\":\"123183\"}]}]}},\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"storage\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"00000cc84c2f6520dab375e8636d606671f3f5575abe\"},{\"int\":\"254\"}]},{\"int\":\"255\"}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"01e0a1035a390e9104b62439250043207bb4d4045000\"},{\"int\":\"256\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"257\"},{\"bytes\":\"0000521fc029738499f7961a65008b98e7532dbe84be\"}]}]},{\"prim\":\"False\"},{\"int\":\"688865859800\"}],\"big_map_diff\":[{\"action\":\"update\",\"big_map\":\"257\",\"key_hash\":\"expruZq4NzrFdtzKEk2n7casnJ19CdRdK6cvDsQwSojvnFongSvDmg\",\"key\":{\"bytes\":\"000068bcd737ddef7d392b6e1e7418dba55b5d4c982a\"},\"value\":{\"int\":\"0\"}},{\"action\":\"update\",\"big_map\":\"257\",\"key_hash\":\"exprvPcbSxjnRjZpKfZhaTn3ngMYi6VZzQX1hSer2o5PPHUB1vWNNL\",\"key\":{\"bytes\":\"00008d4a865ab667f700f8263925cce1943b9077efd2\"},\"value\":{\"int\":\"123183\"}}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\",\"change\":\"-16750\"}],\"consumed_gas\":\"49560\",\"consumed_milligas\":\"49559860\",\"storage_size\":\"81055\",\"paid_storage_size_diff\":\"67\",\"lazy_storage_diff\":[{\"kind\":\"big_map\",\"id\":\"257\",\"diff\":{\"action\":\"update\",\"updates\":[{\"key_hash\":\"exprvPcbSxjnRjZpKfZhaTn3ngMYi6VZzQX1hSer2o5PPHUB1vWNNL\",\"key\":{\"bytes\":\"00008d4a865ab667f700f8263925cce1943b9077efd2\"},\"value\":{\"int\":\"123183\"}},{\"key_hash\":\"expruZq4NzrFdtzKEk2n7casnJ19CdRdK6cvDsQwSojvnFongSvDmg\",\"key\":{\"bytes\":\"000068bcd737ddef7d392b6e1e7418dba55b5d4c982a\"},\"value\":{\"int\":\"0\"}}]}},{\"kind\":\"big_map\",\"id\":\"256\",\"diff\":{\"action\":\"update\",\"updates\":[]}},{\"kind\":\"big_map\",\"id\":\"255\",\"diff\":{\"action\":\"update\",\"updates\":[]}},{\"kind\":\"big_map\",\"id\":\"254\",\"diff\":{\"action\":\"update\",\"updates\":[]}}]}}},{\"kind\":\"transaction\",\"source\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\",\"fee\":\"0\",\"counter\":\"12049582\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"0\",\"destination\":\"KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\"},{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1YX7FF2bJHKzU8Bd5RAYWymsniTdpeG3Ab\"},{\"int\":\"2224621936621006733\"}]}]}},\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"storage\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"01d4a05778c3b8380cffddf6ec231a46cc577ab19100\"},{\"int\":\"380\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"1500000000000000000000000\"},{\"bytes\":\"016957b767c0d09c6376afef66728edd59fca059ac00\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"381\"},{\"prim\":\"False\"}]},{\"int\":\"382\"},{\"int\":\"1372792299337306242845509\"}],\"big_map_diff\":[{\"action\":\"update\",\"big_map\":\"380\",\"key_hash\":\"expruZq4NzrFdtzKEk2n7casnJ19CdRdK6cvDsQwSojvnFongSvDmg\",\"key\":{\"bytes\":\"000068bcd737ddef7d392b6e1e7418dba55b5d4c982a\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"0\"}]}},{\"action\":\"update\",\"big_map\":\"380\",\"key_hash\":\"exprvPcbSxjnRjZpKfZhaTn3ngMYi6VZzQX1hSer2o5PPHUB1vWNNL\",\"key\":{\"bytes\":\"00008d4a865ab667f700f8263925cce1943b9077efd2\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"2224621936621006733\"}]}}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1VBqBx7wCGPfZB4NXsA2YnUbJihcCLwv6a\",\"change\":\"-18500\"}],\"consumed_gas\":\"25876\",\"consumed_milligas\":\"25875490\",\"storage_size\":\"79150\",\"paid_storage_size_diff\":\"74\",\"lazy_storage_diff\":[{\"kind\":\"big_map\",\"id\":\"382\",\"diff\":{\"action\":\"update\",\"updates\":[]}},{\"kind\":\"big_map\",\"id\":\"381\",\"diff\":{\"action\":\"update\",\"updates\":[]}},{\"kind\":\"big_map\",\"id\":\"380\",\"diff\":{\"action\":\"update\",\"updates\":[{\"key_hash\":\"exprvPcbSxjnRjZpKfZhaTn3ngMYi6VZzQX1hSer2o5PPHUB1vWNNL\",\"key\":{\"bytes\":\"00008d4a865ab667f700f8263925cce1943b9077efd2\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"2224621936621006733\"}]}},{\"key_hash\":\"expruZq4NzrFdtzKEk2n7casnJ19CdRdK6cvDsQwSojvnFongSvDmg\",\"key\":{\"bytes\":\"000068bcd737ddef7d392b6e1e7418dba55b5d4c982a\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"0\"}]}}]}}]}}}]}"
        val result = SimulationResponseParser(testNetworkConstants).parse(input)
        val simulatedFees1 = result!!.simulations[0]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees1.type)
        Assert.assertEquals(Tez("16750"), simulatedFees1.extraFees.getFee(ExtraFeeType.BURN_FEE))
        val simulatedFees2 = result!!.simulations[1]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees2.type)
        Assert.assertEquals(Tez("18500"), simulatedFees2.extraFees.getFee(ExtraFeeType.BURN_FEE))

        // Different gas cost
        val result2 = SimulationResponseParser(
            NetworkConstants(
                testNetworkConstants.gasLimitPerOperation,
                testNetworkConstants.storageLimitPerOperation,
                testNetworkConstants.gasLimitPerBlock,
                testNetworkConstants.originationSize,
                "100"
            )
        ).parse(input)
        val simulatedFees1_2 = result2!!.simulations[0]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees1_2.type)
        Assert.assertEquals(Tez("6700"), simulatedFees1_2.extraFees.getFee(ExtraFeeType.BURN_FEE))
        val simulatedFees2_2 = result2!!.simulations[1]
        Assert.assertEquals(OperationType.TRANSACTION, simulatedFees2_2.type)
        Assert.assertEquals(Tez("7400"), simulatedFees2_2.extraFees.getFee(ExtraFeeType.BURN_FEE))
    }
}
