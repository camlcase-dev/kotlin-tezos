/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.ext

import io.camlcase.kotlintezos.model.validTezosAddress
import org.junit.Assert
import org.junit.Test

class ValidateAddressTest {

    @Test
    fun testNullAddress() {
        val address: String? = null
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testEmptyAddress() {
        val address = ""
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testBlankAddress() {
        val address = "     "
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidAddress() {
        val address = "Lorem ipsum dolor sit amet consesquieut dolorem amen dico"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testEmojiAddress() {
        val address = "😀"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testSymbolAddress() {
        val address = "☀︎"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testBlankInvalidAddress() {
        val address = "    \n1234 "
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testShortAddress() {
        val address = "tz1_34567"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidSpaceTz1Address() {
        val address = "tz1eZAGXmXxwkXUBUxuSk5X J5UZ5Q25Baja"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidDollarTz1Address() {
        val address = "tz1\$ZAGXmXxwkXUBUxuSk5XsJ5UZ5Q25Baja"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidSizeTz1Address() {
        val address = "tz1eZAGXmXxwkXUBUxuSk5XsJ5UZ5Q25Baja1" // length 37
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidLineBreakTz1Address() {
        val address = "tz1eZAGXmXxwk\nUBUxuSk5XsJ5UZ5Q25Baja1"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testInvalidSymbolkTz1Address() {
        val address = "tz1eZAGXmXxwkTUBUxuSk5XsJ☾UZ5Q25Baja1"
        val result = address.validTezosAddress()
        Assert.assertFalse(result)
    }

    @Test
    fun testValidTz1Address() {
        val address = "tz1eZAGXmXxwkXUBUxuSk5XkJ5UZ5Q25Baja"
        val result = address.validTezosAddress()
        Assert.assertTrue(result)
    }

    @Test
    fun testValidTz2Address() {
        val address = "tz2XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW"
        val result = address.validTezosAddress()
        Assert.assertTrue(result)
    }

    @Test
    fun testValidTz3Address() {
        val address = "tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5"
        val result = address.validTezosAddress()
        Assert.assertTrue(result)
    }

    @Test
    fun testValidKT1Address() {
        val address = "KT1RAHAXehUNusndqZpcxM8SfCjLi83utZsR"
        val result = address.validTezosAddress()
        Assert.assertTrue(result)
    }
}
