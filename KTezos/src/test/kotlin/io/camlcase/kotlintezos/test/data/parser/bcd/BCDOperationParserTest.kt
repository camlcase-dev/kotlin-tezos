/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.bcd

import io.camlcase.kotlintezos.data.parser.bcd.BCDOperationListParser
import io.camlcase.kotlintezos.exchange.DexterEntrypoint
import io.camlcase.kotlintezos.model.bcd.BCDTransaction
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import org.junit.Assert
import org.junit.Test

class BCDOperationParserTest {

    @Test
    fun `parse Transaction xtzToToken KO`() {
        val json =
            "[{\"id\":\"960e0f0734a245d7a5269f87d94196ab\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":false,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\",\"fee\":45252,\"counter\":2970556,\"gas_limit\":448009,\"amount\":10000000,\"destination\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"status\":\"backtracked\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.006-PsCARTHA.storage_exhausted.operation\",\"title\":\"Storage quota exceeded for the operation\",\"descr\":\"A script or one of its callee wrote more bytes than the operation said it would\",\"kind\":\"temporary\"}],\"result\":{\"consumed_gas\":328280,\"storage_size\":11835},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"2665962\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-10-06 16:01:54.194 +0000 UTC\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null},{\"id\":\"f7a2ccd1794e443f883a9e8cdd0e8c10\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":true,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"counter\":2970556,\"destination\":\"KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY\",\"status\":\"backtracked\",\"entrypoint\":\"transfer\",\"result\":{\"consumed_gas\":119629,\"storage_size\":5614,\"paid_storage_size_diff\":72},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"from\",\"value\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\"},{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"value\",\"value\":\"2672571\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null}]"
        val result = BCDOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        val bcdOperation = result!![1] as BCDTransaction
        Assert.assertTrue(bcdOperation.internal)
        Assert.assertTrue(bcdOperation.destination.alias == null)

        val bcdTx = result!![0] as BCDTransaction
        val errors = bcdTx.errors!!
        Assert.assertTrue(errors[0].description.contains("script or one of its callee wrote more bytes than the operation said "))
        Assert.assertNull(errors[0].contractMessage)
    }

    @Test
    fun `parse Transaction xtzToToken KO deadline`() {
        val json =
            "[{\"id\":\"31f3c660f750446f98d6525961b72a3f\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"oo5XsmdPjxvBAbCyL9kh3x5irUmkWNwUFfi2rfiKqJGKA6Sxjzf\",\"internal\":false,\"network\":\"mainnet\",\"timestamp\":\"2020-10-28T00:40:28Z\",\"level\":1190194,\"kind\":\"transaction\",\"source\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\",\"fee\":61961,\"counter\":7388570,\"gas_limit\":613968,\"storage_limit\":257,\"amount\":1000000,\"destination\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\",\"destination_alias\":\"Dexter tzBTC/XTZ\",\"status\":\"failed\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.006-PsCARTHA.michelson_v1.runtime_error\",\"title\":\"Script runtime error\",\"descr\":\"Toplevel error for all runtime script errors\",\"kind\":\"temporary\"},{\"id\":\"proto.006-PsCARTHA.michelson_v1.script_rejected\",\"title\":\"Script failed\",\"descr\":\"A FAILWITH instruction was reached\",\"kind\":\"temporary\",\"location\":1843,\"with\":\"\\\"NOW is greater than deadline.\\\"\"}],\"result\":{},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"15308\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-10-28 00:40:16 +0000 UTC\"}]},\"mempool\":false,\"content_index\":0,\"rawMempool\":null}]"
        val result = BCDOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)

        val bcdOperation = result!![0] as BCDTransaction
        Assert.assertTrue(!bcdOperation.internal)
        Assert.assertTrue(bcdOperation.entrypoint == DexterEntrypoint.TEZ_TO_TOKEN.entrypoint)
        Assert.assertTrue(!bcdOperation.destination.alias.isNullOrBlank())

        val errors = bcdOperation.errors!!
        Assert.assertTrue(errors[1].contractMessage!!.contains("NOW is greater than deadline."))

    }

    @Test
    fun `parse Transaction allocation`() {
        val json =
            "[{\"id\":\"960e0f0734a245d7a5269f87d94196ab\",\"allocated_destination_contract_burned\": 543211,\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":false,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\",\"fee\":45252,\"counter\":2970556,\"gas_limit\":448009,\"amount\":10000000,\"destination\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"status\":\"backtracked\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.006-PsCARTHA.storage_exhausted.operation\",\"title\":\"Storage quota exceeded for the operation\",\"descr\":\"A script or one of its callee wrote more bytes than the operation said it would\",\"kind\":\"temporary\"}],\"result\":{\"consumed_gas\":328280,\"storage_size\":11835},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"2665962\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-10-06 16:01:54.194 +0000 UTC\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null},{\"id\":\"f7a2ccd1794e443f883a9e8cdd0e8c10\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":true,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"counter\":2970556,\"destination\":\"KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY\",\"status\":\"backtracked\",\"entrypoint\":\"transfer\",\"result\":{\"consumed_gas\":119629,\"storage_size\":5614,\"paid_storage_size_diff\":72},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"from\",\"value\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\"},{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"value\",\"value\":\"2672571\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null}]"
        val result = BCDOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        val bcdOperation = result!![0] as BCDTransaction
        Assert.assertTrue(bcdOperation.fees!!.extraFees!!.asList[0] is AllocationFee)
    }
}
