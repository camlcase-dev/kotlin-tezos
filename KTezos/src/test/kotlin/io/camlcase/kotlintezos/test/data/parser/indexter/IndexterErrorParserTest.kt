/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.indexter

import io.camlcase.kotlintezos.data.parser.indexter.IndexterErrorParser
import org.junit.Assert
import org.junit.Test

class IndexterErrorParserTest {
    @Test
    fun test_Head_Error() {
        val input =
            "{\"status_code\":503,\"uuid\":\"81d919a4-7c13-47d8-bf1d-d465cb64de86\",\"error\":\"failed to get block 'head': failed to parse json: invalid character '\\u003c' looking for beginning of value\"}"
        val result = IndexterErrorParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.error == "failed to get block 'head': failed to parse json: invalid character '<' looking for beginning of value")
        Assert.assertTrue(result!!.code == 503)
        Assert.assertTrue(result!!.uuid == "81d919a4-7c13-47d8-bf1d-d465cb64de86")
    }

    @Test
    fun test_Supply() {
        val input =
            "{\"status_code\":200,\"uuid\":\"3b8ccecf-f406-4b5b-a0f0-d5b1c1f77424\",\"error\":\"failed to parse supply from response\"}"
        val result = IndexterErrorParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.error == "failed to parse supply from response")
        Assert.assertTrue(result!!.code == 200)
        Assert.assertTrue(result!!.uuid == "3b8ccecf-f406-4b5b-a0f0-d5b1c1f77424")
    }

    @Test
    fun test_Empty_Array() {
        val input = "[]"
        val result = IndexterErrorParser().parse(input)

        Assert.assertNull(result)
    }

    @Test
    fun test_Empty_Object() {
        val input = "{}"
        val result = IndexterErrorParser().parse(input)

        Assert.assertNull(result)
    }

    @Test
    fun test_No_Uuid() {
        val input = "{\"error\":\"failed to parse supply from response\"}"
        val result = IndexterErrorParser().parse(input)

        Assert.assertNull(result)
    }
}
