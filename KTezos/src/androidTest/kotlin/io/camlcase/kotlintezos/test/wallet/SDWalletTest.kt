/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.test.util.Params.Companion.MNEMONIC
import io.camlcase.kotlintezos.test.util.Wallet
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.crypto.TrustWalletFacade
import io.camlcase.kotlintezos.wallet.crypto.toHexString
import org.junit.Assert
import org.junit.Test

class SDWalletTest {
    @Test
    fun testGenerateWallet() {
        val wallet = SDWallet()
        Assert.assertNotNull(wallet)
        Assert.assertNotNull(wallet!!.mnemonic)
        Assert.assertNotNull(wallet!!.publicKey)
        Assert.assertNotNull(wallet!!.mainAddress)
    }

    @Test
    fun testGenerateWalletMnemonicNoPassphrase() {
        val wallet = SDWallet(MNEMONIC)
        Assert.assertNotNull(wallet)
        Assert.assertEquals(PUBLIC_KEY_NO_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_NO_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_NO_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }

    @Test
    fun testGenerateWalletMnemonicEmptyPassphrase() {
        val wallet = SDWallet(MNEMONIC, "")
        Assert.assertNotNull(wallet)
        // A wallet with an empty passphrase should be the same as a wallet with no passphrase.
        Assert.assertEquals(PUBLIC_KEY_NO_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_NO_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_NO_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }

    @Test
    fun testGenerateWalletMnemonicWithPassphrase() {
        val wallet = SDWallet(MNEMONIC, "TezosKitTest")
        Assert.assertNotNull(wallet)
        Assert.assertEquals(PUBLIC_KEY_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }

    @Test
    fun testGenerate_Wallet_Invalid_Entropy() {
        var wallet = SDWallet(65)
        Assert.assertNull(wallet)

        wallet = SDWallet(129)
        Assert.assertNull(wallet)

        wallet = SDWallet(288)
        Assert.assertNull(wallet)

        wallet = SDWallet(16)
        Assert.assertNull(wallet)
    }

    @Test
    fun testGenerate_Wallet_Valid_Entropy() {
        val wallet = SDWallet(192)
        Assert.assertNotNull(wallet)
        Assert.assertEquals(18, wallet!!.mnemonic.size)
        Assert.assertTrue(TrustWalletFacade().validate(wallet.mnemonic))
    }

    @Test
    fun testGenerate_Common_Mnemonic() {
        var wallet = SDWallet(Wallet.mnemonic)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet!!.secretKey.bytes.toHexString() == Wallet.SDWalletEd255519.privateKey)
        Assert.assertTrue(wallet!!.publicKey.bytes.toHexString() == Wallet.SDWalletEd255519.publicKey)
        Assert.assertTrue(wallet!!.publicKey.base58Representation == Wallet.SDWalletEd255519.base58Encoded)

        wallet = SDWallet(Wallet.mnemonic, Wallet.passphrase)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet!!.secretKey.bytes.toHexString() == Wallet.SDWalletEd255519_Pwd.privateKey)
        Assert.assertTrue(wallet!!.publicKey.bytes.toHexString() == Wallet.SDWalletEd255519_Pwd.publicKey)
    }

    @Test
    fun test_Common_Sign() {
        val wallet = SDWallet(Wallet.mnemonic)
        val messageHex = Wallet.messageToSign.toByteArray().toHexString()
        val result = wallet!!.sign(messageHex)
        Assert.assertTrue(result!!.toHexString() == Wallet.SDWalletEd255519.signedData)
    }

    companion object {
        // Expected outputs for a wallet without a passphrase.
        const val PUBLIC_KEY_NO_PASSPHRASE = "edpku9ZF6UUAEo1AL3NWy1oxHLL6AfQcGYwA5hFKrEKVHMT3Xx889A"
        const val SECRET_KEY_NO_PASSPHRASE =
            "edskS4pbuA7rwMjsZGmHU18aMP96VmjegxBzwMZs3DrcXHcMV7VyfQLkD5pqEE84wAMHzi8oVZF6wbgxv3FKzg7cLqzURjaXUp"
        const val PUBLIC_KEY_HASH_NO_PASSPHRASE = "tz1Y3qqTg9HdrzZGbEjiCPmwuZ7fWVxpPtRw"

        // Expected outputs for a wallet with a passphrase.
        const val PUBLIC_KEY_PASSPHRASE = "edpktnCgi3C7ZLyLrF4NAebDkgu5PZRRJ9BafxskVEj6U1GycyRird"
        const val SECRET_KEY_PASSPHRASE =
            "edskRjazzmroxmJagYDhCT1jXna8m9H2qvjtPAcrZYZ31og4ud1u2kkxYGv8e7CjmbW33QubzugueXqLFPMbM2eAj6j3AQHrCW"
        const val PUBLIC_KEY_HASH_PASSPHRASE = "tz1ZfhME1B2kmagqEJ9P7PE8joM3TbVQ5r4v"
    }
}
