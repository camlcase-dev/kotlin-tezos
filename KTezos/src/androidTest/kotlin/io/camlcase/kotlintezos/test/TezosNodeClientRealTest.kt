/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test

import android.util.Log
import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.core.parser.toPrimitive
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OriginationOperationResult
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.TokenOperationParams
import io.camlcase.kotlintezos.model.operation.TokenOperationType
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import io.camlcase.kotlintezos.test.network.MockDispatcher
import io.camlcase.kotlintezos.test.network.tezosNodeClient
import io.camlcase.kotlintezos.test.util.TestnetAddress.Companion.TEST_ADDRESS_2
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.Wallet
import io.github.vjames19.futures.jdk8.onComplete
import kotlinx.serialization.json.Json
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger
import java.util.concurrent.Executors

class TezosNodeClientRealTest {
    private val client = tezosNodeClient()

    private fun testHead() {
        client.getHead(object : TezosCallback<Map<String, Any?>> {
            override fun onSuccess(item: Map<String, Any?>?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    private fun testHeadHash() {
        client.getHeadHash(object : TezosCallback<String> {
            override fun onSuccess(item: String?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    private fun testGetBalance(address: Address = testWallet1!!.mainAddress) {
        client.getBalance(address, object : TezosCallback<Tez> {
            override fun onSuccess(item: Tez?) {
                Log.i("** SUCCESS! ", "$item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                Log.i("** ERROR! ", "$error")
            }
        })
    }

    private fun testOperationMetadata() {
        val executorService = Executors.newFixedThreadPool(3)
        val service =
            BlockchainMetadataService(
                DefaultNetworkClient(TezosNodeClient.FLORENCENET_GIGA_NODE, true),
                executorService
            )
        service.getMetadata(TEST_ADDRESS_2)
            .onComplete(
                onFailure = { throwable ->
                    println("** ERROR! $throwable")
                },
                onSuccess = { result ->
                    println("** SUCCESS! $result - ${Thread.currentThread()}")
                })
    }

    /**
     * You first need to check the account has enough XTZ
     * https://carthagenet.tezos.id/accounts/[from]
     */
    private fun testSendTransaction(
        from: Wallet? = testWallet1!!,
        to: Address = testWallet2!!.mainAddress,
        amount: Tez = Tez(0.1)
    ) {
        client.send(
            amount,
            to,
            from!!.mainAddress,
            from,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }
            })
    }

    fun testOriginateContract(
        wallet: Wallet = testWallet1!!
    ) {
        val json = MockDispatcher.getStringFromFile("dexter_spending_limit.json")
        val limit = String.format(spendingLimit, Tez(1.0).stringRepresentation)
        val init = String.format(michelineInit, limit, wallet.mainAddress)

        val list: List<Map<String, Any?>> = Json.parseToJsonElement(json).toPrimitive() as List<Map<String, Any?>>
        val map: Map<String, Any?> = Json.parseToJsonElement(init).toPrimitive() as Map<String, Any?>
        val script = SmartContractScript(
            list,
            map
        )

        client.originateContract(
            wallet.mainAddress,
            script,
            wallet,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<OriginationOperationResult> {
                override fun onSuccess(item: OriginationOperationResult?) {
                    println("** ORIGINATION SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ORIGINATION ERROR! $error")
                }
            })
    }

    private fun testGetDelegate(address: Address = testWallet1!!.mainAddress) {
        client.getDelegate(address, object : TezosCallback<String> {
            override fun onSuccess(item: String?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    /**
     * Check the delegate has balance
     */
    private fun testRegisterDelegate(delegate: Wallet? = testWallet1) {
        println(delegate)
        client.registerDelegate(
            delegate!!.mainAddress,
            delegate,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DELEGATE SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DELEGATE ERROR! $error")
                }
            })
    }

    /**
     * [source] should not be registered as baker/delegate
     */
    private fun testDelegate(
        source: Wallet? = testWallet2,
        delegate: Address = testWallet1!!.mainAddress
    ) {
        client.delegate(
            source!!.mainAddress,
            delegate,
            source,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DELEGATE SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DELEGATE ERROR! $error")
                }
            })
    }

    private fun testUndelegate(
        source: Wallet? = testWallet1!!
    ) {
        client.undelegate(
            source!!.mainAddress,
            source,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DELEGATE SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DELEGATE ERROR! $error")
                }
            })

    }

    private fun testReveal(source: Wallet? = SDWallet()) {
        println(source)
        client.reveal(
            source!!.mainAddress,
            source.publicKey,
            source,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }
            })
    }

    private fun testCallSmartContact(
        source: Wallet? = testWallet1,
        contract: Address = TEST_SAMPLE_CONTRACT,
        amount: Tez = Tez.zero,
        parameter: MichelsonParameter? = StringMichelsonParameter("world")
    ) {
        client.call(
            source!!.mainAddress,
            contract,
            amount,
            parameter,
            null,
            source,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SMART CONTRACT SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** SMART CONTRACT ERROR! $error")
                }
            })
    }

    fun testContractStorage(contract: Address = TEST_SAMPLE_CONTRACT) {
        client.getContractStorage(contract, object : TezosCallback<Map<String, Any?>> {
            override fun onSuccess(item: Map<String, Any?>?) {
                println("** STORAGE SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** STORAGE ERROR! $error")
            }
        })
    }

    fun testGetBigMapValue(
        contract: Address = "KT1LKSFTrGSDNfVbWV4JXRrqGRD8XDSv5NAU",
        key: MichelsonParameter = StringMichelsonParameter("tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW"),
        type: MichelsonComparable = MichelsonComparable.ADDRESS
    ) {
        client.getBigMapValue(contract, key, type, object : TezosCallback<Map<String, Any?>> {
            override fun onSuccess(item: Map<String, Any?>?) {
                println("** BIGMAP SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** BIGMAP ERROR! $error")
            }
        })
    }

    private fun testRunMultipleOperations(
        wallet: Wallet = testWallet2!!,
        phoenixContractAddress: Address = "KT1QveTF9T6FQaz49TjbW2fKn5goGMGvCaPj"
    ) {
        val list: ArrayList<Operation> = ArrayList()

        list.add(
            TransactionOperation(
                Tez(10.0),
                wallet.mainAddress,
                phoenixContractAddress,
                OperationFees.simulationFees
            )
        )
        val params = TokenOperationParams.Transfer(
            BigInteger("1"),
            TokenContractRealTest.STKR_MOCK_ADDRESS,
            phoenixContractAddress,
            wallet.mainAddress
        )
        list.addAll(
            OperationFactory.createTokenOperation(
                TokenOperationType.TRANSFER,
                params,
                OperationFees.simulationFees
            )!!
        )

        val params2 = TokenOperationParams.Transfer(
            BigInteger("1"),
            TokenContractRealTest.TZBTC_MOCK_ADDRESS,
            phoenixContractAddress,
            wallet.mainAddress
        )
        list.addAll(
            OperationFactory.createTokenOperation(
                TokenOperationType.TRANSFER,
                params2,
                OperationFees.simulationFees
            )!!
        )

        client.runOperations(
            list,
            wallet.mainAddress,
            wallet,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    error.printStackTrace()
                    println("** ERROR! $error")
                }
            })
    }

    private fun testActivateAccount(
        address: Address = "tz1aAp14Ggss3NnnSFYmVT4e2LgRe7yFgiUU",
        secret: String = "bf50972ef645cd2638d0f41136cf5b9e6a599eb8"
    ) {
        val wallet = SDWallet(
            listOf(
                "second",
                "recall",
                "tape",
                "burden",
                "present",
                "dash",
                "margin",
                "exercise",
                "dolphin",
                "genius",
                "walnut",
                "protect",
                "fish",
                "radar",
                "small"
            ),
            "lwlmoril.rtevyeqi@tezos.example.org" + "dThaLzpcZs"
        )
        Assert.assertTrue(wallet!!.mainAddress == address)
        client.activateAccount(
            address,
            secret = secret,
            wallet,
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    error.printStackTrace()
                    println("** ERROR! $error")
                }
            })
    }

    private fun waitForInclusion() {
        Thread.sleep(90 * 1000) // wait ~1minute for the tx to be included in the chain
    }

    /**
     * Test against a real node.
     * Run with debugger.
     */
    @Test
    fun testRealNode() {

        // Let the connection return something
//        Thread.sleep(15 * 60 * 1000)
    }
}

// tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5
val testWallet1 = SDWallet(
    "water, auto, cabin, document, shallow, art, idle, draw, panda, allow, mean, track".split(", ")
)

// tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn
val testWallet2 = SDWallet(
    "bronze, post, shove, leader, style, enforce, van, orphan, blossom, shiver, year, soon".split(", ")
)

const val TEST_SAMPLE_CONTRACT = "KT194hQVCPCsCYUhDjbeUCwpKcKyzN3AZLbc" // idString.tz
private val spendingLimit = "{\"prim\":\"Some\",\"args\":[{\"int\":\"%s\"}]}]}"
private val michelineInit =
    "{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"None\"},%s,{\"string\":\"%s\"}]}"
