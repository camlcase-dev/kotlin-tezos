package io.camlcase.kotlintezos.test.calculations

import androidx.test.platform.app.InstrumentationRegistry
import io.camlcase.kotlintezos.core.js.JavascriptContext
import io.camlcase.kotlintezos.core.parser.toPrimitiveMap
import io.camlcase.kotlintezos.exchange.calculations.AndroidJSDexterCalculations
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.test.network.MockDispatcher
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger

class AndroidJSDexterCalculationsTest {
    private val appContext = InstrumentationRegistry.getInstrumentation().context
    private val jsDexterCalculations = AndroidJSDexterCalculations(JavascriptContext(appContext))

    @Before
    fun setUp() {
        jsDexterCalculations.initReading()
    }

    @After
    fun tearDown() {
        jsDexterCalculations.closeReading()
    }

    @Test
    fun test_XtzToToken_Calculations() {
        val json = MockDispatcher.getStringFromFile("xtz_to_token.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val xtzFrom = Tez(jsonObj["xtz_in"] as String)
            val expectedTo = BigInteger(jsonObj["token_out"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val amountOfTokenToReceive =
                jsDexterCalculations.xtzToTokenTokenOutput(xtzFrom, xtzPool, tokenPool)

            // ASSERT
            Assert.assertEquals(expectedTo, amountOfTokenToReceive)
        }
    }

    @Test
    fun test_XtzToToken_PriceImpact() {
        val json = MockDispatcher.getStringFromFile("xtz_to_token.json")
        val list = Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val xtzFrom = Tez(jsonObj["xtz_in"] as String)
            val expectedPriceImpact = BigDecimal(jsonObj["price_impact"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val priceImpact =
                jsDexterCalculations.xtzToTokenPriceImpact(xtzFrom, xtzPool, tokenPool)

            // ASSERT
            val resultIn4Decimals = priceImpact.setScale(4, BigDecimal.ROUND_HALF_UP)
            val expectedIn4Decimals = expectedPriceImpact.setScale(4, BigDecimal.ROUND_HALF_UP)
            Assert.assertTrue(resultIn4Decimals == expectedIn4Decimals)
        }
    }


    @Test
    fun test_TokenToXTZ_Calculations() {
        val json = MockDispatcher.getStringFromFile("token_to_xtz_old.json")
        val list =  Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val tokenFrom = BigInteger(jsonObj["token_in"] as String)
            val expected = Tez(jsonObj["xtz_out"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val amountOfXtzToReceive =
                jsDexterCalculations.tokenToXtzXtzOutput(tokenFrom, xtzPool, tokenPool)

            // ASSERT
            Assert.assertEquals(expected, amountOfXtzToReceive)
        }
    }

    @Test
    fun test_TokenToXTZ_Slippage() {
        val json = MockDispatcher.getStringFromFile("token_to_xtz.json")
        val list =  Json.parseToJsonElement(json) as JsonArray
        for (item in list) {
            // PARSE
            val jsonObj = (item as JsonObject).toPrimitiveMap()
            val tokenFrom = BigInteger(jsonObj["token_in"] as String)
            val expectedPriceImpact = BigDecimal(jsonObj["price_impact"] as String)
            val xtzPool = Tez(jsonObj["xtz_pool"] as String)
            val tokenPool = BigInteger(jsonObj["token_pool"] as String)

            // CALCULATE
            val priceImpact =
                jsDexterCalculations.tokenToXtzPriceImpact(tokenFrom, xtzPool, tokenPool)

            // ASSERT
            val resultIn4Decimals = priceImpact.setScale(4, BigDecimal.ROUND_HALF_UP)
            val expectedIn4Decimals = expectedPriceImpact.setScale(4, BigDecimal.ROUND_HALF_UP)
            Assert.assertTrue(resultIn4Decimals == expectedIn4Decimals)
        }
    }

    @Test(expected = TezosError::class)
    fun test_Invalid_TokenToXTZ_PriceImpact() {
        val tokenFrom = BigInteger.ZERO
        val xtzPool = Tez("2000000000")
        val tokenPool = BigInteger("20000")
        val result =
            jsDexterCalculations.tokenToXtzPriceImpact(tokenFrom, xtzPool, tokenPool)
    }

    @Test
    fun test_xtzToTokenMarketRate() {
        var tezPool = Tez("1000000")
        var tokenPool = BigInteger("500000000000000000000")
        var decimals = 18
        var result = jsDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        // That last 6 appears because JS calc
        Assert.assertEquals(BigDecimal("500.00000000000006"), result)

        tezPool = Tez("144621788919")
        tokenPool = BigInteger("961208019")
        decimals = 8
        result = jsDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("0.000066463568607795"), result)

        tezPool = Tez("20167031717")
        tokenPool = BigInteger("41063990114535450000")
        decimals = 18
        result = jsDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("0.0020361940562586686"), result)

        tezPool = Tez("46296642164")
        tokenPool = BigInteger("110543540642")
        decimals = 6
        result = jsDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("2.3877226398064355"), result)

        tezPool = Tez("58392357794")
        tokenPool = BigInteger("73989702350")
        decimals = 6
        result = jsDexterCalculations.xtzToTokenMarketRate(tezPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("1.2671127720347453"), result)
    }

    @Test
    fun test_xtzToTokenExchangeRate_OK() {
        var xtzIn = Tez("1000000")
        var xtzPool = Tez("34204881343")
        var tokenPool = BigInteger("39306268")
        // based on tzBTC test data
        val exchangeRate =
            jsDexterCalculations.xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate > BigDecimal((0.001145 - 0.0005)))
        Assert.assertTrue(exchangeRate < BigDecimal((0.001145 + 0.0005)))

        // based on USDtz test data
        xtzIn = Tez("1000000")
        xtzPool = Tez("3003226688")
        tokenPool = BigInteger("668057425")
        val exchangeRate2 =
            jsDexterCalculations.xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate2 > BigDecimal((0.22170500 - 0.0005)))
        Assert.assertTrue(exchangeRate2 < BigDecimal((0.22170500 + 0.0005)))

    }

    @Test
    fun test_xtzToTokenMinimumTokenOutput() {
        val inputs = listOf(
            Triple("10000", "0.05", "9500"),
            Triple("10000", "0.01", "9900"),
            Triple("330000", "0.005", "328350"),
            Triple("1000", "0.01", "990"),
            Triple("5000", "0.2", "4000"),
            Triple("100", "0.055", "94"),
            Triple("5846941182", "0.3142", "4009832262"),
        )

        for (input in inputs) {
            val tokenOut = BigInteger(input.first)
            val allowedSlippage = input.second.toDouble()
            val expected = BigInteger(input.third)

            val result =
                jsDexterCalculations.xtzToTokenMinimumTokenOutput(tokenOut, allowedSlippage)
            Assert.assertEquals(expected, result)
        }
    }

    @Test
    fun test_tokenToXtzExchangeRate() {
        var tokenIn = BigInteger("100000000")
        var xtzPool = Tez("38490742927")
        var tokenPool = BigInteger("44366268")
        // based on tzBTC test data
        val exchangeRate =
            jsDexterCalculations.tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate > BigDecimal((266.3723523200 - 0.0005)))
        Assert.assertTrue(exchangeRate < BigDecimal((266.3723523200 + 0.0005)))

        // based on USDtz test data
        tokenIn = BigInteger("34000000")
        xtzPool = Tez("3003926688")
        tokenPool = BigInteger("667902216")
        val exchangeRate2 =
            jsDexterCalculations.tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool)
        Assert.assertTrue(exchangeRate2 > BigDecimal((4.26747503 - 0.0005)))
        Assert.assertTrue(exchangeRate2 < BigDecimal((4.26747503 + 0.0005)))
    }

    @Test
    fun test_tokenToXtzMarketRate() {
        var xtzPool = Tez("1000000")
        var tokenPool = BigInteger("500000000000000000000")
        var decimals = 18
        var result =
            jsDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("0.0019999999999999996"), result)

        xtzPool = Tez("144621788919")
        tokenPool = BigInteger("961208019")
        decimals = 8
        result =
            jsDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("15045.836703428498"), result)

        xtzPool = Tez("20167031717")
        tokenPool = BigInteger("41063990114535450000")
        decimals = 18
        result =
            jsDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("491.11232641421907"), result)

        xtzPool = Tez("46296642164")
        tokenPool = BigInteger("110543540642")
        decimals = 6
        result =
            jsDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("0.4188091126367452"), result)

        xtzPool = Tez("58392357794")
        tokenPool = BigInteger("73989702350")
        decimals = 6
        result =
            jsDexterCalculations.tokenToXtzMarketRate(xtzPool, tokenPool, decimals)
        Assert.assertEquals(BigDecimal("0.7891957385878037"), result)
    }

    @Test
    fun test_tokenToXtzMinimumXtzOutput() {
        val inputs = listOf(
            Triple("10000", "0.05", "9500"),
            Triple("10000", "0.01", "9900"),
            Triple("330000", "0.005", "328350"),
            Triple("2739516881", "0.36", "1753290803"),
        )

        for (input in inputs) {
            val xtzOut = Tez(input.first)
            val allowedSlippage = input.second.toDouble()
            val expected = Tez(input.third)

            val result =
                jsDexterCalculations.tokenToXtzMinimumXtzOutput(xtzOut, allowedSlippage)
            Assert.assertEquals(expected, result)
        }
    }

    @Test
    fun test_xtzToToken_PriceLimit() {
        // good
        var xtzIn: Tez = Tez("1000000")
        var xtzPool: Tez = Tez("29757960047")
        var tokenPool: BigInteger = BigInteger("351953939")

        var priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.00010314839572795817"), priceImpact)

        // too small
        xtzIn = Tez("5")
        xtzPool = Tez("100000")
        tokenPool = BigInteger("10")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal.ZERO, priceImpact)

        // other
        xtzIn = Tez("20000")
        tokenPool = BigInteger("10")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.5"), priceImpact)

        xtzIn = Tez("90000")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.5555555555555556"), priceImpact)

        xtzIn = Tez("200000")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.7"), priceImpact)

        // based on tzBTC test data
        xtzIn = Tez("1000000")
        xtzPool = Tez("34204881343")
        tokenPool = BigInteger("39306268")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.00012362753169542967"), priceImpact)

        xtzIn = Tez("200000000")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.005814829860627381"), priceImpact)

        // based on USDtz test data
        xtzIn = Tez("10000000")
        xtzPool = Tez("3003226688")
        tokenPool = BigInteger("668057425")

        priceImpact = jsDexterCalculations.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.003318788783598409"), priceImpact)
    }


    @Test
    fun test_tokenToXtz_PriceImpact() {
        // based on tzBTC test data
        var tokenIn = BigInteger("100000000")
        var xtzPool = Tez("3849181242")
        var tokenPool = BigInteger("44365061")
        var priceImpact = jsDexterCalculations.tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.6926883784623051"), priceImpact)

        tokenIn = BigInteger("40000000")
        priceImpact = jsDexterCalculations.tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.47412992462990605"), priceImpact)

        // based on USDtz test data
        tokenIn = BigInteger("1000000")
        xtzPool = Tez("2869840667")
        tokenPool = BigInteger("699209512")
        priceImpact = jsDexterCalculations.tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.0014283084378495926"), priceImpact)

        tokenIn = BigInteger("8000000000")
        xtzPool = Tez("9563874659")
        tokenPool = BigInteger("19868860091")
        priceImpact = jsDexterCalculations.tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool)
        Assert.assertEquals(BigDecimal("0.28705874495878053"), priceImpact)
    }
}
