/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.kotlintezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.wallet.SigningService
import org.junit.Assert
import org.junit.Test

class SigningServiceTest {

    @Test
    fun testOk() {
        val result = SigningService.sign(fakeOperationPayload, fakeMetadata, FORGE_RESULT, fakeSignature)
        println("$result")
        Assert.assertNotNull(result)
    }

    @Test(expected = TezosError::class)
    fun testEmptyForgeResult() {
        SigningService.sign(fakeOperationPayload, fakeMetadata, "    ", fakeSignature)
    }

    @Test(expected = TezosError::class)
    fun testNullForgeResult() {
        SigningService.sign(fakeOperationPayload, fakeMetadata, null, fakeSignature)
    }

}
