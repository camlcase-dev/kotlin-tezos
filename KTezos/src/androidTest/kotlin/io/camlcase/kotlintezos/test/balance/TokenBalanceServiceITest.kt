/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.balance

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.test.network.CamlCredentials
import io.camlcase.kotlintezos.test.testWallet1
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.token.balance.TokenBalanceService
import io.camlcase.kotlintezos.token.balance.TokenBalanceService.Companion.revealSignature
import io.camlcase.kotlintezos.wallet.SDWallet
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.math.BigInteger
import java.util.concurrent.CountDownLatch

/**
 * Against a real node
 */
class TokenBalanceServiceITest {
    internal lateinit var countDownLatch: CountDownLatch

    @Before
    fun setup() {
        countDownLatch = CountDownLatch(1)
    }

    private fun initService(): TokenBalanceService {
        val executorService = CurrentThreadExecutor()
        val networkClient = CamlCredentials().createNetworkClient(TezosNodeClient.TESTNET_GIGANODE_NODE)
        val metadataService = BlockchainMetadataService(networkClient, executorService)
        val simulationService = SimulationService(networkClient, executorService)
        return TokenBalanceService(
            TezosNetwork.FLORENCENET,
            metadataService,
            simulationService,
            executorService
        )
    }

    // Account with three balances > 0
    @Test
    fun getTokenBalances_OK() {
        val service = initService()

        service.getTokenBalances(
            tokens,
            testWallet1!!.mainAddress,
            fakeSignature,
            object : TezosCallback<HashMap<Address, BigInteger>> {
                override fun onSuccess(item: HashMap<Address, BigInteger>?) {
                    println(item)
                    Assert.assertTrue(item!!.size == tokens.size)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    // Empty account with giberish publicKey
    @Test
    fun getTokenBalances_KO() {
        val service = initService()
        service.getTokenBalances(
            tokens,
            EMPTY_WALLET!!.mainAddress,
            fakeSignature,
            object : TezosCallback<HashMap<Address, BigInteger>> {
                override fun onSuccess(item: HashMap<Address, BigInteger>?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    println(error)
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    // Empty account with their correct public key
    @Test
    fun getTokenBalances_Empty_OK() {
        val service = initService()
        service.getTokenBalances(
            tokens,
            EMPTY_WALLET!!.mainAddress,
            revealSignature(EMPTY_WALLET.publicKey),
            object : TezosCallback<HashMap<Address, BigInteger>> {
                override fun onSuccess(item: HashMap<Address, BigInteger>?) {
                    println(item)
                    Assert.assertTrue(item!!.size == tokens.size)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    companion object {
        val tokens = listOf(
            "KT1BVwiXfDdaXsvcmvSmBkpZt4vbGVhLmhBh",
            "KT1AuVKbQZTAwA7mdS7TZSPZD9ZH1Eb62JFf",
        )
    }
}

val EMPTY_WALLET = SDWallet(
    "inflict, silk, donkey, iron, extend, cheap, clump, sick, hospital, above, resource, minute".split(", ")
)
