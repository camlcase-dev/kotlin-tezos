/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.test.network.CamlCredentials
import io.camlcase.kotlintezos.test.util.VALIDATOR_NODE_URL
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.token.TokenContractClient
import io.camlcase.kotlintezos.wallet.SDWallet
import org.junit.Test
import java.math.BigInteger

class TokenContractRealTest {

    private fun initClient(
        nodeUrl: String = TezosNodeClient.EDONET_GIGA_NODE,
        tokenContractAddress: Address = "KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK"
    ): TokenContractClient {
        val nodeClient = TezosNodeClient(nodeUrl, VALIDATOR_NODE_URL, true)
        val networkClient = CamlCredentials().createNetworkClient()
        return TokenContractClient(
            TezosNetwork.CARTHAGENET,
            tokenContractAddress,
            nodeClient,
            networkClient
        )
    }

    private fun testTokenBalance(
        nodeUrl: String = TezosNodeClient.EDONET_GIGA_NODE,
        tokenContractAddress: Address = "KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK",
        userAddress: Address = "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z"
    ) {

        val client = initClient(nodeUrl, tokenContractAddress)
        client.getTokenBalance(userAddress, fakeSignature.publicKey, object : TezosCallback<BigInteger> {
            override fun onSuccess(item: BigInteger?) {
                println("** TOKEN SUCCESS! $item - ${Thread.currentThread()}")

            }

            override fun onFailure(error: TezosError) {
                println("** TOKEN ERROR! $error")
            }
        })
    }

    private fun testSendToken(
        nodeUrl: String = TezosNodeClient.EDONET_GIGA_NODE,
        tokenContractAddress: Address = STKR_MOCK_ADDRESS,
        from: SDWallet = testWallet2!!,
        to: Address = testWallet1!!.mainAddress
    ) {

        val client = initClient(nodeUrl, tokenContractAddress)
        client.send(
            1.toBigInteger(),
            to,
            from.mainAddress,
            from,
            OperationFeesPolicy.Estimate(),
            callback = object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** TOKEN SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** TOKEN ERROR! $error")
                }
            }
        )
    }

    /**
     * Test against a real node.
     * Run with debugger.
     */
    @Test
    fun testRealNode() {

        // Let the connection return something
//        Thread.sleep(15 * 60 * 1000)
    }

    companion object {
        const val STKR_MOCK_ADDRESS = "KT19YysGBZTgKg2FtkC1Ngi7ahmWYTzKZoxa"
        const val TZBTC_MOCK_ADDRESS = "KT1QgTuMDYsRvofNy3sfrpYzGwx3GD7pSapo"
    }
}

